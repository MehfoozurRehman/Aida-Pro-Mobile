import React from 'react';
import {Dimensions} from 'react-native';
import Svg, {G, Path} from 'react-native-svg';
import CompanyDetailsPreviewScreen from '../screens/Global/CompanyDetailsPreviewScreen';
import CompanyDetailsEditScreen from '../screens/Global/CompanyDetailsEditScreen';
import MapScreen from '../screens/Global/MapScreen';
import ProfileScreen from '../screens/Global/ProfileScreen';
import ChangePassword from '../screens/Global/ChangePassword';
import {HomeScreen as ProfessionlHomeScreen} from '../screens/Professional';
import {HomeScreen as FreelancerHomeScreen} from '../screens/Freelancer';
import {
  home,
  homeActive,
  billing,
  billingActive,
  chat,
  chatActive,
  posting,
  postingActive,
  user,
  userActive,
  planActive,
  plan,
  experiance,
  experianceActive,
  portfolio,
  portfolioActive,
  education,
  educationActive,
  contact,
  contactActive,
  blog,
  blogActive,
  coffee,
  coffeeActive,
  detailsActive,
  details,
} from '../assets/icons';
import {
  HomeScreen as CompanyHomeScreen,
  BillingAndPaymentsScreen,
  PlansAndPricingScreen,
  PostAJobScreen,
  PostAProjectScreen,
  PostingDetailsScreen,
  PostingScreen,
  PostingsListScreen,
  SearchScreen,
} from '../screens/Company';
import {
  AboutUsScreen,
  BlogArticalScreen,
  BlogScreen,
  ChatScreen,
  CoffeeCornerDiscussionScreen,
  CoffeeCornerScreen,
  ContactUsScreen,
  FaqScreen,
  MessengerScreen,
  NotificationScreen,
  PersonalDetailsPreview,
  PrivacyPolicyScreen,
  ProfessionalDetailsPreview,
  TermsAndConditionsScreen,
  JobProjectScreen,
  JobProjectDetailsScreen,
  PersonalDetailsEdit,
  JobProjectPreviewScreen,
  FaqDetailScreen,
} from '../screens/Global';
import {dark} from './colors';
export const windowWidth = Dimensions.get('window').width;
export const windowHeight = Dimensions.get('window').height;
export const role = 'Freelancer';

export let globalDrawerScreenList = [
  {
    name: 'Home',
    component:
      role === 'Freelancer'
        ? FreelancerHomeScreen
        : role === 'Professional'
        ? ProfessionlHomeScreen
        : CompanyHomeScreen,
  },
  {
    name: 'Messenger',
    component: MessengerScreen,
  },
  {
    name: 'Chat',
    component: ChatScreen,
  },
  {
    name: 'Posting',
    component: PostingScreen,
  },
  {
    name: 'Posting List',
    component: PostingsListScreen,
  },
  {
    name: 'Posting Details',
    component: PostingDetailsScreen,
  },
  {
    name: 'Post A Job',
    component: PostAJobScreen,
  },
  {
    name: 'Post A Project',
    component: PostAProjectScreen,
  },

  {
    name: 'Plans & Pricing',
    component: PlansAndPricingScreen,
  },
  {
    name: 'Billing & Payments',
    component: BillingAndPaymentsScreen,
  },
  {
    name: 'Personal Details',
    component: PersonalDetailsPreview,
  },
  {
    name: 'Personal Details Edit',
    component: PersonalDetailsEdit,
  },
  {
    name: 'Professional Details',
    component: ProfessionalDetailsPreview,
  },
  {
    name: 'About Us',
    component: AboutUsScreen,
  },
  {
    name: 'Blog',
    component: BlogScreen,
  },
  {
    name: 'Blog Artical',
    component: BlogArticalScreen,
  },
  {
    name: 'Coffee Corner',
    component: CoffeeCornerScreen,
  },
  {
    name: 'Coffee Corner Discussion',
    component: CoffeeCornerDiscussionScreen,
  },
  {
    name: 'Contact Us',
    component: ContactUsScreen,
  },
  {
    name: 'FAQ',
    component: FaqScreen,
  },
  {
    name: 'Notification',
    component: NotificationScreen,
  },
  {
    name: 'Privacy & Policy',
    component: PrivacyPolicyScreen,
  },
  {
    name: 'Terms & Conditions',
    component: TermsAndConditionsScreen,
  },
  {
    name: 'Company Details',
    component: CompanyDetailsPreviewScreen,
  },
  {
    name: 'Company Details Edit',
    component: CompanyDetailsEditScreen,
  },
  {name: 'My Projects', component: JobProjectScreen},
  {name: 'Project Details', component: JobProjectDetailsScreen},
  {name: 'Search', component: SearchScreen},
  {name: 'Map', component: MapScreen},
  {name: 'Profile', component: ProfileScreen},
  {name: 'Change Password', component: ChangePassword},
  {name: 'Job Preview', component: JobProjectPreviewScreen},
  {name: 'Project Preview', component: JobProjectPreviewScreen},
  {name: 'FAQ Details', component: FaqDetailScreen},
];

export const companyDrawerItemList = [
  {
    label: 'Billing & Payments',
    icon: billing,
    iconActive: billingActive,
  },
  {
    label: 'Blog',
    icon: blog,
    iconActive: blogActive,
  },
];
export const freelancerDrawerItemList = [
  {
    label: 'My Projects',
    icon: posting,
    iconActive: postingActive,
  },
  {
    label: 'Blog',
    icon: blog,
    iconActive: blogActive,
  },
];
export const professionalDrawerItemList = [
  {
    label: 'My Projects',
    icon: posting,
    iconActive: postingActive,
  },
  {
    label: 'Blog',
    icon: blog,
    iconActive: blogActive,
  },
];

export const companyTabItemList = [
  {
    label: 'Home',
    icon: home,
    iconActive: homeActive,
  },
  {
    label: 'Messenger',
    icon: chat,
    iconActive: chatActive,
  },
  {
    label: 'Posting',
    icon: posting,
    iconActive: postingActive,
  },
  {
    label: 'Plans & Pricing',
    icon: plan,
    iconActive: planActive,
  },
  {
    label: 'Coffee Corner',
    icon: coffee,
    iconActive: coffeeActive,
  },
];
export const professionalTabItemList = [
  {
    label: 'Home',
    icon: home,
    iconActive: homeActive,
  },
  {
    label: 'Messenger',
    icon: chat,
    iconActive: chatActive,
  },
  {
    label: 'Personal Details',
    icon: details,
    iconActive: detailsActive,
  },
  {
    label: 'Professional Details',
    icon: plan,
    iconActive: planActive,
  },
  {
    label: 'Coffee Corner',
    icon: coffee,
    iconActive: coffeeActive,
  },
];
export const freelancerTabItemList = [
  {
    label: 'Home',
    icon: home,
    iconActive: homeActive,
  },
  {
    label: 'Messenger',
    icon: chat,
    iconActive: chatActive,
  },
  {
    label: 'Personal Details',
    icon: details,
    iconActive: detailsActive,
  },
  {
    label: 'Professional Details',
    icon: plan,
    iconActive: planActive,
  },
  {
    label: 'Coffee Corner',
    icon: coffee,
    iconActive: coffeeActive,
  },
];
export const signUpInputBoxList = [
  {
    placeholder: 'First',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Last',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Email',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Country',
    secureTextEntry: false,
    transparent: false,
    variant: 'select',
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Password',
    secureTextEntry: true,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Confirm',
    secureTextEntry: true,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
];

export const companyDetailsInputs = [
  {
    placeholder: 'Company Name',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Representative Name',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Email',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Phone',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Industry / Branch',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'No. of Employee',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Address',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Zip Code',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Country',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'City',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
];
export const personalDetailsInputs = [
  {
    placeholder: 'First Name',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Last Name',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Email',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Phone',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Industry',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Address',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Zip Code',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Country',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'City',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={11.907} height={15.9}>
        <G data-name="Group 1478" fill={dark}>
          <Path
            data-name="Path 285"
            d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
          />
          <Path
            data-name="Path 286"
            d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
          />
          <Path
            data-name="Path 287"
            d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
          />
          <Path
            data-name="Path 288"
            d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
          />
        </G>
      </Svg>
    ),
  },
];
export const postJobInputBoxList = [
  {
    placeholder: 'Job Title',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Deadline',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Location',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Education',
    secureTextEntry: false,
    transparent: false,
    variant: 'select',
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Skill Required',
    secureTextEntry: false,
    transparent: false,
    variant: 'select',
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Job Type',
    secureTextEntry: false,
    transparent: false,
    variant: 'select',
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Salary Type',
    secureTextEntry: false,
    transparent: false,
    variant: 'select',
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
];

export const postingListItem = [
  {
    id: 1,
  },
  {
    id: 2,
  },
  {
    id: 3,
  },
  {
    id: 4,
  },
];
export const postingDetailsTabItem = [
  {
    label: 'Profile',
    icon: user,
    iconActive: userActive,
  },
  {
    label: 'Work Experience',
    icon: experiance,
    iconActive: experianceActive,
  },
  {
    label: 'Project Portfolio',
    icon: portfolio,
    iconActive: portfolioActive,
  },
  {
    label: 'Education',
    icon: education,
    iconActive: educationActive,
  },
  {
    label: 'Contact',
    icon: contact,
    iconActive: contactActive,
  },
];
