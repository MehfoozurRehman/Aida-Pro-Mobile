module.exports = {
  dark: '#374957',
  yellow: '#f7983d',
  green100: '#0ee1a3',
  green200: '#0ca69d',
  white: '#ffffff',
  lightGreen: '#f3f9f9',
};
