import React from 'react';
import {Text, TouchableOpacity, Image, View} from 'react-native';
import {white} from '../constants/colors';

export default function CustomDrawerItem({
  icon,
  iconActive,
  label,
  isSelected,
  setIsSelected,
  navigation,
}) {
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate(label === 'Logout' ? 'Login' : label);
      }}
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 8,
        width: '100%',
        borderRadius: 5,
        flex: 1,
        backgroundColor: isSelected === label ? white : null,
      }}>
      <View style={{width: 24, height: 24, marginRight: 10}}>
        <Image
          source={isSelected === label ? icon : iconActive}
          style={{maxHeight: '100%', maxWidth: '100%'}}
        />
      </View>
      <Text
        style={{
          fontSize: 12,
          color: isSelected === label ? '#0DC5A1' : white,
        }}>
        {label}
      </Text>
    </TouchableOpacity>
  );
}
