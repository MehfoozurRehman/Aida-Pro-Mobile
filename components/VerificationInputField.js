import React, {useState} from 'react';
import {Text, StyleSheet} from 'react-native';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import {dark, green200, white} from '../constants/colors';

const styles = StyleSheet.create({
  codeFieldRoot: {marginVertical: 20},
  cell: {
    width: 40,
    height: 40,
    lineHeight: 38,
    fontSize: 18,

    color: dark,
    borderWidth: 1,
    borderRadius: 4,
    borderColor: '#00000030',
    textAlign: 'center',
    backgroundColor: white,
  },
  focusCell: {
    borderColor: green200,
  },
});

const CELL_COUNT = 6;

const VerificationInputField = () => {
  const [value, setValue] = useState('');
  const ref = useBlurOnFulfill({value, cellCount: CELL_COUNT});
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  return (
    <CodeField
      ref={ref}
      {...props}
      // Use `caretHidden={false}` when users can't paste a text value, because context menu doesn't appear
      value={value}
      onChangeText={setValue}
      cellCount={CELL_COUNT}
      rootStyle={styles.codeFieldRoot}
      keyboardType="number-pad"
      textContentType="oneTimeCode"
      renderCell={({index, symbol, isFocused}) => (
        <Text
          key={index}
          style={[styles.cell, isFocused && styles.focusCell]}
          onLayout={getCellOnLayoutHandler(index)}>
          {symbol || (isFocused ? <Cursor /> : null)}
        </Text>
      )}
    />
  );
};

export default VerificationInputField;
