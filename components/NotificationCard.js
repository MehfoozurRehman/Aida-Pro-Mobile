import React from 'react';
import {View, Text} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {dark, lightGreen} from '../constants/colors';

export default function NotificationCard({title, content, onPress}) {
  return (
    <TouchableOpacity
      style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        padding: 10,
        backgroundColor: lightGreen,
        borderRadius: 10,
        borderLeftWidth: 4,
        borderLeftColor: '#0DC5A1',
        marginVertical: 8,
      }}>
      <Text
        style={{
          fontSize: 14,
          fontWeight: 'bold',
          color: dark,
          marginVertical: 4,
        }}>
        {title}
      </Text>
      <Text style={{fontSize: 12, color: dark, marginBottom: 10}}>
        {content}
      </Text>
    </TouchableOpacity>
  );
}
