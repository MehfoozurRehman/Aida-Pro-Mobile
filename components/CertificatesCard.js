import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import Svg, {G, Path} from 'react-native-svg';
import {dark, white} from '../constants/colors';

export default function CertificatesCard({onPress, isNotEditable}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        padding: 16,
        borderRadius: 10,
        backgroundColor: white,
        position: 'relative',
        marginVertical: 10,
      }}>
      <Text style={{fontSize: 12, color: dark, fontWeight: 'bold'}}>
        Computer Scientist
      </Text>
      <Text
        style={{
          fontSize: 13,
          color: dark,
        }}>
        Jan 12,2021
      </Text>
      {isNotEditable ? null : (
        <TouchableOpacity
          style={{
            position: 'absolute',
            top: '-10%',
            left: '106%',
            width: 20,
            height: 20,
            borderRadius: 20,
            backgroundColor: dark,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Svg xmlns="http://www.w3.org/2000/svg" width={9.458} height={9.458}>
            <G
              fill="none"
              stroke={white}
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={1.5}>
              <Path data-name="Line 22" d="M8.397 1.061 1.061 8.397" />
              <Path data-name="Line 23" d="m1.061 1.061 7.336 7.336" />
            </G>
          </Svg>
        </TouchableOpacity>
      )}
    </TouchableOpacity>
  );
}
