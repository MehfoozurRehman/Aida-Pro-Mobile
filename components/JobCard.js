import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import Svg, {Defs, LinearGradient, Stop, Path, G} from 'react-native-svg';
import {dark, green200, lightGreen} from '../constants/colors';

export default function JobCard() {
  return (
    <TouchableOpacity
      style={{
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: lightGreen,
        borderRadius: 16,
        padding: 16,
        marginVertical: 6,
      }}>
      <View style={{display: 'flex', flexDirection: 'row', width: '100%'}}>
        <View
          style={{
            width: 45,
            height: 45,
            borderRadius: 10,
            backgroundColor: '#ABFBE3',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: 10,
          }}>
          <Svg xmlns="http://www.w3.org/2000/svg" width={20.4} height={17}>
            <Defs>
              <LinearGradient
                id="prefix__a"
                x1={0.5}
                x2={0.5}
                y2={1}
                gradientUnits="objectBoundingBox">
                <Stop offset={0} stopColor={green100} />
                <Stop offset={1} stopColor={green200} />
              </LinearGradient>
            </Defs>
            <G transform="translate(0 -2)" fill="url(#prefix__a)">
              <Path
                data-name="Path 2267"
                d="M12.75 5.825a.85.85 0 01-.85-.85V3.7H8.5v1.275a.85.85 0 11-1.7 0V3.7A1.7 1.7 0 018.5 2h3.4a1.7 1.7 0 011.7 1.7v1.275a.85.85 0 01-.85.85z"
              />
              <Path
                data-name="Path 2268"
                d="M10.8 13.373a1.751 1.751 0 01-.6.1 1.863 1.863 0 01-.655-.119L0 10.177v6.486A2.336 2.336 0 002.338 19h15.725a2.336 2.336 0 002.337-2.337v-6.486z"
              />
              <Path
                data-name="Path 2269"
                d="M20.4 6.888v1.946l-10 3.332a.629.629 0 01-.408 0L0 8.834V6.888A2.336 2.336 0 012.338 4.55h15.725A2.336 2.336 0 0120.4 6.888z"
              />
            </G>
          </Svg>
        </View>
        <View style={{display: 'flex', flexDirection: 'column'}}>
          <Text style={{fontSize: 12, color: '#0DC5A1'}}>Data Science</Text>
          <Text style={{fontSize: 14, color: dark}}>
            Machine Learning Project
          </Text>
        </View>
      </View>
      <Text
        style={{
          fontSize: 12,
          marginVertical: 8,
          width: '100%',
          color: '#707070',
        }}>
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry's standard dummy text ever
        since the 1500s.
      </Text>
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          width: '80%',
          marginVertical: 6,
        }}>
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-end',
          }}>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={24.547}
            height={26.044}>
            <G data-name="building (1)" fill={dark}>
              <Path
                data-name="Path 2270"
                d="M23.784 24.518h-2.013V.763A.763.763 0 0021.008 0H3.539a.763.763 0 00-.763.763v23.755H.763a.763.763 0 000 1.526h23.021a.763.763 0 100-1.526zm-14.111 0v-5.2h5.2v5.2zm6.728 0v-5.965a.763.763 0 00-.763-.763H8.91a.763.763 0 00-.763.763v5.965H4.302V1.526h15.943v22.992z"
              />
              <Path
                data-name="Path 2271"
                d="M10.633 3.373H6.891a.763.763 0 00-.763.763v3.739a.763.763 0 00.763.763h3.739a.763.763 0 00.763-.763V4.136a.763.763 0 00-.76-.763zM9.87 7.112H7.657V4.899H9.87z"
              />
              <Path
                data-name="Path 2272"
                d="M17.653 3.373h-3.739a.763.763 0 00-.763.763v3.739a.763.763 0 00.763.763h3.739a.763.763 0 00.763-.763V4.136a.763.763 0 00-.763-.763zm-.763 3.739h-2.213V4.899h2.213z"
              />
              <Path
                data-name="Path 2273"
                d="M10.633 10.476H6.891a.763.763 0 00-.763.763v3.739a.763.763 0 00.763.763h3.739a.763.763 0 00.763-.763v-3.739a.763.763 0 00-.76-.763zm-.763 3.739H7.657v-2.213H9.87z"
              />
              <Path
                data-name="Path 2274"
                d="M17.653 10.476h-3.739a.763.763 0 00-.763.763v3.739a.763.763 0 00.763.763h3.739a.763.763 0 00.763-.763v-3.739a.763.763 0 00-.763-.763zm-.763 3.739h-2.213v-2.213h2.213z"
              />
            </G>
          </Svg>
          <Text
            style={{
              fontSize: 12,
              color: dark,
              marginLeft: 6,
            }}>
            Technology
          </Text>
        </View>
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-end',
          }}>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={26.044}
            fill={dark}
            height={26.044}>
            <G data-name="Group 998">
              <Path
                data-name="Path 3189"
                d="M15.39 4.947V2.919C15.39-.973 0-.973 0 2.919v11.859c0 1.53 2.4 2.484 5.277 2.823-1.054.473-1.725 1.111-1.725 1.92v3.558c0 1.946 3.871 2.965 7.695 2.965s7.695-1.019 7.695-2.965v-1.788c3.621-.1 7.1-1.109 7.1-2.955V7.663c.002-2.458-6.129-3.363-10.652-2.716zM7.695 1.184c4.213 0 6.511 1.173 6.511 1.776s-2.3 1.776-6.511 1.776-6.511-1.174-6.511-1.777 2.298-1.775 6.511-1.775zM1.184 4.606a13.886 13.886 0 006.511 1.313 13.886 13.886 0 006.511-1.312v.588c-2.057.456-3.551 1.289-3.551 2.5v.35a18.775 18.775 0 01-2.96.242c-4.213 0-6.511-1.173-6.511-1.776v-1.9zm10.654 11.839c.113.068.23.134.356.2-.118 0-.237-.01-.356-.013zM1.184 8.158A13.886 13.886 0 007.695 9.47a20.066 20.066 0 002.96-.232v2.95a18.775 18.775 0 01-2.96.242c-4.213 0-6.511-1.173-6.511-1.776v-2.5zm0 6.64V12.3a13.886 13.886 0 006.511 1.312 20.066 20.066 0 002.96-.232v2.95a18.775 18.775 0 01-2.96.242c-4.213.001-6.511-1.172-6.511-1.772zm16.574 8.287c0 .6-2.3 1.776-6.511 1.776s-6.511-1.173-6.511-1.776v-1.9a13.886 13.886 0 006.511 1.312 14.479 14.479 0 006.324-1.2h.187v1.79zm-6.511-1.776c-4.213 0-6.511-1.173-6.511-1.776s2.3-1.776 6.511-1.776 6.511 1.173 6.511 1.776-2.299 1.776-6.512 1.776zm13.614-2.96c0 .573-2.1 1.649-5.919 1.755v-.571c0-.74-.56-1.337-1.462-1.8.289.012.58.02.87.02a13.886 13.886 0 006.511-1.312v1.9zm0-3.551c0 .6-2.3 1.776-6.511 1.776S11.838 15.4 11.838 14.8v-1.9a13.886 13.886 0 006.511 1.312A13.886 13.886 0 0024.86 12.9zm0-3.551c0 .6-2.3 1.776-6.511 1.776s-6.511-1.173-6.511-1.776v-1.9a13.885 13.885 0 006.511 1.312 13.885 13.885 0 006.51-1.317zM18.349 9.47c-4.213 0-6.511-1.173-6.511-1.776s2.3-1.776 6.511-1.776 6.511 1.174 6.511 1.777-2.298 1.775-6.511 1.775z"
              />
            </G>
          </Svg>

          <Text
            style={{
              fontSize: 12,
              color: dark,
              marginLeft: 6,
            }}>
            $12,2511
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}
