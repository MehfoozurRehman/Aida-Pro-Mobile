import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Svg, {Path, Circle} from 'react-native-svg';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {dark, lightGreen} from '../constants/colors';

export default function UploadImage() {
  const [isUploadImage, setIsUploadImage] = useState(false);
  return (
    <>
      {isUploadImage ? (
        <View
          style={{
            borderRadius: 20,
            height: 150,
            marginVertical: 5,
            justifyContent: 'center',
            backgroundColor: lightGreen,
            alignItems: 'center',
          }}>
          <TouchableOpacity
            style={{padding: 8, flexDirection: 'row'}}
            onPress={() => {
              launchCamera(
                {
                  cameraType: 'back',
                  includeBase64: true,
                },
                () => {},
              );
            }}>
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={20}
              height={20}
              viewBox="0 0 24 24"
              fill="none"
              stroke={dark}
              strokeWidth={1.5}
              style={{marginRight: 10}}
              strokeLinecap="round"
              strokeLinejoin="round">
              <Path d="M23 19a2 2 0 01-2 2H3a2 2 0 01-2-2V8a2 2 0 012-2h4l2-3h6l2 3h4a2 2 0 012 2z" />
              <Circle cx={12} cy={13} r={4} />
            </Svg>
            <Text style={{color: dark}}>Open Camera</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{padding: 8, flexDirection: 'row'}}
            onPress={() => {
              launchImageLibrary(
                {
                  mediaType: 'mixed',
                },
                () => {},
              );
            }}>
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={20}
              height={20}
              viewBox="0 0 24 24"
              fill="none"
              stroke={dark}
              strokeWidth={1.5}
              style={{marginRight: 10}}
              strokeLinecap="round"
              strokeLinejoin="round">
              <Path d="M21.44 11.05l-9.19 9.19a6 6 0 01-8.49-8.49l9.19-9.19a4 4 0 015.66 5.66l-9.2 9.19a2 2 0 01-2.83-2.83l8.49-8.48" />
            </Svg>
            <Text style={{color: dark}}>Open Library</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <TouchableOpacity
          onPress={() => {
            setIsUploadImage(true);
          }}
          style={{
            borderRadius: 20,
            height: 150,
            marginVertical: 5,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: lightGreen,
          }}>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={24}
            height={24}
            fill="none"
            stroke={dark}
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
            className="feather feather-plus">
            <Path d="M12 5v14M5 12h14" />
          </Svg>
          <Text style={{color: dark}}>Upload Image</Text>
        </TouchableOpacity>
      )}
    </>
  );
}
