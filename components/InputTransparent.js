import React, {useState} from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import Svg, {Path, Circle} from 'react-native-svg';
import {white} from '../constants/colors';

export default function InputTransparent({
  placeholder,
  secureTextEntry,
  svg,
  error,
  errorMessage,
}) {
  const [focus, setFocus] = useState(false);
  const [isSecureTextEntry, setIsSecureTextEntry] = useState(secureTextEntry);
  return (
    <View
      style={{
        position: 'relative',
        backgroundColor: 'rgba(0,0,0,.1)',
        paddingHorizontal: 10,
        marginVertical: 6,
        borderRadius: 10,
        width: '100%',
        borderColor: error ? 'red' : focus ? '#F6B038' : '',
        borderWidth: error ? 1 : focus ? 1 : 0,
      }}>
      {error ? (
        <Text
          style={{
            color: 'red',
            position: 'absolute',
            fontSize: 11,
            top: 5,
            right: 10,
          }}>
          {errorMessage}
        </Text>
      ) : null}
      <View
        style={{
          width: '100%',
          height: 45,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        {svg ? (
          <View
            style={{
              width: '10%',
              justifyContent: 'center',
              alignItems: 'center',
              height: '100%',
            }}>
            {svg}
          </View>
        ) : null}
        <>
          <TextInput
            style={{
              flex: 1,
              paddingHorizontal: 0,
              paddingVertical: 0,
              fontSize: 14,
              color: white,
            }}
            returnKeyType="next"
            onFocus={() => {
              setFocus(true);
            }}
            onBlur={() => {
              setFocus(false);
            }}
            placeholder={placeholder}
            secureTextEntry={isSecureTextEntry}
            placeholderTextColor={white}
          />
          {secureTextEntry ? (
            <TouchableOpacity
              onPress={() => {
                isSecureTextEntry
                  ? setIsSecureTextEntry(false)
                  : setIsSecureTextEntry(true);
              }}
              style={{
                width: '10%',
                justifyContent: 'center',
                alignItems: 'center',
                height: '100%',
              }}>
              {isSecureTextEntry ? (
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={24}
                  height={22}
                  fill="none"
                  stroke={white}
                  strokeWidth={1}
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="prefix__feather prefix__feather-eye">
                  <Path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z" />
                  <Circle cx={12} cy={12} r={3} />
                </Svg>
              ) : (
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={24}
                  height={22}
                  fill="none"
                  stroke={white}
                  strokeWidth={1}
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="prefix__feather prefix__feather-eye-off">
                  <Path d="M17.94 17.94A10.07 10.07 0 0112 20c-7 0-11-8-11-8a18.45 18.45 0 015.06-5.94M9.9 4.24A9.12 9.12 0 0112 4c7 0 11 8 11 8a18.5 18.5 0 01-2.16 3.19m-6.72-1.07a3 3 0 11-4.24-4.24M1 1l22 22" />
                </Svg>
              )}
            </TouchableOpacity>
          ) : null}
        </>
      </View>
    </View>
  );
}
