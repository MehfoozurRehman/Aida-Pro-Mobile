import React, {useState} from 'react';
import {TextInput, TouchableOpacity, View} from 'react-native';
import Svg, {Defs, ClipPath, G, Path, Circle} from 'react-native-svg';
import {dark, lightGreen, white} from '../constants/colors';

export default function SearchInput({
  placeholder,
  secureTextEntry,
  svg,
  transparent,
  isFilter,
  style,
  onPressFilter,
}) {
  const [focus, setFocus] = useState(false);
  const [isSecureTextEntry, setIsSecureTextEntry] = useState(secureTextEntry);
  return (
    <View
      style={{
        width: '100%',
        height: 45,
        backgroundColor: transparent ? 'rgba(0,0,0,.09)' : lightGreen,
        borderRadius: 10,
        color: '#fffff',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
        marginVertical: 6,
        borderColor: transparent
          ? focus
            ? '#0DC5A1'
            : ''
          : focus
          ? '#0DC5A1'
          : lightGreen,
        borderWidth: transparent ? (focus ? 1 : 0) : 1,
        ...style,
      }}>
      {svg ? (
        <View
          style={{
            width: '10%',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%',
          }}>
          {svg}
        </View>
      ) : null}
      <>
        <TextInput
          style={{
            flex: 1,
            paddingHorizontal: 10,
            fontSize: 14,
            color: transparent ? white : dark,
          }}
          returnKeyType="next"
          onFocus={() => {
            setFocus(true);
          }}
          onBlur={() => {
            setFocus(false);
          }}
          placeholder={placeholder}
          secureTextEntry={isSecureTextEntry}
          placeholderTextColor={transparent ? white : 'rgba(0,0,0,.5)'}
        />
        {isFilter ? (
          <TouchableOpacity onPress={onPressFilter}>
            <Svg
              data-name="Group 1756"
              xmlns="http://www.w3.org/2000/svg"
              width={22.839}
              height={22.516}>
              <Defs>
                <ClipPath id="prefix__a">
                  <Path
                    data-name="Rectangle 2210"
                    fill="#575757"
                    d="M0 0h22.839v22.516H0z"
                  />
                </ClipPath>
              </Defs>
              <G
                data-name="Group 1755"
                clipPath="url(#prefix__a)"
                fill="#575757">
                <Path
                  data-name="Path 21773"
                  d="M11.939 19.249a7.932 7.932 0 01.07-.858 1.546 1.546 0 011.456-1.288 27.645 27.645 0 012.426 0 1.607 1.607 0 011.552 1.61c.006.166 0 .333 0 .541h4.567a1.781 1.781 0 01.344.025.536.536 0 01-.083 1.062c-.213.01-.427 0-.64 0h-4.164c-.022.292-.02.565-.065.83a1.591 1.591 0 01-1.583 1.339q-1.107.013-2.213 0a1.63 1.63 0 01-1.643-1.632v-.538H.676c-.423 0-.655-.182-.667-.516a.518.518 0 01.434-.55 2.015 2.015 0 01.371-.024h11.125"
                />
                <Path
                  data-name="Path 21774"
                  d="M19.628 2.176H22.1a.546.546 0 11.027 1.086c-.7.013-1.4 0-2.107.005h-.373c-.02.283-.019.539-.059.788a1.561 1.561 0 01-1.5 1.361c-.807.032-1.619.035-2.426 0A1.6 1.6 0 0114.149 3.8v-.532H.8a1.866 1.866 0 01-.4-.031.513.513 0 01-.4-.576.517.517 0 01.48-.481c.106-.009.213 0 .32 0h13.349v-.532A1.625 1.625 0 0115.784.007q1.093-.013 2.187 0a1.632 1.632 0 011.656 1.621c.006.174 0 .348 0 .55"
                />
                <Path
                  data-name="Path 21775"
                  d="M5.381 10.713c.023-.31.022-.576.066-.835a1.577 1.577 0 011.537-1.325 49.8 49.8 0 012.319 0 1.614 1.614 0 011.581 1.608v.552h11.111a3.066 3.066 0 01.373.013.536.536 0 01-.007 1.064 2.9 2.9 0 01-.346.013h-11.13v.506a1.625 1.625 0 01-1.642 1.658q-1.093.021-2.186 0a1.631 1.631 0 01-1.653-1.648v-.513H.728c-.434 0-.668-.2-.666-.548a.52.52 0 01.462-.527 2.421 2.421 0 01.319-.014h4.538"
                />
              </G>
            </Svg>
          </TouchableOpacity>
        ) : null}
        {secureTextEntry ? (
          <TouchableOpacity
            onPress={() => {
              isSecureTextEntry
                ? setIsSecureTextEntry(false)
                : setIsSecureTextEntry(true);
            }}
            style={{
              width: '10%',
              justifyContent: 'center',
              alignItems: 'center',
              height: '100%',
            }}>
            {isSecureTextEntry ? (
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={24}
                height={22}
                fill="none"
                stroke={transparent ? white : dark}
                strokeWidth={1}
                strokeLinecap="round"
                strokeLinejoin="round"
                className="prefix__feather prefix__feather-eye">
                <Path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z" />
                <Circle cx={12} cy={12} r={3} />
              </Svg>
            ) : (
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={24}
                height={22}
                fill="none"
                stroke={transparent ? white : dark}
                strokeWidth={1}
                strokeLinecap="round"
                strokeLinejoin="round"
                className="prefix__feather prefix__feather-eye-off">
                <Path d="M17.94 17.94A10.07 10.07 0 0112 20c-7 0-11-8-11-8a18.45 18.45 0 015.06-5.94M9.9 4.24A9.12 9.12 0 0112 4c7 0 11 8 11 8a18.5 18.5 0 01-2.16 3.19m-6.72-1.07a3 3 0 11-4.24-4.24M1 1l22 22" />
              </Svg>
            )}
          </TouchableOpacity>
        ) : null}
      </>
    </View>
  );
}
