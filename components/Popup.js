import React from 'react';
import {Modal, View, TouchableOpacity, ScrollView, Text} from 'react-native';
import {Shadow} from 'react-native-shadow-2';
import {windowHeight, windowWidth} from '../constants/constants';
import ButtonPrimary from './ButtonPrimary';
import Svg, {Path} from 'react-native-svg';
import LinearGradient from 'react-native-linear-gradient';
import {dark, white} from '../constants/colors';

const Popup = ({
  modalVisible,
  setModalVisible,
  children,
  buttonLabel,
  title,
  onButtonPress,
  small,
  height,
  customButton,
}) => {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      hardwareAccelerated={true}
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible(!modalVisible);
      }}>
      <LinearGradient
        // start={{x: 0, y: 0}}
        // end={{x: 1, y: 0}}
        colors={['#0EE1A390', '#0CA69D90']}
        style={{
          width: windowWidth,
          height: windowHeight,
          justifyContent: 'center',
          padding: 20,
        }}>
        <Shadow
          distance={40}
          startColor={'rgba(0,0,0,.05)'}
          finalColor={'rgba(0,0,0,.01)'}
          offset={[2, 3]}
          containerViewStyle={{backgroundColor: 'rgba(0,0,0,0)'}}>
          <View
            style={{
              width: windowWidth - 40,
              maxHeight: windowHeight - 100,
              borderRadius: 10,
              borderBottomRadius: 0,
              paddingHorizontal: 20,
              paddingTop: 25,
              paddingBottom: 20,
              alignItems: 'center',
              backgroundColor: white,
              position: 'relative',
            }}>
            <TouchableOpacity
              onPress={() => {
                setModalVisible(!modalVisible);
              }}
              style={{
                paddingBottom: 10,
                alignItems: 'center',
                position: 'absolute',
                right: -10,
                top: -10,
              }}>
              <LinearGradient
                start={{x: 0, y: 0}}
                end={{x: 1, y: 0}}
                colors={['#F6B038', '#F5833C']}
                style={{
                  width: 30,
                  height: 30,
                  borderRadius: 50,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={10.435}
                  height={10.435}
                  viewBox="0 0 13.435 13.435">
                  <Path
                    data-name="Icon metro-cross"
                    d="M15.883 12.721l-4.076-4.075 4.076-4.076a.421.421 0 000-.594l-1.925-1.925a.421.421 0 00-.594 0L9.288 6.126 5.213 2.051a.421.421 0 00-.594 0L2.693 3.976a.421.421 0 000 .594l4.076 4.076-4.076 4.075a.421.421 0 000 .594l1.926 1.925a.421.421 0 00.594 0l4.076-4.076 4.076 4.076a.421.421 0 00.594 0l1.925-1.925a.421.421 0 000-.594z"
                    transform="translate(-2.571 -1.928)"
                    fill="#fff"
                  />
                </Svg>
              </LinearGradient>
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 20,
                color: dark,
                width: '100%',
                marginBottom: 5,
                fontWeight: 'bold',
              }}>
              {title}
            </Text>
            <ScrollView style={{width: '100%'}}>{children}</ScrollView>
            {customButton ? customButton : null}
            {buttonLabel ? (
              <ButtonPrimary label={buttonLabel} onPress={onButtonPress} />
            ) : null}
          </View>
        </Shadow>
      </LinearGradient>
    </Modal>
  );
};

export default Popup;
