import React from 'react';
import BouncyCheckbox from 'react-native-bouncy-checkbox';
import {lightGreen, white} from '../constants/colors';

export default function Checkbox({label, variant, onPress}) {
  return (
    <BouncyCheckbox
      size={25}
      fillColor="#0DC5A1"
      unfillColor={variant ? white : lightGreen}
      text={label}
      iconStyle={{borderColor: lightGreen}}
      textStyle={{
        fontFamily: 'JosefinSans-Regular',
        textDecorationLine: 'none',
        fontSize: 13,
      }}
      onPress={onPress}
    />
  );
}
