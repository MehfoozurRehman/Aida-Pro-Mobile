import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {dark, green100, lightGreen} from '../constants/colors';

export default function BlogCard({
  authorName,
  blogName,
  image,
  description,
  onPress,
}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        backgroundColor: lightGreen,
        borderRadius: 20,
        width: '100%',
        marginRight: 16,
        marginBottom: 30,
      }}>
      <Image
        style={{
          width: '100%',
          height: 120,
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        }}
        source={require('../assets/blogImage.png')}
      />
      <View style={{padding: 20}}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View>
            <Text style={{fontSize: 12, color: green100}}>{authorName}</Text>
            <Text style={{fontSize: 14, color: dark, fontWeight: '600'}}>
              {blogName}
            </Text>
          </View>
          <Image
            source={image}
            style={{borderRadius: 45, width: 45, height: 45}}
          />
        </View>
        <Text style={{fontSize: 12, color: '#8B8B8B', marginVertical: 10}}>
          {description}
        </Text>
      </View>
    </TouchableOpacity>
  );
}
