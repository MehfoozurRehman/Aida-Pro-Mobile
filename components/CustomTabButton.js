import React from 'react';
import {Text, Image, TouchableOpacity, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {white} from '../constants/colors';

export default function CustomTabButton({
  label,
  icon,
  navigation,
  activeIcon,
  setSelected,
  selected,
}) {
  return (
    <TouchableOpacity
      onPress={() => {
        navigation ? navigation.navigate(label) : null;
        setSelected(label);
      }}>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={selected === label ? ['#F6B038', '#F5833C'] : [white, white]}
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingVertical: selected === label ? 8 : 0,
          paddingHorizontal: selected === label ? 12 : 0,
          borderRadius: 50,
          flex: 1,
        }}>
        <View style={{marginRight: 10}}>
          <Image
            source={selected === label ? activeIcon : icon}
            style={{width: 20, height: 20, resizeMode: 'contain'}}
          />
        </View>
        {selected === label ? (
          <Text
            numberOfLines={1}
            style={{
              fontSize: 12,
              color: white,
            }}>
            {label}
          </Text>
        ) : null}
      </LinearGradient>
    </TouchableOpacity>
  );
}
