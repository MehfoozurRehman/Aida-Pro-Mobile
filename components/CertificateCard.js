import React from 'react';
import {Image, Text, View} from 'react-native';
import {certificates} from '../assets/icons';
import CertificatesCard from '../components/CertificatesCard';
import {dark, lightGreen} from '../constants/colors';

export default function CertificateCard() {
  return (
    <View
      style={{
        backgroundColor: lightGreen,
        padding: 20,
        borderRadius: 10,
        marginBottom: 20,
      }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: 10,
        }}>
        <Image
          source={certificates}
          style={{width: 50, height: 60, marginRight: 10}}
        />
        <Text style={{fontSize: 12, color: dark}}>Certificate</Text>
      </View>
      <CertificatesCard isNotEditAble={true} />
      <CertificatesCard isNotEditAble={true} />
      <CertificatesCard isNotEditAble={true} />
      <CertificatesCard isNotEditAble={true} />
    </View>
  );
}
