import React from 'react';
import Svg, {Path} from 'react-native-svg';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {dark, lightGreen, white} from '../constants/colors';

function CoffeeCornerCardBadge() {
  return (
    <Text
      style={{
        backgroundColor: '#0DCAA0',
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderRadius: 8,
        color: white,
        marginRight: 10,
        fontSize: 10,
      }}>
      Machine Learning
    </Text>
  );
}
export default function CoffeeCornerCard({onPress, onShare}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        backgroundColor: lightGreen,
        borderRadius: 20,
        width: '100%',
        marginBottom: 20,
        padding: 20,
      }}>
      <Text
        style={{
          fontSize: 18,
          color: dark,
          fontWeight: '600',
        }}>
        Machine Learning Project
      </Text>
      <Text style={{fontSize: 12, color: '#8B8B8B', marginVertical: 10}}>
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry's standard dummy text ever
        since the 1500s, when an unknown printer took a galley of type and
        scrambled it to make a type specimen book. It has survived not only five
        centuries, but also the leap
      </Text>
      <ScrollView horizontal={true} style={{flexDirection: 'row'}}>
        <CoffeeCornerCardBadge />
        <CoffeeCornerCardBadge />
        <CoffeeCornerCardBadge />
        <CoffeeCornerCardBadge />
        <CoffeeCornerCardBadge />
        <CoffeeCornerCardBadge />
        <CoffeeCornerCardBadge />
        <CoffeeCornerCardBadge />
        <CoffeeCornerCardBadge />
        <CoffeeCornerCardBadge />
        <CoffeeCornerCardBadge />
      </ScrollView>
      <View
        style={{
          flexDirection: 'row',
          marginTop: 10,
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity>
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={15.034}
              height={10.31}
              viewBox="0 0 18.034 10.31">
              <Path
                data-name="Icon ionic-ios-arrow-back"
                d="M3.108 9.015L9.932 2.2A1.289 1.289 0 008.106.376L.375 8.1a1.286 1.286 0 00-.037 1.779L8.1 17.658a1.289 1.289 0 001.825-1.82z"
                transform="rotate(90 9.017 9.017)"
                fill={dark}
              />
            </Svg>
          </TouchableOpacity>
          <Text
            style={{
              fontSize: 12,
              color: dark,
              fontWeight: '600',
              marginHorizontal: 10,
            }}>
            3242
          </Text>
          <TouchableOpacity>
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={15.034}
              height={10.31}
              viewBox="0 0 18.034 10.31">
              <Path
                data-name="Icon ionic-ios-arrow-back"
                d="M3.108 9.015L9.932 2.2A1.289 1.289 0 008.106.376L.375 8.1a1.286 1.286 0 00-.037 1.779L8.1 17.658a1.289 1.289 0 001.825-1.82z"
                transform="rotate(-90 5.155 5.155)"
                fill={dark}
              />
            </Svg>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              marginLeft: 20,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={13.252}
              height={13.252}
              viewBox="0 0 13.252 13.252">
              <Path
                data-name="Icon material-mode-comment"
                d="M16.245 4.325A1.323 1.323 0 0014.927 3H4.325A1.329 1.329 0 003 4.325v7.951A1.329 1.329 0 004.325 13.6H13.6l2.65 2.65z"
                transform="translate(-3 -3)"
                fill={dark}
              />
            </Svg>
            <Text
              style={{
                fontSize: 12,
                color: dark,
                fontWeight: '600',
                marginHorizontal: 10,
              }}>
              3242
            </Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={onShare}>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={12.638}
            height={14.443}
            viewBox="0 0 12.638 14.443">
            <Path
              data-name="Icon awesome-share-alt"
              d="M9.929 9.027a2.7 2.7 0 00-1.687.589l-2.89-1.807a2.724 2.724 0 000-1.176l2.891-1.806A2.7 2.7 0 107.286 3.3L4.395 5.1a2.708 2.708 0 100 4.237l2.891 1.807a2.708 2.708 0 102.644-2.12z"
              fill={dark}
            />
          </Svg>
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
  badge: {
    backgroundColor: white,
    padding: 8,
    borderRadius: 10,
    marginRight: 10,
    fontSize: 12,
    color: dark,
  },
});
