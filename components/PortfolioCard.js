import React from 'react';
import {Image, ScrollView, Text, View, TouchableOpacity} from 'react-native';
import {dark, lightGreen} from '../constants/colors';

export default function PortfolioCard({setIsGalleryOpen}) {
  return (
    <View
      style={{
        backgroundColor: lightGreen,
        padding: 20,
        borderRadius: 10,
        marginBottom: 20,
      }}>
      <Text style={{fontSize: 14, color: dark, fontWeight: 'bold'}}>
        Online Store
      </Text>
      <Text style={{fontSize: 12, color: dark, marginBottom: 10}}>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut ero labore et dolore magna aliqua. Ut enim ad minim
        veniam,
      </Text>
      <ScrollView horizontal={true}>
        <TouchableOpacity
          onPress={() => {
            setIsGalleryOpen(true);
          }}>
          <Image
            source={require('../assets/portfiolioPic.png')}
            style={{width: 150, height: 100, borderRadius: 10, marginRight: 10}}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setIsGalleryOpen(true);
          }}>
          <Image
            source={require('../assets/portfiolioPic.png')}
            style={{width: 150, height: 100, borderRadius: 10, marginRight: 10}}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setIsGalleryOpen(true);
          }}>
          <Image
            source={require('../assets/portfiolioPic.png')}
            style={{width: 150, height: 100, borderRadius: 10, marginRight: 10}}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setIsGalleryOpen(true);
          }}>
          <Image
            source={require('../assets/portfiolioPic.png')}
            style={{width: 150, height: 100, borderRadius: 10, marginRight: 10}}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setIsGalleryOpen(true);
          }}>
          <Image
            source={require('../assets/portfiolioPic.png')}
            style={{width: 150, height: 100, borderRadius: 10, marginRight: 10}}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setIsGalleryOpen(true);
          }}>
          <Image
            source={require('../assets/portfiolioPic.png')}
            style={{width: 150, height: 100, borderRadius: 10, marginRight: 10}}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setIsGalleryOpen(true);
          }}>
          <Image
            source={require('../assets/portfiolioPic.png')}
            style={{width: 150, height: 100, borderRadius: 10, marginRight: 10}}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setIsGalleryOpen(true);
          }}>
          <Image
            source={require('../assets/portfiolioPic.png')}
            style={{width: 150, height: 100, borderRadius: 10, marginRight: 10}}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setIsGalleryOpen(true);
          }}>
          <Image
            source={require('../assets/portfiolioPic.png')}
            style={{width: 150, height: 100, borderRadius: 10, marginRight: 10}}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setIsGalleryOpen(true);
          }}>
          <Image
            source={require('../assets/portfiolioPic.png')}
            style={{width: 150, height: 100, borderRadius: 10, marginRight: 10}}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setIsGalleryOpen(true);
          }}>
          <Image
            source={require('../assets/portfiolioPic.png')}
            style={{width: 150, height: 100, borderRadius: 10, marginRight: 10}}
          />
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}
