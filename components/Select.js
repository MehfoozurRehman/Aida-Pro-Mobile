import React, {useState} from 'react';
import Svg, {Circle, Path} from 'react-native-svg';
import {Shadow} from 'react-native-shadow-2';
import {windowHeight, windowWidth} from '../constants/constants';
import ButtonPrimary from './ButtonPrimary';
import LinearGradient from 'react-native-linear-gradient';
import {
  Modal,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {dark, green100, green200, lightGreen, white} from '../constants/colors';

function OptionsItem({name, id}) {
  const [isSelected, setIsSelected] = useState(false);
  return (
    <TouchableOpacity
      onPress={() => {
        isSelected ? setIsSelected(false) : setIsSelected(true);
      }}
      style={{
        marginBottom: 10,
      }}>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={isSelected ? [green100, green200] : [lightGreen, lightGreen]}
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingVertical: 10,
          paddingHorizontal: 20,
          borderRadius: 50,
        }}>
        <Text
          style={{
            color: isSelected ? white : dark,
            fontWeight: isSelected ? 'bold' : 'normal',
          }}>
          {name}
        </Text>
        {isSelected ? (
          <View
            style={{
              backgroundColor: white,
              borderRadius: 50,
              width: 22,
              height: 22,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={15}
              height={15}
              viewBox="0 0 24 24"
              fill="none"
              stroke={green200}
              strokeWidth={2}
              strokeLinecap="round"
              strokeLinejoin="round"
              className="feather feather-check">
              <Path d="M20 6L9 17 4 12" />
            </Svg>
          </View>
        ) : null}
      </LinearGradient>
    </TouchableOpacity>
  );
}

const options = [
  {
    id: '92iijs7yta',
    name: 'Ondo',
  },
  {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
  },
  {
    id: '16hbajsabsd',
    name: 'Calabar',
  },
  {
    id: 'nahs75a5sg',
    name: 'Lagos',
  },
  {
    id: '667atsas',
    name: 'Maiduguri',
  },
  {
    id: 'hsyasajs',
    name: 'Anambra',
  },
  {
    id: 'djsjudksjd',
    name: 'Benue',
  },
  {
    id: 'sdhyaysdj',
    name: 'Kaduna',
  },
  {
    id: 'suudydjsjd',
    name: 'Abuja',
  },
  {
    id: '92iijs7yta',
    name: 'Ondo',
  },
  {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
  },
  {
    id: '16hbajsabsd',
    name: 'Calabar',
  },
  {
    id: 'nahs75a5sg',
    name: 'Lagos',
  },
  {
    id: '667atsas',
    name: 'Maiduguri',
  },
  {
    id: 'hsyasajs',
    name: 'Anambra',
  },
  {
    id: 'djsjudksjd',
    name: 'Benue',
  },
  {
    id: 'sdhyaysdj',
    name: 'Kaduna',
  },
  {
    id: 'suudydjsjd',
    name: 'Abuja',
  },
  {
    id: '92iijs7yta',
    name: 'Ondo',
  },
  {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
  },
  {
    id: '16hbajsabsd',
    name: 'Calabar',
  },
  {
    id: 'nahs75a5sg',
    name: 'Lagos',
  },
  {
    id: '667atsas',
    name: 'Maiduguri',
  },
  {
    id: 'hsyasajs',
    name: 'Anambra',
  },
  {
    id: 'djsjudksjd',
    name: 'Benue',
  },
  {
    id: 'sdhyaysdj',
    name: 'Kaduna',
  },
  {
    id: 'suudydjsjd',
    name: 'Abuja',
  },
  {
    id: '92iijs7yta',
    name: 'Ondo',
  },
  {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
  },
  {
    id: '16hbajsabsd',
    name: 'Calabar',
  },
  {
    id: 'nahs75a5sg',
    name: 'Lagos',
  },
  {
    id: '667atsas',
    name: 'Maiduguri',
  },
  {
    id: 'hsyasajs',
    name: 'Anambra',
  },
  {
    id: 'djsjudksjd',
    name: 'Benue',
  },
  {
    id: 'sdhyaysdj',
    name: 'Kaduna',
  },
  {
    id: 'suudydjsjd',
    name: 'Abuja',
  },
  {
    id: '92iijs7yta',
    name: 'Ondo',
  },
  {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
  },
  {
    id: '16hbajsabsd',
    name: 'Calabar',
  },
  {
    id: 'nahs75a5sg',
    name: 'Lagos',
  },
  {
    id: '667atsas',
    name: 'Maiduguri',
  },
  {
    id: 'hsyasajs',
    name: 'Anambra',
  },
  {
    id: 'djsjudksjd',
    name: 'Benue',
  },
  {
    id: 'sdhyaysdj',
    name: 'Kaduna',
  },
  {
    id: 'suudydjsjd',
    name: 'Abuja',
  },
];

export default function Select({
  placeholder,
  height,
  error,
  errorMessage,
  label,
}) {
  const [openModal, setOpenModal] = useState(false);
  const [itemsSelected, setItemsSelected] = useState(false);
  return (
    <>
      <View
        style={{
          marginVertical: 6,
          position: 'relative',
          borderWidth: 1,
          borderRadius: 10,
          borderColor: error ? 'red' : lightGreen,
          backgroundColor: lightGreen,
        }}>
        {error ? (
          <Text
            style={{
              color: 'red',
              position: 'absolute',
              top: 5,
              right: 8,
              fontSize: 11,
            }}>
            {errorMessage}
          </Text>
        ) : null}
        <Text
          style={{
            color: dark,
            marginBottom: 5,
            fontSize: 12,
            position: 'absolute',
            top: 5,
            left: 10,
          }}>
          {label ? label : placeholder}
        </Text>
        <View
          onPress={() => {
            setOpenModal(true);
          }}
          style={{
            width: '100%',
            marginTop: 20,
            borderRadius: 10,
            paddingVertical: 6,
            paddingHorizontal: 10,
            flexDirection: 'row',
          }}>
          {itemsSelected ? (
            <ScrollView horizontal={true} style={{marginRight: 25}}>
              <View
                style={{
                  height: '100%',
                  alignItems: 'center',
                  backgroundColor: green200,
                  paddingHorizontal: 10,
                  borderRadius: 5,
                  flexDirection: 'row',
                  marginRight: 10,
                  height: 30,
                }}>
                <Text
                  style={{
                    color: white,
                    fontSize: 13,
                    fontWeight: 'bold',
                  }}>
                  {placeholder}
                </Text>
                <TouchableOpacity
                  style={{
                    marginLeft: 5,
                    borderRadius: 50,
                    backgroundColor: dark,
                    padding: 1,
                  }}>
                  <Svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={15}
                    height={15}
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke={white}
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-x">
                    <Path d="M18 6L6 18" />
                    <Path d="M6 6L18 18" />
                  </Svg>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  height: '100%',
                  alignItems: 'center',
                  backgroundColor: green200,
                  paddingHorizontal: 10,
                  borderRadius: 5,
                  flexDirection: 'row',
                  marginRight: 10,
                  height: 30,
                }}>
                <Text
                  style={{
                    color: white,
                    fontSize: 13,
                    fontWeight: 'bold',
                  }}>
                  {placeholder}
                </Text>
                <TouchableOpacity
                  style={{
                    marginLeft: 5,
                    borderRadius: 50,
                    backgroundColor: dark,
                    padding: 1,
                  }}>
                  <Svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={15}
                    height={15}
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke={white}
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-x">
                    <Path d="M18 6L6 18" />
                    <Path d="M6 6L18 18" />
                  </Svg>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  height: '100%',
                  alignItems: 'center',
                  backgroundColor: green200,
                  paddingHorizontal: 10,
                  borderRadius: 5,
                  flexDirection: 'row',
                  marginRight: 10,
                  height: 30,
                }}>
                <Text
                  style={{
                    color: white,
                    fontSize: 13,
                    fontWeight: 'bold',
                  }}>
                  {placeholder}
                </Text>
                <TouchableOpacity
                  style={{
                    marginLeft: 5,
                    borderRadius: 50,
                    backgroundColor: dark,
                    padding: 1,
                  }}>
                  <Svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={15}
                    height={15}
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke={white}
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-x">
                    <Path d="M18 6L6 18" />
                    <Path d="M6 6L18 18" />
                  </Svg>
                </TouchableOpacity>
              </View>
            </ScrollView>
          ) : (
            <TouchableOpacity
              onPress={() => {
                setOpenModal(true);
              }}
              style={{flex: 1, justifyContent: 'center'}}>
              <Text
                style={{
                  color: 'rgba(0,0,0,.5)',
                  fontWeight: 'bold',
                  fontSize: 14,
                }}>
                {placeholder}
              </Text>
            </TouchableOpacity>
          )}
        </View>
        <TouchableOpacity
          onPress={() => {
            setOpenModal(true);
          }}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginLeft: 7,
            position: 'absolute',
            top: itemsSelected ? 25 : 16,
            right: 10,
          }}>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={20}
            height={20}
            viewBox="0 0 24 24"
            fill="none"
            stroke={dark}
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
            className="feather feather-chevron-down">
            <Path d="M6 9L12 15 18 9" />
          </Svg>
        </TouchableOpacity>
      </View>
      <Modal visible={openModal} transparent={true}>
        <LinearGradient
          // start={{x: 0, y: 0}}
          // end={{x: 1, y: 0}}
          colors={['#0EE1A390', '#0CA69D90']}
          style={{
            width: windowWidth,
            height: windowHeight,
            justifyContent: 'center',
            padding: 20,
          }}>
          <Shadow
            distance={40}
            startColor={'rgba(0,0,0,.05)'}
            finalColor={'rgba(0,0,0,.01)'}
            offset={[2, 3]}>
            <View
              style={{
                backgroundColor: white,
                width: windowWidth - 40,
                height: height ? height : windowHeight - 40,
                borderRadius: 20,
                position: 'relative',
              }}>
              <TouchableOpacity
                onPress={() => {
                  setOpenModal(!openModal);
                }}
                style={{
                  paddingBottom: 10,
                  alignItems: 'center',
                  position: 'absolute',
                  right: -10,
                  top: -10,
                }}>
                <LinearGradient
                  start={{x: 0, y: 0}}
                  end={{x: 1, y: 0}}
                  colors={['#F6B038', '#F5833C']}
                  style={{
                    width: 30,
                    height: 30,
                    borderRadius: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={10.435}
                    height={10.435}
                    viewBox="0 0 13.435 13.435">
                    <Path
                      data-name="Icon metro-cross"
                      d="M15.883 12.721l-4.076-4.075 4.076-4.076a.421.421 0 000-.594l-1.925-1.925a.421.421 0 00-.594 0L9.288 6.126 5.213 2.051a.421.421 0 00-.594 0L2.693 3.976a.421.421 0 000 .594l4.076 4.076-4.076 4.075a.421.421 0 000 .594l1.926 1.925a.421.421 0 00.594 0l4.076-4.076 4.076 4.076a.421.421 0 00.594 0l1.925-1.925a.421.421 0 000-.594z"
                      transform="translate(-2.571 -1.928)"
                      fill="#fff"
                    />
                  </Svg>
                </LinearGradient>
              </TouchableOpacity>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  marginTop: 15,
                  marginHorizontal: 20,
                }}>
                <Text style={{fontSize: 20, color: dark, fontWeight: 'bold'}}>
                  {placeholder}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginHorizontal: 20,
                  paddingHorizontal: 10,
                  borderRadius: 50,
                  height: 40,
                  marginVertical: 5,
                  backgroundColor: white,
                  borderWidth: 1,
                  borderColor: '#CFCFCF',
                  marginBottom: 10,
                }}>
                <TouchableOpacity>
                  <Svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={20}
                    height={20}
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="#CFCFCF"
                    strokeWidth={1}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-search">
                    <Circle cx={11} cy={11} r={8} />
                    <Path d="M21 21L16.65 16.65" />
                  </Svg>
                </TouchableOpacity>
                <TextInput
                  placeholder="Search"
                  placeholderTextColor="#CFCFCF"
                  style={{
                    flex: 1,
                    height: '100%',
                    padding: 0,
                    marginLeft: 5,
                    color: dark,
                  }}
                />
              </View>
              <ScrollView style={{flex: 1, paddingHorizontal: 20}}>
                {options.map((option, i) => (
                  <OptionsItem key={i} name={option.name} id={option.id} />
                ))}
              </ScrollView>
              <View style={{marginHorizontal: 20, marginVertical: 5}}>
                <ButtonPrimary
                  label="Save"
                  onPress={() => {
                    setOpenModal(false);
                    setItemsSelected(true);
                  }}
                />
              </View>
            </View>
          </Shadow>
        </LinearGradient>
      </Modal>
    </>
  );
}
