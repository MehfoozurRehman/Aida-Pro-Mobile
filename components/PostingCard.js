import React, {useState} from 'react';
import Svg, {Defs, LinearGradient, Stop, Path, Circle} from 'react-native-svg';
import LinearGradientComp from 'react-native-linear-gradient';
import {Shadow} from 'react-native-shadow-2';
import {windowHeight, windowWidth} from '../constants/constants';
import {Modal, Text, TouchableOpacity, View} from 'react-native';
import {dark, green100, green200, lightGreen, white} from '../constants/colors';

function PostingCardEntry({title, text, onPress, activeOpacity}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={activeOpacity}
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 6,
        paddingVertical: 6,
        paddingHorizontal: 10,
        backgroundColor: lightGreen,
        borderRadius: 20,
      }}>
      <Text style={{fontSize: 12, color: dark}}>{title}</Text>
      <Text style={{fontSize: 12, color: dark}}>{text}</Text>
    </TouchableOpacity>
  );
}

function OptionsItem({name, id, image, code, onPress}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        paddingVertical: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 22,
        alignItems: 'center',
        width: '100%',
      }}>
      <LinearGradientComp
        colors={
          id === 'live'
            ? ['#0DC5A1', green200]
            : id === 'draft'
            ? ['#F6B038', '#F5833C']
            : id === 'hold'
            ? ['#FFDD00', '#E3EB00']
            : id === 'closed'
            ? ['#DE3F3F', '#FF6161']
            : ['#0DC5A1', green200]
        }
        style={{width: 10, height: 10, borderRadius: 10, marginRight: 10}}
      />
      <Text
        style={{
          color: dark,
          fontWeight: 'normal',
          flex: 1,
        }}>
        {name}
      </Text>
    </TouchableOpacity>
  );
}
export default function PostingCard({navigation, onDelete}) {
  const [status, setStatus] = useState('Live');
  const [openModal, setOpenModal] = useState(false);
  const options = [
    {
      name: 'Live',
      id: 'live',
    },
    {
      name: 'Draft',
      id: 'draft',
    },
    {
      name: 'Hold',
      id: 'hold',
    },
    {
      name: 'Closed',
      id: 'closed',
    },
  ];
  return (
    <View
      style={{
        width: '100%',
        borderColor: lightGreen,
        borderWidth: 1.5,
        borderRadius: 10,
        padding: 20,
        marginBottom: 15,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontSize: 16,
              fontWeight: 'bold',
              marginRight: 10,
              color: dark,
            }}>
            Sales Manager
          </Text>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity
            style={{marginRight: 8}}
            onPress={() => {
              navigation.navigate('Post A Job');
            }}>
            <Svg
              data-name="edit (2)"
              xmlns="http://www.w3.org/2000/svg"
              width={24}
              height={24}>
              <Defs>
                <LinearGradient
                  id="a"
                  x1={0.5}
                  x2={0.5}
                  y2={1}
                  gradientUnits="objectBoundingBox">
                  <Stop offset={0} stopColor={green100} />
                  <Stop offset={1} stopColor={green200} />
                </LinearGradient>
              </Defs>
              <Path
                data-name="Path 713"
                d="M19.865 50.526a.536.536 0 0 0-.536.536v4.76a1.61 1.61 0 0 1-1.608 1.608H2.68a1.61 1.61 0 0 1-1.608-1.608v-13.97a1.61 1.61 0 0 1 1.608-1.608h4.76a.536.536 0 1 0 0-1.072H2.68A2.683 2.683 0 0 0 0 41.852v13.969A2.683 2.683 0 0 0 2.68 58.5h15.041a2.683 2.683 0 0 0 2.68-2.68v-4.76a.536.536 0 0 0-.536-.536Zm0 0"
                transform="translate(0 -37.086)"
                fill="url(#a)"
              />
              <Path
                data-name="Path 714"
                d="M122.58.967a2.412 2.412 0 0 0-3.411 0l-9.569 9.564a.536.536 0 0 0-.138.236l-1.258 4.54a.536.536 0 0 0 .66.66l4.54-1.258a.536.536 0 0 0 .236-.138l9.563-9.564a2.415 2.415 0 0 0 0-3.411Zm-11.807 9.912 7.827-7.827 2.524 2.524L113.3 13.4Zm-.5 1.012 2.017 2.017-2.79.773Zm12.178-7.641-.569.569-2.524-2.524.569-.569a1.34 1.34 0 0 1 1.9 0l.629.629a1.342 1.342 0 0 1-.005 1.895Zm0 0"
                transform="translate(-102.391 -.261)"
                fill="url(#a)"
              />
            </Svg>
          </TouchableOpacity>
          <TouchableOpacity onPress={onDelete}>
            <Svg xmlns="http://www.w3.org/2000/svg" width={24} height={24}>
              <Defs>
                <LinearGradient
                  id="a"
                  x1={0.5}
                  x2={0.5}
                  y2={1}
                  gradientUnits="objectBoundingBox">
                  <Stop offset={0} stopColor={dark} />
                  <Stop offset={1} stopColor={dark} />
                </LinearGradient>
              </Defs>
              <Path
                data-name="Icon material-delete"
                d="M8.668 23.187A2.343 2.343 0 0 0 11 25.523h9.343a2.343 2.343 0 0 0 2.336-2.336V9.172H8.668ZM23.851 5.668h-4.088L18.6 4.5h-5.84l-1.172 1.168H7.5V8h16.351Z"
                transform="translate(-7.5 -4.5)"
                fill="url(#a)"
              />
            </Svg>
          </TouchableOpacity>
        </View>
      </View>
      <PostingCardEntry
        onPress={() => {
          navigation.navigate('Posting List');
        }}
        title="Applicants"
        text="10"
      />
      <PostingCardEntry
        onPress={() => {
          navigation.navigate('Posting List');
        }}
        title="Interested"
        text="20"
      />
      <PostingCardEntry
        onPress={() => {
          navigation.navigate('Posting List');
        }}
        title="Visitors"
        text="25"
      />
      <PostingCardEntry title="Created" text="4 Days Ago" activeOpacity={1} />
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginTop: 6,
          paddingVertical: 6,
          paddingHorizontal: 10,
          backgroundColor: lightGreen,
          borderRadius: 20,
        }}>
        <Modal visible={openModal} transparent={true}>
          <LinearGradientComp
            // start={{x: 0, y: 0}}
            // end={{x: 1, y: 0}}
            colors={['#0EE1A390', '#0CA69D90']}
            style={{
              width: windowWidth,
              height: windowHeight,
              justifyContent: 'center',
              padding: 20,
            }}>
            <Shadow
              distance={40}
              startColor={'rgba(0,0,0,.05)'}
              finalColor={'rgba(0,0,0,.01)'}
              offset={[2, 3]}>
              <View
                style={{
                  backgroundColor: white,
                  width: windowWidth - 40,
                  borderRadius: 10,
                  paddingBottom: 15,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginTop: 15,
                    marginHorizontal: 20,
                  }}>
                  <Text
                    style={{
                      fontSize: 20,
                      color: dark,
                      fontWeight: 'bold',
                    }}>
                    Posting List
                  </Text>
                </View>
                <TouchableOpacity
                  onPress={() => {
                    setOpenModal(false);
                  }}
                  style={{
                    paddingBottom: 10,
                    alignItems: 'center',
                    position: 'absolute',
                    right: -10,
                    top: -10,
                  }}>
                  <LinearGradientComp
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 0}}
                    colors={['#F6B038', '#F5833C']}
                    style={{
                      width: 30,
                      height: 30,
                      borderRadius: 50,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={10.435}
                      height={10.435}
                      viewBox="0 0 13.435 13.435">
                      <Path
                        data-name="Icon metro-cross"
                        d="M15.883 12.721l-4.076-4.075 4.076-4.076a.421.421 0 000-.594l-1.925-1.925a.421.421 0 00-.594 0L9.288 6.126 5.213 2.051a.421.421 0 00-.594 0L2.693 3.976a.421.421 0 000 .594l4.076 4.076-4.076 4.075a.421.421 0 000 .594l1.926 1.925a.421.421 0 00.594 0l4.076-4.076 4.076 4.076a.421.421 0 00.594 0l1.925-1.925a.421.421 0 000-.594z"
                        transform="translate(-2.571 -1.928)"
                        fill="#fff"
                      />
                    </Svg>
                  </LinearGradientComp>
                </TouchableOpacity>

                {options.map((option, i) => (
                  <OptionsItem
                    key={i}
                    name={option.name}
                    id={option.id}
                    onPress={() => {
                      setStatus(option.id);
                      setOpenModal(false);
                    }}
                  />
                ))}
              </View>
            </Shadow>
          </LinearGradientComp>
        </Modal>
        <Text style={{fontSize: 12, color: dark}}>Status</Text>
        <TouchableOpacity
          onPress={() => {
            setOpenModal(true);
          }}>
          <LinearGradientComp
            // start={{x: 0, y: 0}}
            // end={{x: 1, y: 0}}
            colors={
              status === 'live'
                ? ['#0DC5A1', green200]
                : status === 'draft'
                ? ['#F6B038', '#F5833C']
                : status === 'hold'
                ? ['#FFDD00', '#E3EB00']
                : status === 'closed'
                ? ['#DE3F3F', '#FF6161']
                : ['#0DC5A1', green200]
            }
            style={{
              borderRadius: 5,
              paddingHorizontal: 12,
              paddingVertical: 5,
            }}>
            <Text
              style={{
                fontSize: 11,
                textTransform: 'capitalize',
                color: white,
              }}>
              {status}
            </Text>
          </LinearGradientComp>
        </TouchableOpacity>
      </View>
    </View>
  );
}
