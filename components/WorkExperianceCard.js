import React from 'react';
import {Image, Text, View} from 'react-native';
import {dark, lightGreen} from '../constants/colors';

export default function WorkExperianceCard() {
  return (
    <View
      style={{
        backgroundColor: lightGreen,
        padding: 20,
        borderRadius: 10,
        marginBottom: 20,
      }}>
      <View
        style={{flexDirection: 'row', alignItems: 'center', marginBottom: 10}}>
        <Image
          source={require('../assets/experiancePic.png')}
          style={{width: 80, height: 80, borderRadius: 10, marginRight: 20}}
        />
        <View>
          <Text style={{fontSize: 14, color: dark, fontWeight: 'bold'}}>
            AIESEC
          </Text>
          <Text
            style={{
              fontSize: 12,
              color: '#0DC5A1',
              textDecorationLine: 'underline',
            }}>
            1 year 1 mo
          </Text>
          <Text style={{fontSize: 12, color: dark}}>Technical Head</Text>
          <Text style={{fontSize: 12, color: '#0DC5A1'}}>2009 - 2010</Text>
        </View>
      </View>
      <Text style={{fontSize: 12, color: dark}}>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut ero labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco poriti laboris nisi ut aliquip
        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
        uienply voluptate velit esse cillum dolore eu fugiat nulla pariatur.
        Excepteur sint occaecat cupidatat norin proident, sunt in culpa qui
        officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet,
        consectetur adipisicing elit, sed do eiusmod
      </Text>
    </View>
  );
}
