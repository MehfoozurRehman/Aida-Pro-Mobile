import React, {useState} from 'react';
import {Modal, Text, TouchableOpacity, View} from 'react-native';
import Svg, {Path} from 'react-native-svg';
import {Shadow} from 'react-native-shadow-2';
import {windowWidth} from '../constants/constants';
import DatePicker from 'react-native-date-picker';
import ButtonPrimary from './ButtonPrimary';
import {dark, green200, lightGreen, white} from '../constants/colors';

export default function TimeDateInput({
  label,
  placeholder,
  mode,
  error,
  errorMessage,
}) {
  const [openModal, setOpenModal] = useState(false);
  const [date, setDate] = useState(new Date());
  const [placeholderValue, setPlaceholderValue] = useState(placeholder);

  return (
    <>
      <View
        style={{
          marginVertical: 6,
          position: 'relative',
          backgroundColor: lightGreen,
          paddingHorizontal: 10,
          paddingVertical: 5,
          borderRadius: 10,
          borderWidth: 1,
          borderColor: error ? 'red' : lightGreen,
        }}>
        {error ? (
          <Text
            style={{
              color: 'red',
              position: 'absolute',
              top: 5,
              right: 8,
              fontSize: 11,
            }}>
            {errorMessage}
          </Text>
        ) : null}
        <Text
          style={{
            color: error ? 'red' : dark,
            fontSize: 12,
            marginBottom: 0,
          }}>
          {label ? label : placeholder}
        </Text>
        <View
          onPress={() => {
            setOpenModal(true);
          }}
          style={{
            backgroundColor: lightGreen,
            width: '100%',
            borderRadius: 10,
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            onPress={() => {
              setOpenModal(true);
            }}
            style={{flex: 1, justifyContent: 'center', paddingBottom: 5}}>
            <Text
              style={{
                color: 'rgba(0,0,0,.5)',
                fontWeight: 'bold',
              }}>
              {placeholderValue}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <Modal visible={openModal} transparent={true}>
        <View
          style={{
            backgroundColor: 'rgba(225,225,225,.9)',
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Shadow
            distance={40}
            startColor={'rgba(0,0,0,.05)'}
            finalColor={'rgba(0,0,0,.01)'}
            offset={[2, 3]}>
            <View
              style={{
                backgroundColor: white,
                width: windowWidth - 40,
                height: 350,
                borderRadius: 20,
                borderWidth: 2,
                borderColor: green200,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  marginTop: 15,
                  marginHorizontal: 20,
                }}>
                <Text style={{fontSize: 20, color: dark, fontWeight: 'bold'}}>
                  {placeholder}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    setOpenModal(false);
                  }}>
                  <Svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={20}
                    height={20}
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke={dark}
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-x">
                    <Path d="M18 6L6 18" />
                    <Path d="M6 6L18 18" />
                  </Svg>
                </TouchableOpacity>
              </View>
              <View style={{padding: 20, alignItems: 'center'}}>
                <DatePicker
                  date={date}
                  mode={mode}
                  style={{width: 250}}
                  onDateChange={date => {
                    setDate(date);
                  }}
                />
              </View>
              <View style={{marginHorizontal: 20, marginVertical: 5}}>
                <ButtonPrimary
                  label="Save"
                  onPress={() => {
                    setOpenModal(false);
                    setPlaceholderValue(date.toUTCString());
                  }}
                />
              </View>
            </View>
          </Shadow>
        </View>
      </Modal>
    </>
  );
}
