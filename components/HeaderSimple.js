import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import Svg, {G, Path} from 'react-native-svg';
import {dark} from '../constants/colors';

export default function HeaderSimple({title, backPath}) {
  const navigation = useNavigation();
  return (
    <View
      style={{
        width: '100%',
        paddingVertical: 20,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
      }}>
      <TouchableOpacity
        style={{padding: 5}}
        onPress={() => {
          navigation.navigate(backPath ? backPath : 'Home');
        }}>
        <Svg xmlns="http://www.w3.org/2000/svg" width={20.012} height={17.163}>
          <G data-name="Group 505">
            <Path
              data-name="Path 730"
              d="M18.809 7.176a1.43 1.43 0 00-.247-.018H4.443l.308-.143a2.864 2.864 0 00.809-.573l3.959-3.959a1.482 1.482 0 00.208-1.9A1.432 1.432 0 007.579.397L.42 7.558a1.432 1.432 0 000 2.025l7.159 7.159a1.432 1.432 0 002.148-.143 1.482 1.482 0 00-.208-1.9l-3.952-3.961a2.864 2.864 0 00-.716-.523l-.43-.193h14.062a1.482 1.482 0 001.511-1.2 1.432 1.432 0 00-1.185-1.646z"
              fill="#333"
            />
          </G>
        </Svg>
      </TouchableOpacity>
      <Text style={{fontSize: 20, fontWeight: '600', color: dark}}>
        {title}
      </Text>
    </View>
  );
}
