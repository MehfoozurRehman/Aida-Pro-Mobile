import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import QuillEditor, {QuillToolbar} from 'react-native-cn-quill';
import {dark, lightGreen} from '../constants/colors';

export default function Textarea({placeholder, error, errorMessage}) {
  const _editor = React.createRef();

  return (
    <View
      style={{
        marginVertical: 6,
        position: 'relative',
        borderWidth: error ? 1 : 1.3,
        borderColor: error ? 'red' : lightGreen,
        marginVertical: 2,
        borderRadius: 10,
        backgroundColor: lightGreen,
        paddingHorizontal: 8,
        paddingTop: 5,
        paddingBottom: 0,
      }}>
      {error ? (
        <Text
          style={{
            color: 'red',
            position: 'absolute',
            top: 5,
            right: 8,
            fontSize: 11,
          }}>
          {errorMessage}
        </Text>
      ) : null}
      <Text
        style={{
          color: dark,
          fontSize: 12,
          marginBottom: 7,
        }}>
        {placeholder}
      </Text>
      <View
        style={{
          flex: 1,
          height: 200,
        }}>
        <QuillEditor
          style={{
            flex: 1,
            padding: 0,
            backgroundColor: dark,
          }}
          ref={_editor}
          placeholder={placeholder}
        />
        <QuillToolbar
          editor={_editor}
          options="full"
          theme="light"
          styles={{
            toolbar: {
              provider: provided => ({
                ...provided,
                borderTopWidth: 0,
                borderLeftWidth: 0,
                borderRadius: 10,
                backgroundColor: lightGreen,
              }),
              root: provided => ({
                ...provided,
                borderTopWidth: 0,
                backgroundColor: lightGreen,
                marginBottom: 1,
                borderBottomLeftRadius: 10,
                marginLeft: 1,
              }),
            },
          }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({});
