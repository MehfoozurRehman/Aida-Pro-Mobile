import React from 'react';
import {View, Text} from 'react-native';
import {dark, lightGreen} from '../constants/colors';
import ButtonPrimary from './ButtonPrimary';

export default function ProfessionalDetailsCard({
  svg,
  text,
  btnLabel,
  onPress,
  children,
}) {
  return (
    <View
      style={{
        padding: 20,
        backgroundColor: lightGreen,
        borderRadius: 10,
        marginVertical: 10,
      }}>
      <View
        style={{
          width: '100%',
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {svg}
          <Text
            style={{
              fontSize: 14,
              fontWeight: 'bold',
              color: dark,
              marginLeft: 10,
            }}>
            {text}
          </Text>
        </View>
        <ButtonPrimary
          onPress={onPress}
          style={{height: 40, width: 90}}
          label={btnLabel}
        />
      </View>
      <View>{children}</View>
    </View>
  );
}
