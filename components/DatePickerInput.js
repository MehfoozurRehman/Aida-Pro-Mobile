import React, {useState} from 'react';
import {View, Text} from 'react-native';
import DatePicker from 'react-native-date-picker';

export default function DatePickerInput({label}) {
  const [date, setDate] = useState(new Date());
  return (
    <View
      style={{
        width: '100%',
        height: 50,
        borderRadius: 50,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20,
        marginVertical: 6,
        borderColor: '#C1C1C1',
        borderWidth: 1,
      }}>
      <Text style={{fontSize: 18, color: '#A1A1A1', marginLeft: 10}}>
        {label}
      </Text>
      <DatePicker
        style={{width: 200, height: 45}}
        date={date}
        mode="date"
        textColor="#A1A1A1"
        placeholder="Select date"
        format="YYYY-MM-DD"
        onDateChange={date => {
          ({date: date});
        }}
      />
    </View>
  );
}
