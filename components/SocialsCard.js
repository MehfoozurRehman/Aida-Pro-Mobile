import React from 'react';
import {Image, Text, View} from 'react-native';
import {dark, white} from '../constants/colors';

export default function SocialsCard({imgSouce, text}) {
  return (
    <View
      style={{
        padding: 16,
        borderRadius: 10,
        backgroundColor: white,
        position: 'relative',
        marginVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
      }}>
      <Image
        source={imgSouce}
        style={{width: 30, height: 30, marginRight: 10}}
      />
      <Text style={{fontSize: 12, color: dark, fontWeight: 'bold'}}>
        {text}
      </Text>
    </View>
  );
}
