import React from 'react';
import {Image, Text, View} from 'react-native';
import {skills} from '../assets/icons';
import {dark, lightGreen, white} from '../constants/colors';

export default function SkillsCard() {
  return (
    <View
      style={{
        backgroundColor: lightGreen,
        padding: 20,
        borderRadius: 10,
        marginBottom: 20,
      }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: 10,
        }}>
        <Image
          source={skills}
          style={{width: 55, height: 60, marginRight: 10}}
        />
        <Text>Skills</Text>
      </View>
      <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
        <Text
          style={{
            fontSize: 12,
            color: dark,
            backgroundColor: white,
            padding: 10,
            paddingHorizontal: 20,
            flexDirection: 'row',
            alignItems: 'center',
            borderRadius: 50,
            marginBottom: 10,
          }}>
          Graphic Designing
        </Text>
        <Text
          style={{
            fontSize: 12,
            color: dark,
            backgroundColor: white,
            padding: 10,
            paddingHorizontal: 20,
            flexDirection: 'row',
            alignItems: 'center',
            borderRadius: 50,
            marginBottom: 10,
          }}>
          Programming
        </Text>
        <Text
          style={{
            fontSize: 12,
            color: dark,
            backgroundColor: white,
            padding: 10,
            paddingHorizontal: 20,
            flexDirection: 'row',
            alignItems: 'center',
            borderRadius: 50,
            marginBottom: 10,
          }}>
          Programming
        </Text>
        <Text
          style={{
            fontSize: 12,
            color: dark,
            backgroundColor: white,
            padding: 10,
            paddingHorizontal: 20,
            flexDirection: 'row',
            alignItems: 'center',
            borderRadius: 50,
            marginBottom: 10,
          }}>
          Graphic Designing
        </Text>
        <Text
          style={{
            fontSize: 12,
            color: dark,
            backgroundColor: white,
            padding: 10,
            paddingHorizontal: 20,
            borderRadius: 50,
            marginBottom: 10,
          }}>
          Graphic Designing
        </Text>
      </View>
    </View>
  );
}
