// import component
import React, {useState} from 'react';
import Svg, {Circle, Path} from 'react-native-svg';
import {Shadow} from 'react-native-shadow-2';
import {windowHeight, windowWidth} from '../constants/constants';
import {
  Image,
  Modal,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {dark, green200, lightGreen, white} from '../constants/colors';

function OptionsItem({name, id, image, code, onPress}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        paddingVertical: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
      }}>
      <Image
        source={image}
        style={{
          width: 40,
          height: 20,
        }}
      />
      <Text
        style={{
          color: dark,
          fontWeight: 'normal',
          marginHorizontal: 30,
        }}>
        {code}
      </Text>
      <Text
        style={{
          color: dark,
          fontWeight: 'normal',
          flex: 1,
        }}>
        {name}
      </Text>
    </TouchableOpacity>
  );
}

const options = [
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Pakistan',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'India',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'United State',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
  {
    id: '92iijs7yta',
    image: require('../assets/flag.png'),
    code: '+92',
    name: 'Ondo',
  },
];

export default function CountrySelect({
  placeholder,
  height,
  error,
  errorMessage,
}) {
  const [openModal, setOpenModal] = useState(false);
  const [itemsSelected, setItemsSelected] = useState(false);
  return (
    <>
      <View
        style={{
          marginVertical: 6,
          position: 'relative',
          backgroundColor: lightGreen,
          paddingVertical: 5,
          paddingHorizontal: 10,
          borderRadius: 10,
          borderWidth: 1,
          borderColor: error ? 'red' : lightGreen,
        }}>
        {error ? (
          <Text
            style={{
              color: 'red',
              position: 'absolute',
              top: 5,
              right: 8,
              fontSize: 11,
            }}>
            {errorMessage}
          </Text>
        ) : null}
        <Text
          style={{
            color: error ? 'red' : dark,
            fontSize: 12,
          }}>
          {placeholder}
        </Text>
        <View
          onPress={() => {
            setOpenModal(true);
          }}
          style={{
            backgroundColor: lightGreen,
            width: '100%',
            height: 30,
            borderRadius: 10,
            flexDirection: 'row',
          }}>
          {itemsSelected ? (
            <TouchableOpacity
              onPress={() => {
                setOpenModal(true);
              }}
              style={{flex: 1, justifyContent: 'center'}}>
              <Text
                style={{
                  color: 'rgba(0,0,0,.5)',
                  fontWeight: 'bold',
                }}>
                Pakistan
              </Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => {
                setOpenModal(true);
              }}
              style={{flex: 1, justifyContent: 'center'}}>
              <Text
                style={{
                  color: 'rgba(0,0,0,.5)',
                  fontWeight: 'bold',
                }}>
                {placeholder}
              </Text>
            </TouchableOpacity>
          )}

          <TouchableOpacity
            onPress={() => {
              setOpenModal(true);
            }}
            style={{
              alignItems: 'center',
              marginLeft: 5,
              marginTop: -3,
            }}>
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={20}
              height={20}
              viewBox="0 0 24 24"
              fill="none"
              stroke={dark}
              strokeWidth={2}
              strokeLinecap="round"
              strokeLinejoin="round"
              className="feather feather-chevron-down">
              <Path d="M6 9L12 15 18 9" />
            </Svg>
          </TouchableOpacity>
        </View>
      </View>
      <Modal visible={openModal} transparent={true}>
        <View
          style={{
            backgroundColor: 'rgba(225,225,225,.9)',
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Shadow
            distance={40}
            startColor={'rgba(0,0,0,.05)'}
            finalColor={'rgba(0,0,0,.01)'}
            offset={[2, 3]}>
            <View
              style={{
                backgroundColor: white,
                width: windowWidth - 40,
                height: height ? height : windowHeight - 40,
                borderRadius: 20,
                borderWidth: 2,
                borderColor: green200,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  marginTop: 15,
                  marginHorizontal: 20,
                }}>
                <Text style={{fontSize: 20, color: dark, fontWeight: 'bold'}}>
                  {placeholder}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    setOpenModal(false);
                  }}>
                  <Svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={20}
                    height={20}
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke={dark}
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-x">
                    <Path d="M18 6L6 18" />
                    <Path d="M6 6L18 18" />
                  </Svg>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginHorizontal: 20,
                  paddingHorizontal: 10,
                  borderRadius: 10,
                  height: 40,
                  marginVertical: 5,
                  backgroundColor: lightGreen,
                }}>
                <TextInput
                  placeholder="Search"
                  placeholderTextColor={dark}
                  style={{
                    flex: 1,
                    height: '100%',
                    padding: 0,
                    color: dark,
                  }}
                />
                <TouchableOpacity>
                  <Svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={20}
                    height={20}
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke={dark}
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-search">
                    <Circle cx={11} cy={11} r={8} />
                    <Path d="M21 21L16.65 16.65" />
                  </Svg>
                </TouchableOpacity>
              </View>
              <ScrollView
                style={{flex: 1, paddingHorizontal: 20, marginBottom: 20}}>
                {options.map((option, i) => (
                  <OptionsItem
                    key={i}
                    name={option.name}
                    id={option.id}
                    image={option.image}
                    code={option.code}
                    onPress={() => {
                      setOpenModal(false);
                      setItemsSelected(true);
                    }}
                  />
                ))}
              </ScrollView>
            </View>
          </Shadow>
        </View>
      </Modal>
    </>
  );
}
