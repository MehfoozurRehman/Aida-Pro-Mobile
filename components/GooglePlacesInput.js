import React, {useState} from 'react';
import {Text, View} from 'react-native';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {dark, lightGreen} from '../constants/colors';

export default function GooglePlacesInput({
  label,
  placeholder,
  error,
  errorMessage,
}) {
  const [placeholderShow, setPlaceholderShow] = useState(true);
  const [focus, setFocus] = useState(false);
  return (
    <>
      <View
        style={{
          borderWidth: 1,
          borderColor: error ? 'red' : focus ? '#0DCAA0' : lightGreen,
          backgroundColor: lightGreen,
          paddingHorizontal: 10,
          position: 'relative',
          paddingTop: 5,
          paddingBottom: 0,
          borderRadius: 10,
          marginVertical: 6,
        }}>
        {error ? (
          <Text
            style={{
              color: 'red',
              position: 'absolute',
              top: 5,
              right: 8,
              fontSize: 11,
            }}>
            {errorMessage}
          </Text>
        ) : null}
        <Text style={{color: focus ? '#0DCAA0' : dark, fontSize: 12}}>
          {label ? label : placeholder}
        </Text>
        {placeholderShow ? (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              height: 30,
              top: 22,
              left: 10,
              position: 'absolute',
              zIndex: -1000,
            }}>
            <Text style={{color: 'rgba(0,0,0,.5)', fontWeight: 'bold'}}>
              {placeholder}
            </Text>
          </View>
        ) : null}
        <GooglePlacesAutocomplete
          styles={{
            container: {},
            predefinedPlacesDescription: {
              color: dark,
            },
            textInputContainer: {
              flexDirection: 'row',
            },
            textInput: {
              color: dark,
              fontSize: 13,
              height: 30,
              fontWeight: 'bold',
              flex: 1,
              paddingHorizontal: 0,
              textDecorationLine: 'none',
              backgroundColor: 'rgba(0,0,0,0)',
              paddingBottom: 0,
            },
            poweredContainer: {
              display: 'none',
            },
            listView: {
              borderWidth: 1,
              borderColor: lightGreen,
              elevation: 5,
              borderRadius: 10,
              backgroundColor: lightGreen,
              marginBottom: 10,
            },
            row: {
              backgroundColor: lightGreen,
              padding: 10,
              height: 35,
              flexDirection: 'row',
              borderRadius: 10,
            },
            separator: {
              height: 0.5,
              backgroundColor: '#c8c7cc',
            },
            description: {
              color: '#5d5d5d',
              fontSize: 12,
            },
          }}
          query={{
            key: 'AIzaSyCklyIrqTy4HYKDHl92hlk1oBICM5o7tPo',
            language: 'en',
          }}
          returnKeyType="next"
          textInputProps={{
            onFocus: () => {
              setFocus(true);
            },
            onBlur: () => {
              setFocus(false);
            },
            onChangeText: e => {
              if (e !== '') {
                setPlaceholderShow(false);
              } else {
                setPlaceholderShow(true);
              }
            },
          }}
        />
      </View>
    </>
  );
}
