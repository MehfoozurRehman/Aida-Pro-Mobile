import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {dark, lightGreen, white} from '../constants/colors';

export default function ChatUser({
  image,
  name,
  message,
  time,
  badgeValue,
  navigation,
}) {
  return (
    <TouchableOpacity
      style={{
        flexDirection: 'row',
        width: '100%',
        backgroundColor: lightGreen,
        borderRadius: 10,
        padding: 12,
        marginBottom: 10,
      }}
      onPress={() => {
        navigation.navigate('Chat');
      }}>
      <Image
        source={image}
        style={{width: 45, height: 45, borderRadius: 45, marginRight: 15}}
      />
      <View style={{justifyContent: 'center', flex: 1}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 3,
          }}>
          <Text style={{fontSize: 15, fontWeight: '500', color: dark}}>
            {name}
          </Text>
          {time ? (
            <Text style={{color: dark, fontSize: 12}}>{time} ago</Text>
          ) : null}
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={{color: dark, fontSize: 12}}>{message}</Text>
          {badgeValue && badgeValue !== 0 ? (
            <View
              style={{
                backgroundColor: dark,
                width: 20,
                height: 20,
                borderRadius: 20,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  color: white,
                  fontSize: 8,
                }}>
                {badgeValue}
              </Text>
            </View>
          ) : null}
        </View>
      </View>
    </TouchableOpacity>
  );
}
