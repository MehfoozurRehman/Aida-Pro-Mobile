import React from 'react';
import {View, Text} from 'react-native';
import {dark, lightGreen, yellow} from '../constants/colors';

export default function PlanDetailsCard() {
  return (
    <View
      style={{
        marginRight: 20,
        backgroundColor: lightGreen,
        padding: 20,
        borderRadius: 10,
      }}>
      <View style={{flex: 1, marginRight: 20}}>
        <Text style={{fontSize: 12, color: '#0DC5A1', marginBottom: 5}}>
          Plan 1
        </Text>
        <Text style={{fontSize: 12, color: dark, marginBottom: 5}}>
          Credit Contacts
        </Text>
        <Text style={{fontSize: 12, color: '#adadc5', marginBottom: 5}}>
          Sept 19 - Oct 19
        </Text>
      </View>
      <View style={{flex: 1, marginRight: 20}}>
        <Text style={{fontSize: 12, color: yellow, marginBottom: 5}}>
          10 Credits
        </Text>
        <Text style={{fontSize: 12, color: dark, marginBottom: 5}}>
          EUR 39/Credit
        </Text>
        <Text style={{fontSize: 12, fontWeight: 'bold', color: dark}}>
          EUR 40
        </Text>
      </View>
    </View>
  );
}
