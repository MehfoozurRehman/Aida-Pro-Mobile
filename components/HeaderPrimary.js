import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Svg, {G, Path} from 'react-native-svg';
import {green200, white} from '../constants/colors';

export default function HeaderPrimary({
  title,
  icon,
  backPath,
  noBackButton,
  imgStyle,
}) {
  const navigation = useNavigation();
  return (
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      colors={['#0DC5A1', green200]}
      style={{
        width: '100%',
        borderBottomLeftRadius: 26,
        borderBottomRightRadius: 26,
        paddingBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View
        style={{
          width: '100%',
          borderBottomLeftRadius: 26,
          borderBottomRightRadius: 26,
          overflow: 'hidden',
          justifyContent: 'center',
          alignItems: 'center',
          paddingVertical: 20,
          paddingHorizontal: 15,
        }}>
        {noBackButton ? (
          <View style={{height: 15}} />
        ) : (
          <View style={{width: '100%'}}>
            <TouchableOpacity
              style={{padding: 5}}
              onPress={() => {
                navigation.navigate(backPath ? backPath : 'Home');
              }}>
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={20.012}
                height={17.163}>
                <G data-name="Group 505">
                  <Path
                    data-name="Path 730"
                    d="M18.809 7.176a1.43 1.43 0 00-.247-.018H4.443l.308-.143a2.864 2.864 0 00.809-.573l3.959-3.959a1.482 1.482 0 00.208-1.9A1.432 1.432 0 007.579.397L.42 7.558a1.432 1.432 0 000 2.025l7.159 7.159a1.432 1.432 0 002.148-.143 1.482 1.482 0 00-.208-1.9l-3.952-3.961a2.864 2.864 0 00-.716-.523l-.43-.193h14.062a1.482 1.482 0 001.511-1.2 1.432 1.432 0 00-1.185-1.646z"
                    fill={white}
                  />
                </G>
              </Svg>
            </TouchableOpacity>
          </View>
        )}
        <Image
          source={icon}
          style={{
            width: 40,
            height: 40,
            marginBottom: 8,
            ...imgStyle,
          }}
        />
        <Text style={{fontSize: 18, color: white, fontWeight: 'bold'}}>
          {title}
        </Text>
      </View>
    </LinearGradient>
  );
}
