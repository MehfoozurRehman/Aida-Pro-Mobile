import React, {useState} from 'react';
import Slider from '@react-native-community/slider';
import {Text, View} from 'react-native';
import {dark, lightGreen} from '../constants/colors';

export default function Range() {
  const [value, setValue] = useState(5);
  return (
    <View
      style={{
        backgroundColor: lightGreen,
        paddingVertical: 10,
        marginVertical: 6,
        borderRadius: 10,
        paddingHorizontal: 5,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingHorizontal: 8,
        }}>
        <Text style={{color: dark, fontSize: 12}}>Range</Text>
        <Text style={{fontSize: 12, color: dark}}>{value}km</Text>
      </View>
      <Slider
        style={{width: '100%', height: 30, padding: 0}}
        minimumValue={5}
        focusable={true}
        maximumValue={500}
        tapToSeek={true}
        minimumTrackTintColor="#0DCBA0"
        maximumTrackTintColor={dark}
        thumbTintColor="#0DCBA0"
        step={6}
        onValueChange={e => {
          setValue(e.toString());
        }}
      />
    </View>
  );
}
