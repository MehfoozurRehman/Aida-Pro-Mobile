import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {dark, lightGreen} from '../constants/colors';

export default function Contact({source, heading, subHeading, isSocial}) {
  return (
    <View
      style={{
        backgroundColor: lightGreen,
        padding: 20,
        borderRadius: 10,
        marginBottom: 16,
      }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: 10,
        }}>
        <Image source={source} style={{height: 60, marginRight: 16}} />
        <View style={{display: 'flex', flexDirection: 'column', width: '80%'}}>
          <Text style={{fontSize: 14, color: dark, fontWeight: 'bold'}}>
            {heading}
          </Text>
          {isSocial ? (
            <View
              style={{
                width: '60%',
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 6,
              }}>
              <TouchableOpacity>
                <Image
                  source={require('../assets/facebook.png')}
                  style={{width: 35, height: 35}}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image
                  source={require('../assets/google.png')}
                  style={{width: 35, height: 35}}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image
                  source={require('../assets/linkedin.png')}
                  style={{width: 35, height: 35}}
                />
              </TouchableOpacity>
            </View>
          ) : (
            <Text style={{fontSize: 12, color: dark}}>{subHeading}</Text>
          )}
        </View>
      </View>
    </View>
  );
}
