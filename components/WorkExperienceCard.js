import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import Svg, {G, Path} from 'react-native-svg';
import {dark, white} from '../constants/colors';

export default function WorkExperienceCard({onPress}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        padding: 16,
        borderRadius: 10,
        backgroundColor: white,
        position: 'relative',
        marginVertical: 10,
      }}>
      <Text style={{fontSize: 12, color: dark, fontWeight: 'bold'}}>
        Full Stack Developer Node Js
      </Text>
      <Text
        style={{
          fontSize: 12,
          color: dark,
          marginVertical: 2,
        }}>
        Researcher
      </Text>
      <Text
        style={{
          fontSize: 13,
          color: '#0DC5A1',
          textDecorationLine: 'underline',
          textDecorationColor: '#0DC5A1',
        }}>
        Dec 12,2012 - Jan 12,2021
      </Text>
      <TouchableOpacity
        style={{
          position: 'absolute',
          top: '-10%',
          left: '106%',
          width: 20,
          height: 20,
          borderRadius: 20,
          backgroundColor: dark,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Svg xmlns="http://www.w3.org/2000/svg" width={9.458} height={9.458}>
          <G
            fill="none"
            stroke={white}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={1.5}>
            <Path data-name="Line 22" d="M8.397 1.061 1.061 8.397" />
            <Path data-name="Line 23" d="m1.061 1.061 7.336 7.336" />
          </G>
        </Svg>
      </TouchableOpacity>
    </TouchableOpacity>
  );
}
