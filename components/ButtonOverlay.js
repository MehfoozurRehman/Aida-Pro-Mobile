import React from 'react';
import {TouchableOpacity, Text, Image} from 'react-native';
import {dark} from '../constants/colors';

export default function ButtonOverlay({label, style, icon, onPress}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        height: 45,
        marginVertical: 10,
        width: '100%',
        borderRadius: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,.09)',
        ...style,
      }}>
      {icon ? (
        <Image source={icon} style={{width: 18, height: 18, marginRight: 10}} />
      ) : null}
      <Text style={{fontSize: 14, fontWeight: '600', color: dark}}>
        {label}
      </Text>
    </TouchableOpacity>
  );
}
