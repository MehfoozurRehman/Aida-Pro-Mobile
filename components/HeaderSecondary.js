import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {View, TouchableOpacity} from 'react-native';
import Svg, {G, Text as Txt, TSpan, Path} from 'react-native-svg';

export default function HeaderSecondary({
  title,
  icon,
  backPath,
  variant,
  noBackButton,
}) {
  const navigation = useNavigation();
  return (
    <View
      style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        paddingVertical: 20,
        paddingHorizontal: 15,
      }}>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate(backPath ? backPath : 'Home');
        }}>
        <Svg xmlns="http://www.w3.org/2000/svg" width={25.425} height={16.344}>
          <G data-name="Group 496" fill="#0dc5a0">
            <Path
              data-name="Path 708"
              d="M24.506 0H.919C.411 0 0 .185 0 .413v2.2c0 .228.412.413.919.413h23.587c.508 0 .919-.185.919-.413V.409c0-.224-.412-.409-.919-.409z"
            />
            <Path
              data-name="Path 709"
              d="M16.942 6.66H.635c-.351 0-.635.185-.635.413v2.2c0 .228.285.413.635.413h16.307c.351 0 .635-.185.635-.413v-2.2c.001-.228-.284-.413-.635-.413z"
            />
            <Path
              data-name="Path 710"
              d="M12.291 13.318H.461a.439.439 0 00-.461.413v2.2a.439.439 0 00.461.413h11.83a.439.439 0 00.461-.413v-2.2a.439.439 0 00-.461-.413z"
            />
          </G>
        </Svg>
      </TouchableOpacity>
      <Svg
        data-name="Group 1092"
        xmlns="http://www.w3.org/2000/svg"
        width={91.011}
        height={28.898}>
        <Txt
          transform="translate(80.011 5)"
          fill="#0dc5a1"
          fontSize={5}
          fontFamily="SegoeUI-Bold, Segoe UI"
          fontWeight={700}>
          <TSpan x={0} y={0}>
            {'PRO'}
          </TSpan>
        </Txt>
        <G data-name="Group 141" fill="#0dc5a1">
          <Path
            data-name="Path 243"
            d="M.049 28.579a2.3 2.3 0 01.409-1.709c2.575-4.784 5.193-9.546 7.746-14.342.59-1.109 1.095-1.308 2-.334 1.108 1.187 2.293 2.3 3.47 3.425.6.568.7 1.079.021 1.643Q6.963 22.854.232 28.449c-.026.024-.037.028-.183.13z"
          />
          <Path
            data-name="Path 244"
            d="M27.204 26.624a1.879 1.879 0 01-1.407-.64q-7.034-6.826-14.049-13.67c-.309-.3-.884-.573-.705-1.064.19-.52.8-.286 1.222-.292 1.773-.024 3.547.007 5.319-.015a1.725 1.725 0 011.752.929q3.7 6.98 7.428 13.949c.11.203.226.454.44.803z"
          />
          <Path
            data-name="Path 245"
            d="M27.116 28.8c-.617.279-1.386-.109-1.9-.291q-6.985-2.467-13.949-4.99c-.574-.208-1.425-.237-1.575-.911-.079-.642.673-.982 1.127-1.391 1.221-1.1 2.46-2.186 3.68-3.289.437-.395.845-.638 1.371-.133 3.675 3.53 7.443 6.963 10.933 10.682a2.04 2.04 0 01.313.323z"
          />
          <Path
            data-name="Path 246"
            d="M13.921 10.074c-1.01 0-2.021.023-3.03-.007-.856-.025-1.048-.438-.633-1.19 1.047-1.9 2.09-3.807 3.1-5.727.471-.892.8-.688 1.2.049 1 1.868 2.031 3.724 3.059 5.579.524.946.136 1.3-.817 1.306s-1.919 0-2.879 0z"
          />
        </G>
        <G data-name="Group 142" fill="#0dc5a1">
          <Path data-name="Path 247" d="M30.876 28.898V2.561h4.155v26.337z" />
          <Path
            data-name="Path 248"
            d="M39.4 28.898V2.561h9.348A13.933 13.933 0 0154.331 3.6a11.5 11.5 0 014.006 2.819 11.814 11.814 0 012.43 4.173 15.7 15.7 0 01.816 5.1 15.416 15.416 0 01-.909 5.434 11.872 11.872 0 01-2.578 4.155 11.431 11.431 0 01-4.043 2.671 14.22 14.22 0 01-5.3.946zm17.954-13.206a12.145 12.145 0 00-.575-3.821 8.324 8.324 0 00-1.688-2.986 7.838 7.838 0 00-2.708-1.948 8.911 8.911 0 00-3.635-.705h-5.193v18.994h5.193a8.944 8.944 0 003.691-.723 7.6 7.6 0 002.708-2 8.635 8.635 0 001.651-3.023 12.4 12.4 0 00.556-3.788z"
          />
          <Path
            data-name="Path 249"
            d="M71.983 2.561L73.6 6.893l8.533 22.006h-3.685l-2.342-6.027h-8.557l-2.311 6.027h-3.716zm3.436 17.718l-3.56-9.525-3.685 9.525z"
          />
        </G>
      </Svg>
    </View>
  );
}
