import React from 'react';
import {Image, Text, View} from 'react-native';
import {degrees} from '../assets/icons';
import EducationDetailsCard from '../components/EducationDetailsCard';
import {dark, lightGreen} from '../constants/colors';

export default function DegreeCard() {
  return (
    <View
      style={{
        backgroundColor: lightGreen,
        padding: 20,
        borderRadius: 10,
        marginBottom: 20,
      }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: 10,
        }}>
        <Image
          source={degrees}
          style={{width: 55, height: 40, marginRight: 10}}
        />
        <Text style={{fontSize: 12, color: dark}}>Degrees</Text>
      </View>
      <EducationDetailsCard isNotEditAble={true} />
      <EducationDetailsCard isNotEditAble={true} />
    </View>
  );
}
