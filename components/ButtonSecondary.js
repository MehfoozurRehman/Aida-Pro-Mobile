import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import {dark, white} from '../constants/colors';

export default function ButtonSecondary({label, style, onPress}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        height: 45,
        marginVertical: 10,
        width: '100%',
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        backgroundColor: dark,
        ...style,
      }}>
      <Text style={{fontSize: 14, fontWeight: '600', color: white}}>
        {label}
      </Text>
    </TouchableOpacity>
  );
}
