import React from 'react';
import {Text, View} from 'react-native';
import {dark} from '../constants/colors';

export default function ProfilePreviewRow({
  svgLeft,
  titleLeft,
  headingLeft,
  titleRight,
  headingRight,
}) {
  return (
    <View
      style={{
        width: '100%',
        borderRadius: 10,
        display: 'flex',
        flexDirection: 'row',
        marginVertical: 20,
      }}>
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          width: '60%',
        }}>
        <View style={{marginRight: 10}}>{svgLeft}</View>
        <View>
          <Text style={{fontSize: 13, color: dark}}>{titleLeft}</Text>
          <Text style={{fontSize: 12, color: dark, fontWeight: 'bold'}}>
            {headingLeft}
          </Text>
        </View>
      </View>
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          width: '40%',
        }}>
        <View style={{marginRight: 10}}>{svgLeft}</View>
        <View>
          <Text style={{fontSize: 13, color: dark}}>{titleRight}</Text>
          <Text style={{fontSize: 12, color: dark, fontWeight: 'bold'}}>
            {headingRight}
          </Text>
        </View>
      </View>
    </View>
  );
}
