import React from 'react';
import {TouchableOpacity, Text, Image} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {white} from '../constants/colors';

export default function ButtonPrimary({label, style, icon, onPress}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{width: '100%', height: 45, marginVertical: 10, ...style}}>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={['#F6B038', '#F5833C']}
        style={{
          width: '100%',
          height: '100%',
          borderRadius: 50,
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: icon ? 'row' : 'column',
        }}>
        {icon ? (
          <Image
            source={icon}
            style={{width: 18, height: 18, marginRight: 10}}
          />
        ) : null}
        <Text style={{fontSize: 12, fontWeight: '600', color: white}}>
          {label}
        </Text>
      </LinearGradient>
    </TouchableOpacity>
  );
}
