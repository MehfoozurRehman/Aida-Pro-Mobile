import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Svg, {G, Path} from 'react-native-svg';
import {dark, lightGreen, white} from '../constants/colors';

export default function SkillsTag({text, onPress}) {
  return (
    <View
      style={{
        padding: 10,
        backgroundColor: white,
        alignSelf: 'flex-start',
        borderRadius: 40,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 4,
        marginRight: 10,
      }}>
      <Text style={{fontSize: 12, color: dark, marginRight: 6}}>{text}</Text>
      <TouchableOpacity
        onPress={onPress}
        style={{
          width: 20,
          height: 20,
          borderRadius: 20,
          backgroundColor: lightGreen,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Svg xmlns="http://www.w3.org/2000/svg" width={9.458} height={9.458}>
          <G
            fill="none"
            stroke={dark}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={1.5}>
            <Path data-name="Line 22" d="M8.397 1.061 1.061 8.397" />
            <Path data-name="Line 23" d="m1.061 1.061 7.336 7.336" />
          </G>
        </Svg>
      </TouchableOpacity>
    </View>
  );
}
