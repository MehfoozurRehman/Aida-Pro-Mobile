import React from 'react';
import {Image, Text, View} from 'react-native';
import {summary} from '../assets/icons';
import {dark, lightGreen, white} from '../constants/colors';

export default function SummaryCard() {
  return (
    <View
      style={{
        backgroundColor: lightGreen,
        padding: 20,
        borderRadius: 10,
        marginBottom: 20,
      }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: 10,
        }}>
        <Image
          source={summary}
          style={{width: 55, height: 60, marginRight: 10}}
        />
        <Text>Summary</Text>
      </View>
      <View
        style={{
          backgroundColor: white,
          padding: 10,
          paddingHorizontal: 20,
          flexDirection: 'row',
          alignItems: 'center',
          borderRadius: 50,
          marginBottom: 10,
        }}>
        <Text style={{fontSize: 12, color: dark, width: 80}}>Age</Text>
        <Text style={{fontSize: 12, color: dark}}>27 Years Old</Text>
      </View>
      <View
        style={{
          backgroundColor: white,
          padding: 10,
          paddingHorizontal: 20,
          flexDirection: 'row',
          alignItems: 'center',
          borderRadius: 50,
          marginBottom: 10,
        }}>
        <Text style={{fontSize: 12, color: dark, width: 80}}>Gender</Text>
        <Text style={{fontSize: 12, color: dark}}>Male</Text>
      </View>
      <View
        style={{
          backgroundColor: white,
          padding: 10,
          paddingHorizontal: 20,
          flexDirection: 'row',
          alignItems: 'center',
          borderRadius: 50,
          marginBottom: 10,
        }}>
        <Text style={{fontSize: 12, color: dark, width: 80}}>Education</Text>
        <Text style={{fontSize: 12, color: dark}}>Bachelor's in CS</Text>
      </View>
      <View
        style={{
          backgroundColor: white,
          padding: 10,
          paddingHorizontal: 20,
          flexDirection: 'row',
          alignItems: 'center',
          borderRadius: 50,
          marginBottom: 10,
          width: '100%',
        }}>
        <Text style={{fontSize: 12, color: dark, width: 80}}>Address</Text>
        <Text style={{fontSize: 12, color: dark, flex: 1}}>
          St. 44, House 545, Jaliem Road, Tokyo
        </Text>
      </View>
    </View>
  );
}
