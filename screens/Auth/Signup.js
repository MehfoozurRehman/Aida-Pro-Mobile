import React from 'react';
import {TouchableOpacity, Image, Text, View, ScrollView} from 'react-native';
import {Input, Checkbox, Select} from '../../components';
import {role, windowHeight} from '../../constants/constants';
import Svg, {G, Path} from 'react-native-svg';
import GooglePlacesInput from '../../components/GooglePlacesInput';
import ButtonPrimary from '../../components/ButtonPrimary';
import {dark, white} from '../../constants/colors';

export default function Signup({navigation}) {
  return (
    <View
      style={{
        paddingHorizontal: 20,
        flex: 1,
        height: windowHeight,
        backgroundColor: white,
        justifyContent: 'space-between',
      }}>
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 10,
          marginTop: 20,
        }}>
        <TouchableOpacity
          style={{padding: 5}}
          onPress={() => {
            navigation.push('Welcome');
          }}>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={20.012}
            height={17.163}>
            <G data-name="Group 505">
              <Path
                data-name="Path 730"
                d="M18.809 7.176a1.43 1.43 0 00-.247-.018H4.443l.308-.143a2.864 2.864 0 00.809-.573l3.959-3.959a1.482 1.482 0 00.208-1.9A1.432 1.432 0 007.579.397L.42 7.558a1.432 1.432 0 000 2.025l7.159 7.159a1.432 1.432 0 002.148-.143 1.482 1.482 0 00-.208-1.9l-3.952-3.961a2.864 2.864 0 00-.716-.523l-.43-.193h14.062a1.482 1.482 0 001.511-1.2 1.432 1.432 0 00-1.185-1.646z"
                fill={dark}
              />
            </G>
          </Svg>
        </TouchableOpacity>
        <Image
          source={require('../../assets/logo.png')}
          style={{width: 102, height: 32}}
        />
      </View>
      <Text
        style={{
          fontSize: 25,
          fontWeight: '600',
          color: dark,
          textTransform: 'capitalize',
          width: 235,
          marginBottom: 20,
          borderBottomColor: '#0DC5A1',
          borderBottomWidth: 2,
          paddingBottom: 2,
        }}>
        Let's get you started
      </Text>
      <ScrollView>
        {role === 'Freelancer' ? (
          <>
            <Input
              placeholder="First name"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
            <Input
              placeholder="Last name"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
            <GooglePlacesInput
              placeholder="Location"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
            <Input
              placeholder="Email"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
            <Input
              placeholder="Phone"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
            <Input
              placeholder="Password"
              secureTextEntry={true}
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
            <Input
              placeholder="Confirm Password"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
          </>
        ) : role === 'Professional' ? (
          <>
            <Input
              placeholder="First name"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
            <Input
              placeholder="Last name"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
            <GooglePlacesInput
              placeholder="Location"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
            <Input
              placeholder="Email"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
            <Input
              placeholder="Phone"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
            <Input
              placeholder="Password"
              secureTextEntry={true}
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
            <Input
              placeholder="Confirm Password"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
          </>
        ) : (
          <>
            <Input
              placeholder="Company name"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
            <Input
              placeholder="Contact person"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
            <GooglePlacesInput
              placeholder="location"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
            <Select
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
              label="Industry"
              placeholder="Industry"
            />
            <Input
              placeholder="Email"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
            <Input
              placeholder="Password"
              secureTextEntry={true}
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
            <Input
              placeholder="Confirm Password"
              secureTextEntry={true}
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={18.219}
                  height={12.246}>
                  <G data-name="Group 202">
                    <Path
                      data-name="Path 292"
                      d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                      fill={dark}
                      stroke={dark}
                      strokeWidth={0.3}
                    />
                  </G>
                </Svg>
              }
            />
          </>
        )}

        <Checkbox
          variant={true}
          label={
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  flexWrap: 'wrap',
                }}>
                <Text style={{color: dark}}>I agree to AIDApro’s</Text>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate('TermsConditions');
                  }}>
                  <Text style={{color: '#0DC5A1'}}>Terms and Conditions</Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  flexWrap: 'wrap',
                }}>
                <Text style={{color: dark}}> & </Text>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate('PrivacyPolicy');
                  }}>
                  <Text style={{color: '#0DC5A1'}}>Privacy policy</Text>
                </TouchableOpacity>
              </View>
            </View>
          }
        />
        <ButtonPrimary
          label="Sign Up"
          style={{marginVertical: 20}}
          onPress={() => {
            navigation.push('CustomDrawer');
          }}
        />
        <View
          style={{
            width: '100%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontSize: 14,
              color: dark,
              textTransform: 'capitalize',
            }}>
            Or Sign Up with
          </Text>
          <View
            style={{
              width: '80%',
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginVertical: 30,
            }}>
            <TouchableOpacity>
              <Image
                source={require('../../assets/facebook.png')}
                style={{width: 35, height: 35}}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                source={require('../../assets/google.png')}
                style={{width: 35, height: 35}}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                source={require('../../assets/linkedin.png')}
                style={{width: 35, height: 35}}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 20,
            }}>
            <Text
              style={{
                fontSize: 14,
                color: dark,
                marginRight: 4,
              }}>
              Don't have a account?
            </Text>
            <TouchableOpacity
              onPress={() => {
                navigation.push('Login');
              }}>
              <Text
                style={{
                  fontSize: 14,
                  color: dark,
                  textTransform: 'capitalize',
                  textDecorationLine: 'underline',
                }}>
                Login
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}
