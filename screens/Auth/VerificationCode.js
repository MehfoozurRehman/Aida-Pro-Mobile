import React from 'react';
import {TouchableOpacity, Text, View, Image} from 'react-native';
import {windowHeight} from '../../constants/constants';
import Svg, {G, Path} from 'react-native-svg';
import VerificationInputField from '../../components/VerificationInputField';
import ButtonPrimary from '../../components/ButtonPrimary';
import {dark, green200} from '../../constants/colors';

export default function ForgotPassword({navigation}) {
  return (
    <View
      style={{
        padding: 20,
        paddingVertical: 30,
        flex: 1,
        height: windowHeight,
        backgroundColor: '#F6F8FB',
        justifyContent: 'space-between',
      }}>
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 10,
        }}>
        <TouchableOpacity
          style={{padding: 5}}
          onPress={() => {
            navigation.push('ForgotPassword');
          }}>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={20.012}
            height={17.163}>
            <G data-name="Group 505">
              <Path
                data-name="Path 730"
                d="M18.809 7.176a1.43 1.43 0 00-.247-.018H4.443l.308-.143a2.864 2.864 0 00.809-.573l3.959-3.959a1.482 1.482 0 00.208-1.9A1.432 1.432 0 007.579.397L.42 7.558a1.432 1.432 0 000 2.025l7.159 7.159a1.432 1.432 0 002.148-.143 1.482 1.482 0 00-.208-1.9l-3.952-3.961a2.864 2.864 0 00-.716-.523l-.43-.193h14.062a1.482 1.482 0 001.511-1.2 1.432 1.432 0 00-1.185-1.646z"
                fill={dark}
              />
            </G>
          </Svg>
        </TouchableOpacity>
        <Image
          source={require('../../assets/logo.png')}
          style={{width: 102, height: 32}}
        />
      </View>
      <View>
        <Text
          style={{
            fontSize: 22,
            fontWeight: '600',
            color: dark,
            textTransform: 'capitalize',
            width: '100%',
            marginBottom: 10,
          }}>
          Authentication Code
        </Text>
        <Text
          style={{
            fontSize: 12,
            color: dark,
            textTransform: 'capitalize',
            width: '100%',
          }}>
          Write the authentication code you received on
        </Text>
        <TouchableOpacity>
          <Text style={{color: green200, marginBottom: 10}}>
            free@gmail.com
          </Text>
        </TouchableOpacity>
        <VerificationInputField />
        <ButtonPrimary
          label="Continue"
          style={{marginVertical: 20}}
          onPress={() => {
            navigation.push('Login');
          }}
        />
      </View>
      <View />
    </View>
  );
}
