import React from 'react';
import Svg, {G, Path} from 'react-native-svg';
import {windowHeight} from '../../constants/constants';
import BouncyCheckbox from 'react-native-bouncy-checkbox';
import ButtonPrimary from '../../components/ButtonPrimary';
import InputTransparent from '../../components/InputTransparent';
import {
  TouchableOpacity,
  Image,
  ImageBackground,
  Text,
  View,
  ScrollView,
} from 'react-native';
import {white} from '../../constants/colors';

export default function Login({navigation}) {
  return (
    <ScrollView style={{flex: 1}}>
      <ImageBackground
        source={require('../../assets/loginScreenBg.png')}
        style={{
          flex: 1,
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingVertical: 20,
          paddingHorizontal: 15,
          height: windowHeight,
        }}>
        <View style={{width: '100%'}}>
          <TouchableOpacity
            style={{padding: 5}}
            onPress={() => {
              navigation.push('Welcome');
            }}>
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={20.012}
              height={17.163}>
              <G data-name="Group 505">
                <Path
                  data-name="Path 730"
                  d="M18.809 7.176a1.43 1.43 0 00-.247-.018H4.443l.308-.143a2.864 2.864 0 00.809-.573l3.959-3.959a1.482 1.482 0 00.208-1.9A1.432 1.432 0 007.579.397L.42 7.558a1.432 1.432 0 000 2.025l7.159 7.159a1.432 1.432 0 002.148-.143 1.482 1.482 0 00-.208-1.9l-3.952-3.961a2.864 2.864 0 00-.716-.523l-.43-.193h14.062a1.482 1.482 0 001.511-1.2 1.432 1.432 0 00-1.185-1.646z"
                  fill="#333"
                />
              </G>
            </Svg>
          </TouchableOpacity>
        </View>
        <View style={{width: '100%', alignItems: 'center'}}>
          <Text
            style={{
              fontSize: 22,
              fontWeight: '600',
              color: white,
              textTransform: 'capitalize',
              width: '100%',
              marginBottom: 7,
            }}>
            Sign In
          </Text>
          <InputTransparent
            label="Email"
            placeholder="Enter Email"
            transparent={true}
            error={false}
            errorMessage="errorerror"
            svg={
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={18.219}
                height={12.246}>
                <G data-name="Group 202">
                  <Path
                    data-name="Path 292"
                    d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
                    fill="#fff"
                    stroke="#fff"
                    strokeWidth={0.3}
                  />
                </G>
              </Svg>
            }
          />
          <InputTransparent
            label="Password"
            placeholder="Enter Password"
            secureTextEntry={true}
            transparent={true}
            svg={
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={11.192}
                height={17.922}>
                <Path
                  data-name="Path 293"
                  d="M9.793 14.923H1.4a1.4 1.4 0 01-1.4-1.4V6.996a1.4 1.4 0 011.4-1.4h8.394a1.4 1.4 0 011.4 1.4v6.529a1.4 1.4 0 01-1.401 1.398zM1.4 6.529a.467.467 0 00-.466.466v6.529a.467.467 0 00.466.466h8.394a.467.467 0 00.466-.466V6.996a.467.467 0 00-.466-.466z"
                  fill="#fff"
                />
                <Path
                  data-name="Path 294"
                  d="M8.86 6.529a.466.466 0 01-.466-.466V3.731a2.8 2.8 0 10-5.6 0v2.331a.467.467 0 01-.933 0V3.731a3.731 3.731 0 117.461 0v2.331a.466.466 0 01-.462.467z"
                  fill="#fff"
                />
                <Path
                  data-name="Path 295"
                  d="M5.596 10.57A1.244 1.244 0 116.84 9.326a1.245 1.245 0 01-1.244 1.244zm0-1.554a.311.311 0 10.311.311.311.311 0 00-.311-.311z"
                  fill="#fff"
                />
                <Path
                  data-name="Path 296"
                  d="M5.596 12.436a.466.466 0 01-.466-.466v-1.71a.467.467 0 11.933 0v1.71a.466.466 0 01-.467.466z"
                  fill="#fff"
                />
              </Svg>
            }
          />
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginTop: 10,
              width: '100%',
            }}>
            <BouncyCheckbox
              size={24}
              fillColor="#0DC5A1"
              unfillColor={white}
              text="Remember me"
              iconStyle={{borderColor: white}}
              textStyle={{
                fontFamily: 'JosefinSans-Regular',
                textDecorationLine: 'none',
                fontSize: 14,
                color: white,
              }}
            />
            <TouchableOpacity
              onPress={() => {
                navigation.push('ForgotPassword');
              }}>
              <Text
                style={{
                  fontSize: 14,
                  color: white,
                  textTransform: 'capitalize',
                  textDecorationLine: 'underline',
                }}>
                Forgot password?
              </Text>
            </TouchableOpacity>
          </View>
          <ButtonPrimary
            label="Sign In"
            style={{marginVertical: 20}}
            onPress={() => {
              navigation.push('CustomDrawer');
            }}
          />
          <Text
            style={{
              fontSize: 14,
              color: white,
              textTransform: 'capitalize',
            }}>
            Or Sign In with
          </Text>
          <View
            style={{
              width: '80%',
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginVertical: 15,
            }}>
            <TouchableOpacity>
              <Image
                source={require('../../assets/facebook.png')}
                style={{width: 30, height: 30}}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                source={require('../../assets/google.png')}
                style={{width: 30, height: 30}}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{backgroundColor: white, borderRadius: 10}}>
              <Image
                source={require('../../assets/linkedin.png')}
                style={{width: 30, height: 30}}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: 14,
                color: white,
                marginRight: 4,
              }}>
              Create an account?
            </Text>
            <TouchableOpacity
              onPress={() => {
                navigation.push('Signup');
              }}>
              <Text
                style={{
                  fontSize: 14,
                  color: white,
                  textTransform: 'capitalize',
                  textDecorationLine: 'underline',
                }}>
                Signup
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    </ScrollView>
  );
}
