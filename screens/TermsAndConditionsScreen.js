import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {terms} from '../assets/icons';
import HeaderPrimary from '../components/HeaderPrimary';
import {dark} from '../constants/colors';

export default function TermsAndConditionsScreen({}) {
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        overflow: 'hidden',
      }}>
      <HeaderPrimary
        title="Terms & Conditions"
        backPath="Signup"
        icon={terms}
      />
      <ScrollView
        style={{
          flex: 1,
          width: '100%',
          paddingVertical: 20,
          paddingHorizontal: 15,
        }}>
        <View
          style={{
            display: 'flex',
            flexDirection: 'column',
            width: '100%',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontSize: 18,
              color: dark,
              fontWeight: 'bold',
              marginTop: 10,
            }}>
            Term Apllied:
          </Text>
          <Text style={{fontSize: 12, color: dark, marginVertical: 4}}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer.
          </Text>
          <Text
            style={{
              fontSize: 18,
              color: dark,
              fontWeight: 'bold',
              marginTop: 10,
            }}>
            Conditions Lorem Ipsum:
          </Text>
          <Text style={{fontSize: 12, color: dark, marginVertical: 4}}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum
          </Text>
          <Text
            style={{
              fontSize: 18,
              color: dark,
              fontWeight: 'bold',
              marginTop: 10,
            }}>
            Term Apllied:
          </Text>
          <Text style={{fontSize: 12, color: dark, marginVertical: 4}}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. Lorem Ipsum is simply
            dummy text of the printing and typesetting industry. Lorem Ipsum has
            been the industry's
          </Text>
        </View>
      </ScrollView>
    </Animated.View>
  );
}
