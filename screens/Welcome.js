import React from 'react';
import {windowWidth} from '../constants/constants';
import {
  TouchableOpacity,
  SafeAreaView,
  Image,
  StyleSheet,
  View,
  Text,
  ScrollView,
} from 'react-native';
import {dark} from '../constants/colors';

export default function Welcome({navigation}) {
  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView>
        <Image
          source={require('../assets/welcomeScreenPic.png')}
          style={{height: 120, width: '100%'}}
        />
        <Text
          style={{
            color: dark,
            textAlign: 'center',
            padding: 20,
            fontSize: 12,
          }}>
          Welcome to the AIDApro community where AI & Data professionals and
          companies meet. The place to find professionals, vacancies and
          projects.
        </Text>
        <View
          style={{
            flex: 1,

            paddingHorizontal: windowWidth < 350 ? 20 : 25,
          }}>
          <TouchableOpacity
            style={styles.welcomeBtn}
            onPress={() => {
              navigation.push('Login');
            }}>
            <Image
              source={require('../assets/companyBtnBg.png')}
              style={styles.welcomeBtnImage}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.welcomeBtn}
            onPress={() => {
              navigation.push('Login');
            }}>
            <Image
              source={require('../assets/professionalBtnBg.png')}
              style={styles.welcomeBtnImage}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.welcomeBtn}
            onPress={() => {
              navigation.push('Login');
            }}>
            <Image
              source={require('../assets/freelancerBtnBg.png')}
              style={styles.welcomeBtnImage}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  welcomeBtn: {
    width: '100%',
    maxHeight: 150,
    marginVertical: 10,
  },
  welcomeBtnImage: {
    width: '100%',
    height: '100%',
  },
});
