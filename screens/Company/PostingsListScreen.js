import React, {useState} from 'react';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import PostingCrouselView from '../../components/PostingCrouselView';
import PostingListView from '../../components/PostingListView';
import {white} from '../../constants/colors';

export default function PostingsListScreen({
  setProgress,
  drawerAnimationStyle,
  navigation,
}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  const [isCrouselView, setIsCrouselView] = useState(false);
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      {isCrouselView ? (
        <PostingCrouselView
          navigation={navigation}
          setIsCrouselView={setIsCrouselView}
        />
      ) : (
        <PostingListView
          navigation={navigation}
          setIsCrouselView={setIsCrouselView}
        />
      )}
    </Animated.View>
  );
}
