import React, {useEffect} from 'react';
import {useDrawerProgress} from '@react-navigation/drawer';
import Animated from 'react-native-reanimated';
import CustomBottomTabBar from '../../navigation/CustomBottomTabBar';
import Svg, {G, Path} from 'react-native-svg';
import {notification} from '../../assets/icons';
import {
  Text,
  ScrollView,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
} from 'react-native';
import {dark, white} from '../../constants/colors';

export default function Home({
  setProgress,
  drawerAnimationStyle,
  animatedBar,
  navigation,
  isSelected,
  setIsSelected,
}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  useEffect(() => {
    setIsSelected('Home');
  }, []);
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <ImageBackground
        source={require('../../assets/HomeCompanyBg.png')}
        style={{
          flex: 1,
          width: '100%',
        }}>
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: 20,
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.openDrawer();
            }}>
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={25.425}
              height={16.344}>
              <G data-name="Group 496" fill="#0dc5a0">
                <Path
                  data-name="Path 708"
                  d="M24.506 0H.919C.411 0 0 .185 0 .413v2.2c0 .228.412.413.919.413h23.587c.508 0 .919-.185.919-.413V.409c0-.224-.412-.409-.919-.409z"
                />
                <Path
                  data-name="Path 709"
                  d="M16.942 6.66H.635c-.351 0-.635.185-.635.413v2.2c0 .228.285.413.635.413h16.307c.351 0 .635-.185.635-.413v-2.2c.001-.228-.284-.413-.635-.413z"
                />
                <Path
                  data-name="Path 710"
                  d="M12.291 13.318H.461a.439.439 0 00-.461.413v2.2a.439.439 0 00.461.413h11.83a.439.439 0 00.461-.413v-2.2a.439.439 0 00-.461-.413z"
                />
              </G>
            </Svg>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Notification');
            }}>
            <Image source={notification} style={{width: 20, height: 25}} />
          </TouchableOpacity>
        </View>
        <View
          style={{
            display: 'flex',
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            marginVertical: 10,
          }}>
          <Image
            style={{height: 50, resizeMode: 'contain'}}
            source={require('../../assets/logo.png')}
          />
        </View>
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-end',
            paddingVertical: 20,
            paddingHorizontal: 15,
          }}>
          <Text style={{fontSize: 16, color: dark}}>Good Evening,</Text>
          <Text style={{fontSize: 18, fontWeight: '600', color: dark}}>
            {' '}
            Dayyan!
          </Text>
        </View>
        <ScrollView style={{flex: 1, paddingHorizontal: 15, marginBottom: 60}}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: 'Search',
                params: {searchFor: 'professional'},
              });
            }}
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              backgroundColor: white,
              borderRadius: 20,
              padding: 14,
              marginVertical: 8,
            }}>
            <View
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
              }}>
              <Text style={{fontSize: 16, color: dark}}>Search for</Text>
              <Text style={{fontSize: 20, fontWeight: '600', color: dark}}>
                Professional
              </Text>
            </View>
            <View>
              <Image
                source={require('../../assets/JobSeekerSvg.png')}
                style={{width: 100, height: 100}}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: 'Search',
                params: {searchFor: 'freelancer'},
              });
            }}
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              backgroundColor: white,
              borderRadius: 20,
              padding: 14,
              marginVertical: 8,
            }}>
            <View>
              <Image
                source={require('../../assets/FreelancerSvg.png')}
                style={{width: 100, height: 100}}
              />
            </View>
            <View
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
              }}>
              <Text style={{fontSize: 16, color: dark}}>Search for</Text>
              <Text style={{fontSize: 20, fontWeight: '600', color: dark}}>
                Freelancer
              </Text>
            </View>
          </TouchableOpacity>
          <View
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              flexDirection: 'row',
              marginVertical: 8,
            }}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Post A Job');
              }}
              style={{
                width: '47%',
                padding: 14,
                backgroundColor: white,
                borderRadius: 20,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Image
                source={require('../../assets/PostProjectSvg.png')}
                style={{width: 70, height: 70, marginBottom: 10}}
              />
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                }}>
                <Text style={{fontSize: 16, color: dark}}>Post a</Text>
                <Text style={{fontSize: 16, fontWeight: '600', color: dark}}>
                  {''} Job
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Post A Project');
              }}
              style={{
                width: '47%',
                padding: 14,
                backgroundColor: white,
                borderRadius: 20,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={require('../../assets/PostJobSvg.png')}
                style={{width: 70, height: 70, marginBottom: 10}}
              />
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                }}>
                <Text style={{fontSize: 16, color: dark}}>Post a</Text>
                <Text style={{fontSize: 16, fontWeight: '600', color: dark}}>
                  {''} Project
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </ImageBackground>
      <CustomBottomTabBar
        animatedBar={animatedBar}
        navigation={navigation}
        isSelected={isSelected}
        setIsSelected={setIsSelected}
      />
    </Animated.View>
  );
}
