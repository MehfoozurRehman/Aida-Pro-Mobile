import React from 'react';
import {Input, Popup} from '../../components';

const MessagePopup = ({modalVisible, setModalVisible}) => {
  return (
    <Popup
      small={true}
      title="Send Message"
      buttonLabel="Send"
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      onButtonPress={() => {
        setModalVisible(!modalVisible);
      }}>
      <Input placeholder="Write message here" />
    </Popup>
  );
};

export default MessagePopup;
