import React, {useState} from 'react';
import {ScrollView, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import {experianceActive} from '../../assets/icons';
import {Input, Select, Textarea} from '../../components';
import ProjectPreviewPopup from './ProjectPreviewPopup';
import ButtonOverlay from '../../components/ButtonOverlay';
import HeaderPrimary from '../../components/HeaderPrimary';
import ButtonPrimary from '../../components/ButtonPrimary';
import GooglePlacesInput from '../../components/GooglePlacesInput';
import TimeDateInput from '../../components/TimeDateInput';
import CustomBottomTabBar from '../../navigation/CustomBottomTabBar';
import {lightGreen, white} from '../../constants/colors';

export default function PostAProjectScreen({
  setProgress,
  drawerAnimationStyle,
  animatedBar,
  navigation,
  isSelected,
  setIsSelected,
}) {
  const [projectPreviewPopupOpen, setProjectPreviewPopupOpen] = useState(false);
  const progress = useDrawerProgress();
  setProgress(progress);
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary
        title="Post a Project"
        backPath="Home"
        icon={experianceActive}
      />
      <ScrollView
        style={{
          flex: 1,
          width: '100%',
          padding: 15,
        }}>
        <Input
          label="Project Title"
          placeholder="Enter Project Title"
          error={false}
          errorMessage="Please enter job title"
        />
        <TimeDateInput
          label="Start Date"
          placeholder="Select Start Date"
          mode="date"
          error={false}
          errorMessage="Please enter date"
        />
        <TimeDateInput
          label="Deadline"
          placeholder="Select Deadline"
          mode="date"
          error={false}
          errorMessage="Please enter date"
        />
        <GooglePlacesInput
          label="Location"
          placeholder="Select Location"
          secureTextEntry={false}
          error={false}
          errorMessage="Please enter time"
        />
        <Select
          label="Industry"
          placeholder="Select Industry"
          height={500}
          error={false}
          errorMessage="Please enter time"
        />
        <Select
          label="Skill Required"
          placeholder="Select Skill Required"
          height={500}
          error={false}
          errorMessage="Please enter time"
        />
        <Select
          label="Budget Type"
          placeholder="Select Budget Type"
          height={500}
          error={false}
          errorMessage="Please enter time"
        />
        <Textarea
          placeholder="Project Description"
          error={false}
          errorMessage="Please enter time"
        />
        <ButtonPrimary label="Post" />
        <ButtonOverlay style={{backgroundColor: white}} label="Save Draft" />
        <ButtonOverlay
          style={{backgroundColor: lightGreen}}
          label="Preview"
          onPress={() => {
            setProjectPreviewPopupOpen(true);
          }}
        />
        <View style={{marginVertical: 40}}></View>
      </ScrollView>
      <ProjectPreviewPopup
        modalVisible={projectPreviewPopupOpen}
        setModalVisible={setProjectPreviewPopupOpen}
        title="Project Preview"
      />
      <CustomBottomTabBar
        animatedBar={animatedBar}
        navigation={navigation}
        isSelected={isSelected}
        setIsSelected={setIsSelected}
      />
    </Animated.View>
  );
}
