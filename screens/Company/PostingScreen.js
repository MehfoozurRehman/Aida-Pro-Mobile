import React, {useState} from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import CustomBottomTabBar from '../../navigation/CustomBottomTabBar';
import {postingActive} from '../../assets/icons';
import {Checkbox} from '../../components';
import LinearGradientComp from 'react-native-linear-gradient';
import PostingCard from '../../components/PostingCard';
import HeaderPrimary from '../../components/HeaderPrimary';
import ActionConfirmation from '../ActionConfirmation';
import {dark, green200, lightGreen, white} from '../../constants/colors';

function TabButton({iconColors, label, selected, setSelected}) {
  return (
    <TouchableOpacity
      style={{marginRight: 10}}
      onPress={() => {
        setSelected(label);
      }}>
      <LinearGradientComp
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={
          selected === label ? ['#0DC5A1', green200] : [lightGreen, lightGreen]
        }
        style={{
          borderRadius: 50,
          flexDirection: 'row',
          width: 85,
          height: 35,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        {selected === label ? null : (
          <LinearGradientComp
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={iconColors}
            style={{
              width: 15,
              height: 15,
              borderRadius: 50,
              marginRight: 10,
            }}></LinearGradientComp>
        )}

        <Text
          style={{
            fontSize: 12,
            fontWeight: '600',
            color: selected === label ? white : dark,
          }}>
          {label}
        </Text>
      </LinearGradientComp>
    </TouchableOpacity>
  );
}
export default function PostingScreen({
  setProgress,
  drawerAnimationStyle,
  animatedBar,
  navigation,
  isSelected,
  setIsSelected,
}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  const [isConfirmationOpen, setIsConfirmationOpen] = useState(false);
  const options = [
    {
      id: '92iijs7yta',
      name: 'Ondo',
    },
    {
      id: 'a0s0a8ssbsd',
      name: 'Ogun',
    },
    {
      id: '16hbajsabsd',
      name: 'Calabar',
    },
    {
      id: 'nahs75a5sg',
      name: 'Lagos',
    },
    {
      id: '667atsas',
      name: 'Maiduguri',
    },
    {
      id: 'hsyasajs',
      name: 'Anambra',
    },
    {
      id: 'djsjudksjd',
      name: 'Benue',
    },
    {
      id: 'sdhyaysdj',
      name: 'Kaduna',
    },
    {
      id: 'suudydjsjd',
      name: 'Abuja',
    },
    {
      id: '92iijs7yta',
      name: 'Ondo',
    },
    {
      id: 'a0s0a8ssbsd',
      name: 'Ogun',
    },
    {
      id: '16hbajsabsd',
      name: 'Calabar',
    },
    {
      id: 'nahs75a5sg',
      name: 'Lagos',
    },
    {
      id: '667atsas',
      name: 'Maiduguri',
    },
    {
      id: 'hsyasajs',
      name: 'Anambra',
    },
    {
      id: 'djsjudksjd',
      name: 'Benue',
    },
    {
      id: 'sdhyaysdj',
      name: 'Kaduna',
    },
    {
      id: 'suudydjsjd',
      name: 'Abuja',
    },
    {
      id: '92iijs7yta',
      name: 'Ondo',
    },
    {
      id: 'a0s0a8ssbsd',
      name: 'Ogun',
    },
    {
      id: '16hbajsabsd',
      name: 'Calabar',
    },
    {
      id: 'nahs75a5sg',
      name: 'Lagos',
    },
    {
      id: '667atsas',
      name: 'Maiduguri',
    },
    {
      id: 'hsyasajs',
      name: 'Anambra',
    },
    {
      id: 'djsjudksjd',
      name: 'Benue',
    },
    {
      id: 'sdhyaysdj',
      name: 'Kaduna',
    },
    {
      id: 'suudydjsjd',
      name: 'Abuja',
    },
    {
      id: '92iijs7yta',
      name: 'Ondo',
    },
    {
      id: 'a0s0a8ssbsd',
      name: 'Ogun',
    },
    {
      id: '16hbajsabsd',
      name: 'Calabar',
    },
    {
      id: 'nahs75a5sg',
      name: 'Lagos',
    },
    {
      id: '667atsas',
      name: 'Maiduguri',
    },
    {
      id: 'hsyasajs',
      name: 'Anambra',
    },
    {
      id: 'djsjudksjd',
      name: 'Benue',
    },
    {
      id: 'sdhyaysdj',
      name: 'Kaduna',
    },
    {
      id: 'suudydjsjd',
      name: 'Abuja',
    },
    {
      id: '92iijs7yta',
      name: 'Ondo',
    },
    {
      id: 'a0s0a8ssbsd',
      name: 'Ogun',
    },
    {
      id: '16hbajsabsd',
      name: 'Calabar',
    },
    {
      id: 'nahs75a5sg',
      name: 'Lagos',
    },
    {
      id: '667atsas',
      name: 'Maiduguri',
    },
    {
      id: 'hsyasajs',
      name: 'Anambra',
    },
    {
      id: 'djsjudksjd',
      name: 'Benue',
    },
    {
      id: 'sdhyaysdj',
      name: 'Kaduna',
    },
    {
      id: 'suudydjsjd',
      name: 'Abuja',
    },
  ];

  const [selected, setSelected] = useState('Live');
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary
        title="Postings"
        backPath="Home"
        icon={postingActive}
        noBackButton
      />
      <View style={{flex: 1, width: '100%', paddingVertical: 20}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            paddingHorizontal: 20,
          }}>
          <Checkbox label="Freelancer" />
          <Checkbox label="Professional" />
        </View>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          style={{
            marginVertical: 15,
            maxHeight: 40,
            marginHorizontal: 20,
          }}>
          <TabButton
            iconColors={['#0DC5A1', green200]}
            label="Live"
            selected={selected}
            setSelected={setSelected}
          />
          <TabButton
            iconColors={['#F6B038', '#F5833C']}
            label="Drafts"
            selected={selected}
            setSelected={setSelected}
          />
          <TabButton
            iconColors={['#FFDD00', '#E3EB00']}
            label="Hold"
            selected={selected}
            setSelected={setSelected}
          />
          <TabButton
            iconColors={['#DE3F3F', '#FF6161']}
            label="Closed"
            selected={selected}
            setSelected={setSelected}
          />
        </ScrollView>
        <ScrollView
          style={{
            flex: 1,
            width: '100%',
            paddingHorizontal: 20,
            marginBottom: 40,
          }}>
          <PostingCard
            navigation={navigation}
            options={options}
            onDelete={() => {
              setIsConfirmationOpen(true);
            }}
          />
          <PostingCard
            navigation={navigation}
            options={options}
            onDelete={() => {
              setIsConfirmationOpen(true);
            }}
          />
          <PostingCard
            navigation={navigation}
            options={options}
            onDelete={() => {
              setIsConfirmationOpen(true);
            }}
          />
          <PostingCard
            navigation={navigation}
            options={options}
            onDelete={() => {
              setIsConfirmationOpen(true);
            }}
          />
          <PostingCard
            navigation={navigation}
            options={options}
            onDelete={() => {
              setIsConfirmationOpen(true);
            }}
          />
          <PostingCard
            navigation={navigation}
            options={options}
            onDelete={() => {
              setIsConfirmationOpen(true);
            }}
          />
          <PostingCard
            navigation={navigation}
            options={options}
            onDelete={() => {
              setIsConfirmationOpen(true);
            }}
          />
          <PostingCard
            navigation={navigation}
            options={options}
            onDelete={() => {
              setIsConfirmationOpen(true);
            }}
          />
          <PostingCard
            navigation={navigation}
            options={options}
            onDelete={() => {
              setIsConfirmationOpen(true);
            }}
          />
        </ScrollView>
      </View>
      <ActionConfirmation
        modalVisible={isConfirmationOpen}
        setModalVisible={() => {
          setIsConfirmationOpen(false);
        }}
        onAccept={() => {
          setIsConfirmationOpen(false);
        }}
        onDecline={() => {
          setIsConfirmationOpen(false);
        }}
        content="Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab nostrum asperiores et dolor voluptatem provident sunt soluta quisquam. Itaque, odit."
        title="Delete"
      />
      <CustomBottomTabBar
        animatedBar={animatedBar}
        navigation={navigation}
        isSelected={isSelected}
        setIsSelected={setIsSelected}
      />
    </Animated.View>
  );
}
