import React, {useState} from 'react';
import Animated from 'react-native-reanimated';
import LinearGradientComp from 'react-native-linear-gradient';
import Svg, {Path, G, Line} from 'react-native-svg';
import {CustomTabButton} from '../../components';
import {postingDetailsTabItem} from '../../constants/constants';
import SummaryCard from '../../components/SummaryCard';
import SkillsCard from '../../components/SkillsCard';
import LanguagesCard from '../../components/LanguagesCard';
import WorkExperianceCard from '../../components/WorkExperianceCard';
import PortfolioCard from '../../components/PortfolioCard';
import DegreeCard from '../../components/DegreeCard';
import CertificateCard from '../../components/CertificateCard';
import Contact from '../../components/Contact';
import MessagePopup from './MessagePopup';
import VideoCallPopup from './VideoCallPopup';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {
  email,
  address,
  phone,
  social,
  planeActive,
  videoActive,
  cvActive,
} from '../../assets/icons';
import {green200, white} from '../../constants/colors';
import Gallery from 'react-native-image-gallery';

function PostingButtonOverlay({label, style, onPress, icon}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        height: 45,
        marginVertical: 10,
        width: '100%',
        borderRadius: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'rgba(255,255,255,.5)',
        ...style,
      }}>
      {icon ? (
        <Image
          source={icon}
          style={{
            width: 18,
            height: 18,
            marginRight: 10,
            resizeMode: 'contain',
          }}
        />
      ) : null}
      <Text style={{fontSize: 14, fontWeight: '600', color: white}}>
        {label}
      </Text>
    </TouchableOpacity>
  );
}

export default function PostingDetailsScreen({
  setProgress,
  drawerAnimationStyle,
  navigation,
}) {
  // const progress = useDrawerProgress();
  // setProgress(progress);
  const [selected, setSelected] = useState('Profile');
  const [messageModalVisible, setMessageModalVisible] = useState(false);
  const [videoCallModalVisible, setVideoCallModalVisible] = useState(false);
  const [isGalleryOpen, setIsGalleryOpen] = useState(false);

  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      {isGalleryOpen ? (
        <View
          style={{
            backgroundColor: 'black',
            position: 'absolute',
            width: '100%',
            height: '100%',
            zIndex: 999,
          }}>
          <TouchableOpacity
            onPress={() => {
              setIsGalleryOpen(false);
            }}
            style={{
              top: 20,
              right: 20,
              position: 'absolute',
              zIndex: 9999,
            }}>
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width="30"
              height="30"
              viewBox="0 0 24 24"
              fill="none"
              stroke="#ffffff"
              stroke-width="2"
              stroke-linecap="round"
              stroke-linejoin="round"
              class="feather feather-x">
              <Line x1="18" y1="6" x2="6" y2="18"></Line>
              <Line x1="6" y1="6" x2="18" y2="18"></Line>
            </Svg>
          </TouchableOpacity>
          <Gallery
            style={{
              height: '100%',
            }}
            useNativeDriver={true}
            images={[
              {source: {uri: 'http://i.imgur.com/XP2BE7q.jpg'}},
              {source: {uri: 'http://i.imgur.com/5nltiUd.jpg'}},
              {source: {uri: 'http://i.imgur.com/6vOahbP.jpg'}},
              {source: {uri: 'http://i.imgur.com/kj5VXtG.jpg'}},
            ]}
          />
        </View>
      ) : null}

      <LinearGradientComp
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={['#0DC5A1', green200]}
        style={{
          width: '100%',
          borderBottomLeftRadius: 26,
          borderBottomRightRadius: 26,
          justifyContent: 'center',
          alignItems: 'center',
          paddingHorizontal: 20,
          paddingTop: 20,
          paddingBottom: 20,
        }}>
        <View style={{width: '100%'}}>
          <TouchableOpacity
            style={{padding: 5}}
            onPress={() => {
              navigation.navigate('Home');
            }}>
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={20.012}
              height={17.163}>
              <G data-name="Group 505">
                <Path
                  data-name="Path 730"
                  d="M18.809 7.176a1.43 1.43 0 00-.247-.018H4.443l.308-.143a2.864 2.864 0 00.809-.573l3.959-3.959a1.482 1.482 0 00.208-1.9A1.432 1.432 0 007.579.397L.42 7.558a1.432 1.432 0 000 2.025l7.159 7.159a1.432 1.432 0 002.148-.143 1.482 1.482 0 00-.208-1.9l-3.952-3.961a2.864 2.864 0 00-.716-.523l-.43-.193h14.062a1.482 1.482 0 001.511-1.2 1.432 1.432 0 00-1.185-1.646z"
                  fill={white}
                />
              </G>
            </Svg>
          </TouchableOpacity>
        </View>
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            marginVertical: 10,
            alignItems: 'center',
          }}>
          <Image
            source={require('../../assets/userPic.png')}
            style={{
              width: 50,
              height: 50,
              borderRadius: 50,
              marginRight: 10,
            }}
          />
          <View>
            <Text
              style={{
                fontSize: 18,
                fontWeight: 'bold',
                color: white,
                marginBottom: 1,
              }}>
              John James
            </Text>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <LinearGradientComp
                start={{x: 0, y: 0}}
                end={{x: 1, y: 0}}
                colors={[green200, green200]}
                style={{
                  width: 18,
                  height: 18,
                  borderRadius: 50,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginRight: 8,
                  elevation: 1,
                }}
              />
              <Text style={{fontSize: 12, color: white}}>James@gmail.com</Text>
            </View>
          </View>
        </View>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          style={{maxHeight: 50}}>
          <PostingButtonOverlay
            label="Message"
            icon={planeActive}
            style={{
              width: 110,
              height: 40,
              marginRight: 10,
            }}
            onPress={() => {
              setMessageModalVisible(true);
            }}
          />
          <PostingButtonOverlay
            label="Video Call"
            icon={videoActive}
            style={{width: 120, height: 40, marginRight: 10}}
            onPress={() => {
              setVideoCallModalVisible(true);
            }}
          />
          <PostingButtonOverlay
            label="View CV"
            icon={cvActive}
            style={{width: 120, height: 40}}
          />
          <PostingButtonOverlay
            label="Reject"
            icon={cvActive}
            style={{width: 120, height: 40}}
          />
        </ScrollView>
      </LinearGradientComp>
      <View
        style={{
          width: '100%',
          height: 50,
          flexDirection: 'row',
          backgroundColor: white,
          borderTopLeftRadius: 25,
          borderTopRightRadius: 25,
          paddingVertical: 8,
          paddingHorizontal: 20,
          justifyContent: 'space-between',
          marginBottom: 5,
        }}>
        {postingDetailsTabItem.map((item, i) => (
          <CustomTabButton
            key={item.label + i}
            label={item.label}
            icon={item.icon}
            activeIcon={item.iconActive}
            selected={selected}
            setSelected={setSelected}
          />
        ))}
      </View>
      <ScrollView style={{flex: 1, width: '100%', paddingHorizontal: 20}}>
        {selected === 'Profile' ? (
          <>
            <SummaryCard />
            <SkillsCard />
            <LanguagesCard />
          </>
        ) : selected === 'Work Experience' ? (
          <>
            <WorkExperianceCard />
            <WorkExperianceCard />
            <WorkExperianceCard />
            <WorkExperianceCard />
          </>
        ) : selected === 'Project Portfolio' ? (
          <>
            <PortfolioCard setIsGalleryOpen={setIsGalleryOpen} />
            <PortfolioCard setIsGalleryOpen={setIsGalleryOpen} />
            <PortfolioCard setIsGalleryOpen={setIsGalleryOpen} />
            <PortfolioCard setIsGalleryOpen={setIsGalleryOpen} />
          </>
        ) : selected === 'Education' ? (
          <>
            <DegreeCard />
            <CertificateCard />
          </>
        ) : selected === 'Contact' ? (
          <>
            <Contact
              source={email}
              heading="Email"
              subHeading="johndow@gmail.com"
            />
            <Contact source={phone} heading="Phone" subHeading="042 343252" />
            <Contact
              source={address}
              heading="Address"
              subHeading="incididunt ut ero labore et dolore magna aliqua. Ut enim ad minim veniam."
            />
            <Contact
              source={social}
              heading="Socials"
              subHeading="johndow@gmail.com"
              isSocial={true}
            />
          </>
        ) : null}
      </ScrollView>
      <MessagePopup
        modalVisible={messageModalVisible}
        setModalVisible={setMessageModalVisible}
      />
      <VideoCallPopup
        modalVisible={videoCallModalVisible}
        setModalVisible={setVideoCallModalVisible}
      />
    </Animated.View>
  );
}
