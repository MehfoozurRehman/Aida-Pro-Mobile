import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Popup} from '../../components';
import {dark} from '../../constants/colors';

const ProjectPreviewPopup = ({modalVisible, setModalVisible, title}) => {
  return (
    <Popup
      title={title}
      buttonLabel="Edit"
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      onButtonPress={() => {
        setModalVisible(!modalVisible);
      }}>
      <View style={styles.entry}>
        <Text style={styles.heading}>Project title</Text>
        <Text style={styles.value}>Project title</Text>
      </View>
      <View style={styles.entry}>
        <Text style={styles.heading}>Deadline</Text>
        <Text style={styles.value}>Deadline</Text>
      </View>
      <View style={styles.entry}>
        <Text style={styles.heading}>Location</Text>
        <Text style={styles.value}>Location</Text>
      </View>
      <View style={styles.entry}>
        <Text style={styles.heading}>Education</Text>
        <Text style={styles.value}>Education, Education</Text>
      </View>
      <View style={styles.entry}>
        <Text style={styles.heading}>Skills required</Text>
        <Text style={styles.value}>Skills, Skills, Skills</Text>
      </View>
      <View style={styles.entry}>
        <Text style={styles.heading}>Job type</Text>
        <Text style={styles.value}>Job type</Text>
      </View>
      <View style={styles.entry}>
        <Text style={styles.heading}>Salary type</Text>
        <Text style={styles.value}>Salary type</Text>
      </View>
      <View style={styles.entry}>
        <Text style={styles.heading}>Job Descripion</Text>
        <Text style={styles.value}>
          orem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries, but also the leap into electronic typesetting,
          remaining essentially unchanged. It was popularised in th
        </Text>
      </View>
    </Popup>
  );
};

export default ProjectPreviewPopup;

const styles = StyleSheet.create({
  entry: {
    flexDirection: 'row',
    width: '100%',
    paddingVertical: 3,
  },
  heading: {
    color: dark,
    fontWeight: 'bold',
    fontSize: 14,
    width: 105,
  },
  value: {
    color: dark,
    fontSize: 12,
  },
});
