import React from 'react';
import {View} from 'react-native';
import {Checkbox, Range} from '../../components';
import {Popup} from '../../components';
import ButtonPrimary from '../../components/ButtonPrimary';
import ButtonSecondary from '../../components/ButtonSecondary';
import GooglePlacesInput from '../../components/GooglePlacesInput';
import Select from '../../components/Select';

const FiltersPopup = ({modalVisible, setModalVisible, onAccept, onDecline}) => {
  return (
    <Popup
      title="Filters"
      customButton={
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            width: '100%',
          }}>
          <ButtonPrimary
            label="Filter"
            onPress={onAccept}
            style={{maxWidth: '49%', height: 40}}
          />
          <ButtonSecondary
            label="Clear"
            onPress={onDecline}
            style={{maxWidth: '49%', height: 40}}
          />
        </View>
      }
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      onButtonPress={() => {
        setModalVisible(!modalVisible);
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          marginVertical: 10,
        }}>
        <Checkbox label="Freelancer" />
        <Checkbox label="Professional" />
      </View>
      <GooglePlacesInput
        placeholder="Location"
        error={false}
        errorMessage="error"
      />
      <Range />
      <Select label="Skills" placeholder="Select Skills" />
      <Select
        label="Industries worked in"
        placeholder="Select Industries worked in"
      />
      <Select label="Experience level" placeholder="Select Experience level" />
      <Select label="Job Type" placeholder="Select Job Type" />
      <Select
        label="Salary indication"
        placeholder="Select Salary indication"
      />
      <Select label="Education" placeholder="Select Education" />
      <Select label="Availability" placeholder="Select Availability" />
      <Select label="Languages" placeholder="Select Languages" />
    </Popup>
  );
};

export default FiltersPopup;
