import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import {billingActive} from '../../assets/icons';
import PlanDetailsCard from '../../components/PlanDetailsCard';
import HeaderPrimary from '../../components/HeaderPrimary';
import {dark, white} from '../../constants/colors';

export default function BillingAndPaymentsScreen({
  setProgress,
  drawerAnimationStyle,
}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary
        title="Billing & Payments"
        backPath="Home"
        icon={billingActive}
      />
      <ScrollView style={{flex: 1, padding: 20, width: '100%'}}>
        <View style={{width: '100%'}}>
          <Text
            style={{
              fontSize: 18,
              fontWeight: 'bold',
              color: dark,
              marginBottom: 10,
            }}>
            Plan Details
          </Text>
          <ScrollView horizontal={true}>
            <PlanDetailsCard />
            <PlanDetailsCard />
            <PlanDetailsCard />
            <PlanDetailsCard />
          </ScrollView>
        </View>
        <View style={{width: '100%'}}>
          <Text
            style={{
              fontSize: 18,
              fontWeight: 'bold',
              color: dark,
              marginBottom: 10,
            }}>
            Data Usage
          </Text>
          <ScrollView horizontal={true}>
            <PlanDetailsCard />
            <PlanDetailsCard />
            <PlanDetailsCard />
            <PlanDetailsCard />
          </ScrollView>
        </View>
      </ScrollView>
    </Animated.View>
  );
}
