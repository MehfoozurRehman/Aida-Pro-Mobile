import React from 'react';
import {Popup} from '../../components';
import TimeDateInput from '../../components/TimeDateInput';

const VideoCallPopup = ({modalVisible, setModalVisible}) => {
  return (
    <Popup
      small={true}
      title="Request Video Call"
      buttonLabel="Request"
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      onButtonPress={() => {
        setModalVisible(!modalVisible);
      }}>
      <TimeDateInput
        label="Date"
        placeholder="Select Date"
        mode="date"
        error={false}
        errorMessage="Please enter date"
      />
      <TimeDateInput
        label="Time"
        placeholder="Start Time"
        mode="time"
        error={false}
        errorMessage="Please enter time"
      />
      <TimeDateInput
        label="Time"
        placeholder="End Time"
        mode="time"
        error={false}
        errorMessage="Please enter time"
      />
    </Popup>
  );
};

export default VideoCallPopup;
