import React, {useState} from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import CustomBottomTabBar from '../../navigation/CustomBottomTabBar';
import {planActive} from '../../assets/icons';
import Svg, {Path} from 'react-native-svg';
import LinearGradient from 'react-native-linear-gradient';
import CustomRequirementsPopup from '../Global/CustomRequirementsPopup';
import HeaderPrimary from '../../components/HeaderPrimary';
import {lightGreen, white, yellow} from '../../constants/colors';

function PlanBtn({
  onPress,
  planNo,
  planName,
  planDiscription,
  colors,
  coloredBg,
  bordered,
}) {
  return (
    <TouchableOpacity onPress={onPress}>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 0, y: 1}}
        colors={colors}
        style={{
          width: '100%',
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          paddingVertical: 15,
          paddingHorizontal: 25,
          borderRadius: 15,
          marginVertical: 10,
          borderWidth: bordered ? 1 : null,
          borderColor: bordered ? '#A4A4A4' : null,
        }}>
        <View>
          <Text style={{fontSize: 14, color: coloredBg ? white : '#3A3A3A'}}>
            {planNo}
          </Text>
          <Text
            style={{
              fontSize: 16,
              fontWeight: 'bold',
              color: coloredBg ? white : '#3A3A3A',
            }}>
            {planName}
          </Text>
          <Text style={{fontSize: 12, color: coloredBg ? white : '#3A3A3A'}}>
            {planDiscription}
          </Text>
        </View>
        <Svg
          style={{marginBottom: 80}}
          xmlns="http://www.w3.org/2000/svg"
          width={24}
          height={24}
          stroke={coloredBg ? white : '#3A3A3A'}
          strokeWidth={2}
          strokeLinecap="round"
          strokeLinejoin="round"
          className="feather feather-chevron-down">
          <Path d="m6 9 6 6 6-6" />
        </Svg>
      </LinearGradient>
    </TouchableOpacity>
  );
}
function PlanCard({onPress, planNo, planName, children, coloredBg, colors}) {
  return (
    <View>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 0, y: 1}}
        colors={colors}
        style={{
          width: '100%',
          display: 'flex',
          padding: 20,
          borderRadius: 20,
          marginBottom: 20,
        }}>
        <View
          style={{
            width: '100%',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginBottom: 10,
          }}>
          <View>
            <Text style={{fontSize: 14, color: coloredBg ? white : '#3A3A3A'}}>
              {planNo}
            </Text>
            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                color: coloredBg ? white : '#3A3A3A',
              }}>
              {planName}
            </Text>
          </View>
          <TouchableOpacity onPress={onPress}>
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={24}
              height={24}
              stroke={coloredBg ? white : '#3A3A3A'}
              strokeWidth={2}
              strokeLinecap="round"
              strokeLinejoin="round"
              className="feather feather-chevron-up">
              <Path d="m18 15-6-6-6 6" />
            </Svg>
          </TouchableOpacity>
        </View>
        <View>{children}</View>
      </LinearGradient>
    </View>
  );
}
function Plan1Row() {
  return (
    <TouchableOpacity
      style={{
        padding: 20,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: white,
        borderRadius: 10,
        marginVertical: 10,
      }}>
      <Text style={{fontSize: 12, color: yellow, fontWeight: 'bold'}}>
        20 Credits
      </Text>
      <Text
        style={{
          fontSize: 12,
          color: '#3A3A3A',
        }}>
        EUR 19/Credit
      </Text>
    </TouchableOpacity>
  );
}
function Plan2Row() {
  return (
    <TouchableOpacity
      style={{
        padding: 16,
        display: 'flex',
        width: '100%',
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: white,
        borderRadius: 10,
        marginVertical: 10,
      }}>
      <Text style={{fontSize: 12, color: yellow, fontWeight: 'bold'}}>
        EUR 279 / month
      </Text>
      <Text
        style={{
          fontSize: 12,
          color: '#3A3A3A',
        }}>
        EUR 19/Credit
      </Text>
    </TouchableOpacity>
  );
}
export default function PlanAndPricing({
  setProgress,
  drawerAnimationStyle,
  animatedBar,
  navigation,
  isSelected,
  setIsSelected,
}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  const [openPlan1, setOpenPlan1] = useState(false);
  const [openPlan2, setOpenPlan2] = useState(false);
  const [openPlan3, setOpenPlan3] = useState(false);
  const [openPlan4, setOpenPlan4] = useState(false);
  const [
    addCustomizeRequirementsPopupOpen,
    setAddCustomizeRequirementsPopupOpen,
  ] = useState(false);
  const [
    editCustomizeRequirementsPopupOpen,
    setEditCustomizeRequirementsPopupOpen,
  ] = useState(false);
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary
        title="Plans & Pricing"
        backPath="Home"
        icon={planActive}
        imgStyle={{resizeMode: 'contain'}}
        noBackButton
      />
      <ScrollView
        style={{
          flex: 1,
          paddingVertical: 10,
          paddingHorizontal: 15,
          width: '100%',
          marginBottom: 70,
        }}>
        {openPlan1 ? (
          <PlanCard
            planNo="Plan 1"
            planName="Credit Contacts"
            onPress={() => {
              setOpenPlan1(false);
            }}
            colors={[lightGreen, lightGreen]}>
            <View>
              <Plan1Row />
              <Plan1Row />
              <Plan1Row />
            </View>
          </PlanCard>
        ) : (
          <PlanBtn
            colors={[lightGreen, lightGreen]}
            onPress={() => {
              setOpenPlan1(true);
            }}
            planNo="Plan 1"
            planName="Credit Contacts"
            planDiscription="Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt veniam iure aperiam in maxime facere sed fuga modi similique asperiores?"
          />
        )}
        {openPlan2 ? (
          <PlanCard
            planNo="Plan 2"
            planName="Unlimited Contacts"
            colors={['#F6A938', '#F5833C']}
            coloredBg={true}
            onPress={() => {
              setOpenPlan2(false);
            }}>
            <View>
              <Plan2Row />
              <Plan2Row />
              <Plan2Row />
            </View>
          </PlanCard>
        ) : (
          <PlanBtn
            coloredBg={true}
            colors={['#F6A938', '#F5833C']}
            onPress={() => {
              setOpenPlan2(true);
            }}
            planNo="Plan 2"
            planName="Credit Contacts"
            planDiscription="Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt veniam iure aperiam in maxime facere sed fuga modi similique asperiores?"
          />
        )}
        {openPlan3 ? (
          <PlanCard
            planNo="Plan 3"
            planName="Screening of Professionals"
            colors={[lightGreen, lightGreen]}
            onPress={() => {
              setOpenPlan3(false);
            }}>
            <TouchableOpacity
              style={{
                marginVertical: 10,
                padding: 16,
                backgroundColor: white,
                borderRadius: 10,
              }}>
              <Text style={{textAlign: 'left', fontSize: 12, color: '#0B2239'}}>
                Free quotation for customized recruitment of job applicants or
                freelancers. For all your data and AI employment needs.
              </Text>
            </TouchableOpacity>
          </PlanCard>
        ) : (
          <PlanBtn
            colors={[white, white]}
            bordered={true}
            onPress={() => {
              setOpenPlan3(true);
            }}
            planNo="Plan 3"
            planName="Screening of Professionals"
            planDiscription="Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt veniam iure aperiam in maxime facere sed fuga modi similique asperiores?"
          />
        )}
        {openPlan4 ? (
          <PlanCard
            planNo="Plan 4"
            planName="Credit Contacts"
            colors={[lightGreen, lightGreen]}
            onPress={() => {
              setOpenPlan4(false);
            }}>
            <TouchableOpacity
              onPress={() => {
                setAddCustomizeRequirementsPopupOpen(true);
              }}
              style={{
                marginVertical: 10,
                padding: 16,
                backgroundColor: white,
                borderRadius: 10,
              }}>
              <Text style={{textAlign: 'left', fontSize: 12, color: '#0B2239'}}>
                Free quotation for customized recruitment of job applicants or
                freelancers. For all your data and AI employment needs.
              </Text>
            </TouchableOpacity>
          </PlanCard>
        ) : (
          <PlanBtn
            colors={[white, white]}
            bordered={true}
            onPress={() => {
              setOpenPlan4(true);
            }}
            planNo="Plan 4"
            planName="Customized Recruitment"
            planDiscription="Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt veniam iure aperiam in maxime facere sed fuga modi similique asperiores?"
          />
        )}
      </ScrollView>
      <CustomRequirementsPopup
        modalVisible={addCustomizeRequirementsPopupOpen}
        setModalVisible={setAddCustomizeRequirementsPopupOpen}
        title="Customize Requirements"
      />
      <CustomBottomTabBar
        animatedBar={animatedBar}
        navigation={navigation}
        isSelected={isSelected}
        setIsSelected={setIsSelected}
      />
    </Animated.View>
  );
}
