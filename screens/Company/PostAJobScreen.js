import React, {useState} from 'react';
import {ScrollView, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import {experianceActive} from '../../assets/icons';
import {Input, Select, Textarea} from '../../components';
import JobPreviewPopup from './JobPreviewPopup';
import TimeDateInput from '../../components/TimeDateInput';
import GooglePlacesInput from '../../components/GooglePlacesInput';
import ButtonOverlay from '../../components/ButtonOverlay';
import ButtonPrimary from '../../components/ButtonPrimary';
import HeaderPrimary from '../../components/HeaderPrimary';
import CustomBottomTabBar from '../../navigation/CustomBottomTabBar';
import {lightGreen, white} from '../../constants/colors';

export default function PostAJobScreen({
  setProgress,
  drawerAnimationStyle,
  animatedBar,
  navigation,
  isSelected,
  setIsSelected,
}) {
  const [jobPreviewPopup, setJobPreviewPopup] = useState(false);
  const progress = useDrawerProgress();
  setProgress(progress);
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary
        title="Post a Job"
        backPath="Home"
        icon={experianceActive}
      />
      <ScrollView
        style={{
          flex: 1,
          width: '100%',
          paddingVertical: 15,
          paddingHorizontal: 15,
        }}
        keyboardShouldPersistTaps={'handled'}>
        <Input
          label="Job Title"
          placeholder="Enter Job Title"
          error={false}
          errorMessage="Please enter job title"
        />
        <TimeDateInput
          label="Deadline"
          placeholder="Select Deadline"
          mode="date"
          error={false}
          errorMessage="Please enter date"
        />
        <GooglePlacesInput
          label="Location"
          placeholder="Select Location"
          secureTextEntry={false}
          error={false}
          errorMessage="Please enter time"
        />
        <Select
          label="Education"
          placeholder="Select Education"
          height={500}
          error={false}
          errorMessage="Please enter time"
        />
        <Select
          label="Skill Required"
          placeholder="Select Skill Required"
          height={500}
          error={false}
          errorMessage="Please enter time"
        />
        <Select
          label="Job Type"
          placeholder="Select Job Type"
          height={500}
          error={false}
          errorMessage="Please enter time"
        />
        <Select
          label="Salary Type"
          placeholder="Select Salary Type"
          height={500}
          error={false}
          errorMessage="Please enter time"
        />
        <Textarea
          placeholder="Job Description"
          error={false}
          errorMessage="Please enter time"
        />
        <Textarea
          placeholder="Job Requirements"
          error={false}
          errorMessage="Please enter time"
        />
        <ButtonPrimary label="Post" />
        <ButtonOverlay style={{backgroundColor: white}} label="Save Draft" />
        <ButtonOverlay
          style={{backgroundColor: lightGreen}}
          label="Preview"
          onPress={() => {
            setJobPreviewPopup(true);
          }}
        />
        <View style={{marginVertical: 40}}></View>
      </ScrollView>
      <JobPreviewPopup
        modalVisible={jobPreviewPopup}
        setModalVisible={setJobPreviewPopup}
        title="Job Preview"
      />
      <CustomBottomTabBar
        animatedBar={animatedBar}
        navigation={navigation}
        isSelected={isSelected}
        setIsSelected={setIsSelected}
      />
    </Animated.View>
  );
}
