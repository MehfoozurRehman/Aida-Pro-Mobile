import React, {useState} from 'react';
import {useDrawerProgress} from '@react-navigation/drawer';
import Animated from 'react-native-reanimated';
import CustomBottomTabBar from '../../navigation/CustomBottomTabBar';
import Svg, {Path, G} from 'react-native-svg';
import {ProjectCard} from '../../components';
import {notification} from '../../assets/icons';
import {FiltersPopup} from './';
import SearchInput from '../../components/SearchInput';
import {
  Text,
  ScrollView,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  FlatList,
} from 'react-native';
import {dark, white} from '../../constants/colors';

export default function Home({
  setProgress,
  drawerAnimationStyle,
  animatedBar,
  navigation,
  isSelected,
  setIsSelected,
}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  const [modalVisible, setModalVisible] = useState(false);
  const DATA = [
    {
      id: '1',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '2',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '3',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '4',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '5',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '6',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '7',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '8',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '9',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
  ];
  const renderItem = ({item}) => (
    <ProjectCard
      onPress={() => {
        navigation.navigate('Job Preview');
      }}
    />
  );

  return (
    <Animated.View
      style={{
        flex: 1,
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <ImageBackground
        style={{flex: 1, width: '100%'}}
        source={require('../../assets/HomeFreelancerBg.png')}>
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: 20,
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.openDrawer();
            }}>
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={25.425}
              height={16.344}>
              <G data-name="Group 496" fill="#0dc5a0">
                <Path
                  data-name="Path 708"
                  d="M24.506 0H.919C.411 0 0 .185 0 .413v2.2c0 .228.412.413.919.413h23.587c.508 0 .919-.185.919-.413V.409c0-.224-.412-.409-.919-.409z"
                />
                <Path
                  data-name="Path 709"
                  d="M16.942 6.66H.635c-.351 0-.635.185-.635.413v2.2c0 .228.285.413.635.413h16.307c.351 0 .635-.185.635-.413v-2.2c.001-.228-.284-.413-.635-.413z"
                />
                <Path
                  data-name="Path 710"
                  d="M12.291 13.318H.461a.439.439 0 00-.461.413v2.2a.439.439 0 00.461.413h11.83a.439.439 0 00.461-.413v-2.2a.439.439 0 00-.461-.413z"
                />
              </G>
            </Svg>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Notification');
            }}>
            <Image source={notification} style={{width: 20, height: 25}} />
          </TouchableOpacity>
        </View>
        <FlatList
          ListHeaderComponent={
            <>
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  marginVertical: 20,
                }}>
                <Text style={{fontSize: 18, color: dark}}>Find</Text>
                <Text style={{fontSize: 22, fontWeight: 'bold', color: dark}}>
                  Best Jobs!
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Map');
                }}
                style={{
                  width: '100%',
                  justifyContent: 'center',
                  borderRadius: 20,
                  marginBottom: 10,
                }}>
                <Image
                  source={require('../../assets/HomeFreelancerMapPic.png')}
                  style={{width: '100%', height: 200, borderRadius: 20}}
                />
              </TouchableOpacity>
              <SearchInput
                isFilter={true}
                placeholder="Search"
                onPressFilter={() => {
                  setModalVisible(true);
                }}
                svg={
                  <Svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={20.681}
                    height={20.681}>
                    <G
                      data-name="Icon feather-search"
                      fill="none"
                      stroke="#545454"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={1}>
                      <Path
                        data-name="Path 2299"
                        d="M17.553 9.526A8.026 8.026 0 119.526 1.5a8.026 8.026 0 018.027 8.026z"
                      />
                      <Path
                        data-name="Path 2300"
                        d="M19.559 19.559l-4.364-4.364"
                      />
                    </G>
                  </Svg>
                }
              />
              <Text style={{fontSize: 18, color: dark, marginVertical: 10}}>
                Recommended Jobs
              </Text>
            </>
          }
          style={{flex: 1, paddingHorizontal: 20, marginBottom: 60}}
          data={DATA}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
      </ImageBackground>
      <CustomBottomTabBar
        animatedBar={animatedBar}
        navigation={navigation}
        isSelected={isSelected}
        setIsSelected={setIsSelected}
      />
      <FiltersPopup
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
      />
    </Animated.View>
  );
}
