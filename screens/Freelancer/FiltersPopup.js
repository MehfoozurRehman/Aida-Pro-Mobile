import React from 'react';
import {View} from 'react-native';
import {Input, Range} from '../../components';
import {Popup} from '../../components';
import ButtonPrimary from '../../components/ButtonPrimary';
import ButtonSecondary from '../../components/ButtonSecondary';
import GooglePlacesInput from '../../components/GooglePlacesInput';
import Select from '../../components/Select';

const FiltersPopup = ({modalVisible, setModalVisible, onAccept, onDecline}) => {
  return (
    <Popup
      title="Filters"
      customButton={
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            width: '100%',
          }}>
          <ButtonPrimary
            label="Filter"
            onPress={onAccept}
            style={{maxWidth: '49%', height: 40}}
          />
          <ButtonSecondary
            label="Clear"
            onPress={onDecline}
            style={{maxWidth: '49%', height: 40}}
          />
        </View>
      }
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      onButtonPress={() => {
        setModalVisible(!modalVisible);
      }}>
      <GooglePlacesInput
        placeholder="Location"
        error={false}
        errorMessage="error"
      />
      <Range />
      <Select label="Date of posting" placeholder="Select date of posting" />
      <Select label="Contact type" placeholder="Select contact type" />
      <Select label="Field of work" placeholder="Select field of work" />
      <Input
        label="Salary estimation (monthly) €"
        placeholder="Enter min value"
      />
      <Input
        label="Salary estimation (monthly) €"
        placeholder="Enter max value"
      />
    </Popup>
  );
};

export default FiltersPopup;
