import * as React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import LinearGradient from 'react-native-linear-gradient';
import {
  ImageBackground,
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {white} from '../constants/colors';

function OnboardingScreen1({navigation}) {
  return (
    <SafeAreaView style={{flex: 1}}>
      <ImageBackground
        source={require('../assets/onboardingScreenBg2.png')}
        style={{
          flex: 1,
          justifyContent: 'flex-end',
          alignItems: 'center',
        }}>
        <View style={styles.onboardingInfo}>
          <Text style={styles.onboardingInfoText}>Welcome to the</Text>
          <Text style={styles.onboardingInfoTextBold}>AIDApro</Text>
          <Text style={styles.onboardingInfoText}>community</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
            paddingHorizontal: 20,
            alignItems: 'center',
            marginBottom: 20,
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Welcome');
            }}
            style={styles.onboardingBtn}>
            <Text style={styles.onboardingBtnText}>Skip</Text>
          </TouchableOpacity>
          <View style={{display: 'flex', flexDirection: 'row', marginLeft: 20}}>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={['#F6B038', '#F5833C']}
              style={{
                width: 12,
                height: 12,
                backgroundColor: white,
                borderRadius: 12,
                marginRight: 6,
              }}></LinearGradient>
            <View
              style={{
                width: 12,
                height: 12,
                backgroundColor: white,
                borderRadius: 12,
                marginRight: 6,
              }}></View>
            <View
              style={{
                width: 12,
                height: 12,
                backgroundColor: white,
                borderRadius: 12,
              }}></View>
          </View>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('OnboardingScreen2');
            }}
            style={styles.onboardingBtn}>
            <Text style={styles.onboardingBtnText}>Get Started</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
}

function OnboardingScreen2({navigation}) {
  return (
    <SafeAreaView style={{flex: 1}}>
      <ImageBackground
        source={require('../assets/onboardingScreenBg1.png')}
        style={{
          flex: 1,
          justifyContent: 'flex-end',
          alignItems: 'center',
        }}>
        <View style={styles.onboardingInfo}>
          <Text style={styles.onboardingInfoText}>
            Find the latest data and
          </Text>
          <Text style={styles.onboardingInfoTextBold}>AI Vacancies</Text>
          <Text style={styles.onboardingInfoText}>and</Text>
          <Text style={styles.onboardingInfoTextBold}>Projects</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            width: '100%',
            paddingHorizontal: 20,
            marginBottom: 20,
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Welcome');
            }}
            style={styles.onboardingBtn}>
            <Text style={styles.onboardingBtnText}>Skip</Text>
          </TouchableOpacity>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
            }}>
            <View
              style={{
                width: 12,
                height: 12,
                backgroundColor: white,
                borderRadius: 12,
                marginRight: 6,
              }}></View>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={['#F6B038', '#F5833C']}
              style={{
                width: 12,
                height: 12,
                backgroundColor: white,
                borderRadius: 12,
                marginRight: 6,
              }}></LinearGradient>
            <View
              style={{
                width: 12,
                height: 12,
                backgroundColor: white,
                borderRadius: 12,
              }}></View>
          </View>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('OnboardingScreen3');
            }}
            style={styles.onboardingBtn}>
            <Text style={styles.onboardingBtnText}>Next</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
}
function OnboardingScreen3({navigation}) {
  return (
    <SafeAreaView style={{flex: 1}}>
      <ImageBackground
        source={require('../assets/onboardingScreenBg3.png')}
        style={{
          flex: 1,
          justifyContent: 'flex-end',
          alignItems: 'center',
        }}>
        <View
          style={{
            flexDirection: 'row',
            marginBottom: 30,
            flexWrap: 'wrap',
            justifyContent: 'center',
            maxWidth: '80%',
          }}>
          <Text style={styles.onboardingInfoText}>A place where data and</Text>
          <Text style={styles.onboardingInfoTextBold}>
            AI professionals and companies
          </Text>
          <Text style={styles.onboardingInfoText}>
            meet to share experiences,
          </Text>
          <Text style={styles.onboardingInfoText}>
            knowledge, ask questions.
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Welcome');
          }}
          style={styles.onboardingBtn}>
          <Text style={styles.onboardingBtnText}>Finish</Text>
        </TouchableOpacity>
        <View style={{display: 'flex', flexDirection: 'row', marginBottom: 30}}>
          <View
            style={{
              width: 12,
              height: 12,
              backgroundColor: white,
              borderRadius: 12,
              marginRight: 6,
            }}></View>
          <View
            style={{
              width: 12,
              height: 12,
              backgroundColor: white,
              borderRadius: 12,
              marginRight: 6,
            }}></View>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={['#F6B038', '#F5833C']}
            style={{
              width: 12,
              height: 12,
              backgroundColor: white,
              borderRadius: 12,
            }}></LinearGradient>
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
}

const Tab = createMaterialTopTabNavigator();

export default function Onboarding() {
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarStyle: {display: 'none'},
      }}>
      <Tab.Screen name="OnboardingScreen1" component={OnboardingScreen1} />
      <Tab.Screen name="OnboardingScreen2" component={OnboardingScreen2} />
      <Tab.Screen name="OnboardingScreen3" component={OnboardingScreen3} />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  onboardingBtn: {
    borderBottomColor: white,
    borderBottomWidth: 2,
    paddingVertical: 5,
    minWidth: 40,
    alignItems: 'center',
    marginBottom: 14,
  },
  onboardingBtnText: {
    fontSize: 16,
    color: white,
  },
  onboardingInfo: {
    flexDirection: 'row',
    marginBottom: 60,
    flexWrap: 'wrap',
    justifyContent: 'center',
    maxWidth: '80%',
  },
  onboardingInfoText: {
    fontSize: 16,
    color: white,
  },
  onboardingInfoTextBold: {
    fontSize: 18,
    color: white,
    fontWeight: '600',
    marginHorizontal: 5,
  },
});
