import React from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {user, contact, faq, privacy, terms} from '../../assets/icons';
import Svg, {Path} from 'react-native-svg';
import {role} from '../../constants/constants';
import HeaderPrimary from '../../components/HeaderPrimary';
import {useNavigation} from '@react-navigation/native';
import ButtonPrimary from '../../components/ButtonPrimary';
import {useDrawerProgress} from '@react-navigation/drawer';
import {dark, lightGreen, white} from '../../constants/colors';

function ProfileLink({icon, label, toPath}) {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate(toPath);
      }}
      style={{
        flexDirection: 'row',
        backgroundColor: lightGreen,
        marginBottom: 15,
        borderRadius: 50,
        paddingHorizontal: 20,
        paddingVertical: 10,
        alignItems: 'center',
      }}>
      <Image
        source={icon}
        style={{width: 20, height: 20, resizeMode: 'contain', marginRight: 10}}
      />
      <Text style={{color: dark, flex: 1, fontWeight: '500'}}>{label}</Text>
      <Svg
        xmlns="http://www.w3.org/2000/svg"
        width={7.196}
        height={12.588}
        viewBox="0 0 7.196 12.588">
        <Path
          data-name="Icon ionic-ios-arrow-back"
          d="M2.169 6.292l4.763-4.759A.9.9 0 105.658.263L.262 5.655A.9.9 0 00.236 6.9l5.419 5.43a.9.9 0 001.274-1.27z"
          transform="rotate(180 3.598 6.294)"
          fill={dark}
        />
      </Svg>
    </TouchableOpacity>
  );
}

export default function ProfileScreen({
  drawerAnimationStyle,
  setProgress,
  navigation,
}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary
        title="John Doe"
        backPath="Home"
        icon={require('../../assets/userPic.png')}
        imgStyle={{borderRadius: 50, width: 60, height: 60}}
      />
      <ScrollView style={{flex: 1, width: '100%', padding: 10, marginTop: 10}}>
        <ProfileLink
          icon={user}
          label="Profile"
          toPath={role === 'Company' ? 'Company Details' : 'Personal Details'}
        />
        <ProfileLink
          icon={user}
          label="Change Password"
          toPath="Change Password"
        />
        <ProfileLink
          icon={user}
          label="User Settings"
          toPath={role === 'Company' ? 'Company Details' : 'Personal Details'}
        />
        <ProfileLink
          icon={privacy}
          label="Privacy & Policy"
          toPath="Privacy & Policy"
        />
        <ProfileLink
          icon={terms}
          label="Terms & Conditions"
          toPath="Terms & Conditions"
        />
        <ProfileLink icon={faq} label="Faq" toPath="FAQ" />
        <ProfileLink icon={user} label="About Us" toPath="About Us" />
        <ProfileLink icon={contact} label="Contact Us" toPath="Contact Us" />
      </ScrollView>
      <View style={{padding: 20, width: '60%'}}>
        <ButtonPrimary
          label="Logout"
          onPress={() => {
            navigation.navigate('Login');
          }}
        />
      </View>
    </Animated.View>
  );
}
