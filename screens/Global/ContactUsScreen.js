import React, {useEffect, useState} from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import {contactActive} from '../../assets/icons';
import {Input, Textarea} from '../../components';
import Svg, {Path, G} from 'react-native-svg';
import ButtonPrimary from '../../components/ButtonPrimary';
import {role} from '../../constants/constants';
import HeaderPrimary from '../../components/HeaderPrimary';
import {dark, white} from '../../constants/colors';

const contactUsInputBoxListCompany = [
  {
    placeholder: 'Company Name',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Contact Person',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Email',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Phone',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Subject',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
];
const contactUsInputBoxListOther = [
  {
    placeholder: 'First Name',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Last Name',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Email',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Phone',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
  {
    placeholder: 'Subject',
    secureTextEntry: false,
    transparent: false,
    svg: (
      <Svg xmlns="http://www.w3.org/2000/svg" width={18.219} height={12.246}>
        <G data-name="Group 202">
          <Path
            data-name="Path 292"
            d="M17.15.15H1.069a.922.922 0 00-.919.919v10.108a.922.922 0 00.919.919H17.15a.922.922 0 00.919-.919V1.069A.922.922 0 0017.15.15zm-.345.689L9.638 6.217a.961.961 0 01-1.057 0L1.414.839zm-3.828 5.73l3.905 4.824.013.013H1.324l.013-.013 3.905-4.824a.345.345 0 00-.536-.434L.839 10.917V1.27l7.328 5.5a1.645 1.645 0 001.884 0l7.328-5.5v9.647L13.512 6.14a.345.345 0 00-.536.434z"
            fill={dark}
            stroke={dark}
            strokeWidth={0.3}
          />
        </G>
      </Svg>
    ),
  },
];
export default function ContactUsScreen({setProgress, drawerAnimationStyle}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  const [contactUsInputBoxList, setContactUsInputBoxList] = useState(
    contactUsInputBoxListCompany,
  );
  useEffect(() => {
    setContactUsInputBoxList(
      role === 'Company'
        ? contactUsInputBoxListCompany
        : contactUsInputBoxListOther,
    );
  }, [role]);
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary
        title="Contact Us"
        backPath="Profile"
        icon={contactActive}
      />
      <Text
        style={{
          fontSize: 22,
          fontWeight: 'bold',
          color: dark,
          width: '100%',
          marginLeft: 30,
          marginTop: 20,
        }}>
        Get in Touch
      </Text>
      <ScrollView
        style={{
          flex: 1,
          width: '100%',
          paddingVertical: 20,
          paddingHorizontal: 15,
          paddingTop: 5,
        }}>
        {contactUsInputBoxList.map((inputData, i) => (
          <Input
            key={i}
            placeholder={inputData.placeholder}
            secureTextEntry={inputData.secureTextEntry}
            svg={inputData.svg}
            transparent={inputData.transparent}
            variant={inputData.variant}
          />
        ))}
        <Textarea placeholder="Message" />
        <ButtonPrimary label="Send" />
        <View
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            marginVertical: 20,
          }}>
          <Text style={{fontSize: 14, color: dark}}>Phone</Text>
          <Text style={{fontSize: 12, color: dark}}>354 454 5344 343</Text>
          <Text style={{fontSize: 12, color: dark, marginBottom: 20}}>
            354 454 5344 343
          </Text>
          <Text style={{fontSize: 14, marginTop: 10, color: dark}}>Email</Text>
          <Text style={{fontSize: 12, color: dark}}>hello@gmail.com</Text>
          <Text style={{fontSize: 12, color: dark, marginBottom: 20}}>
            abc@gmail.com
          </Text>
          <Text style={{fontSize: 14, marginVertical: 10, color: dark}}>
            Social
          </Text>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              style={{
                width: 40,
                height: 40,
                borderRadius: 40,
                backgroundColor: '#F6A938',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                marginRight: 6,
              }}>
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={9.294}
                height={18.587}>
                <Path
                  d="M7.598 3.086h1.7V.131A21.912 21.912 0 006.822 0C4.376 0 2.7 1.539 2.7 4.367v2.6H0v3.3h2.7v8.313h3.313v-8.305h2.591l.411-3.3h-3v-2.28c0-.955.258-1.609 1.588-1.609z"
                  fill="#fff"
                />
              </Svg>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                width: 40,
                height: 40,
                borderRadius: 40,
                backgroundColor: '#F6A938',
                display: 'flex',
                justifyContent: 'center',
                marginRight: 6,
                alignItems: 'center',
              }}>
              <Svg
                data-name="instagram (1)"
                xmlns="http://www.w3.org/2000/svg"
                width={18.587}
                height={18.587}>
                <Path
                  data-name="Path 148"
                  d="M10.927 9.294A1.634 1.634 0 119.294 7.66a1.634 1.634 0 011.633 1.634zm0 0"
                  fill="#fff"
                />
                <Path
                  data-name="Path 149"
                  d="M12.056 4.356H6.538a2.181 2.181 0 00-2.182 2.178v5.522a2.181 2.181 0 002.178 2.178h5.522a2.181 2.181 0 002.178-2.178V6.538a2.181 2.181 0 00-2.178-2.182zm-2.759 7.66a2.723 2.723 0 112.723-2.723 2.726 2.726 0 01-2.727 2.723zm3.122-5.3a.545.545 0 11.545-.545.545.545 0 01-.549.545zm0 0"
                  fill="#fff"
                />
                <Path
                  data-name="Path 150"
                  d="M13.686 0H4.9A4.907 4.907 0 000 4.9v8.785a4.907 4.907 0 004.9 4.9h8.785a4.907 4.907 0 004.9-4.9V4.9A4.907 4.907 0 0013.686 0zm1.634 12.053a3.271 3.271 0 01-3.267 3.267H6.535a3.271 3.271 0 01-3.267-3.267V6.535a3.271 3.271 0 013.267-3.268h5.518a3.271 3.271 0 013.267 3.268zm0 0"
                  fill="#fff"
                />
              </Svg>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                width: 40,
                height: 40,
                borderRadius: 40,
                backgroundColor: '#F6A938',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                marginRight: 6,
              }}>
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={20.017}
                height={16.264}>
                <Path
                  d="M17.96 4.056a8.192 8.192 0 002.057-2.13 8.57 8.57 0 01-2.365.648A4.082 4.082 0 0019.458.306a8.211 8.211 0 01-2.6.993 4.1 4.1 0 00-7.1 2.806 4.225 4.225 0 00.1.936A11.618 11.618 0 011.394.75a4.1 4.1 0 001.261 5.482A4.056 4.056 0 01.8 5.727v.045a4.123 4.123 0 003.288 4.033 4.081 4.081 0 01-1.076.135 3.622 3.622 0 01-.777-.07 4.144 4.144 0 003.835 2.858 8.249 8.249 0 01-5.089 1.75A7.761 7.761 0 010 14.422a11.557 11.557 0 006.3 1.842A11.6 11.6 0 0017.96 4.056z"
                  fill="#fff"
                />
              </Svg>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                width: 40,
                height: 40,
                borderRadius: 40,
                backgroundColor: '#F6A938',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                marginRight: 6,
              }}>
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={19.362}
                height={19.362}>
                <G data-name="Group 81">
                  <G data-name="Group 80">
                    <Path
                      data-name="Path 144"
                      d="M4.3 7.301L1 5.401a9.617 9.617 0 00.014 8.56l3.284-1.9a5.835 5.835 0 010-4.76z"
                      fill="#fff"
                    />
                  </G>
                </G>
                <G data-name="Group 83">
                  <G data-name="Group 82">
                    <Path
                      data-name="Path 145"
                      d="M16.031 2.374A9.68 9.68 0 001.631 4.3l3.3 1.906a5.866 5.866 0 018.492-1.072.626.626 0 00.842-.041l1.8-1.8a.634.634 0 00-.034-.919z"
                      fill="#fff"
                    />
                  </G>
                </G>
                <G data-name="Group 85">
                  <G data-name="Group 84">
                    <Path
                      data-name="Path 146"
                      d="M19.296 8.561a.635.635 0 00-.63-.563h-7.933a.631.631 0 00-.631.631v2.523a.631.631 0 00.631.631h4.445a5.938 5.938 0 01-2.026 2.648l1.888 3.27a9.887 9.887 0 003.982-5.534 9.318 9.318 0 00.274-3.606z"
                      fill="#fff"
                    />
                  </G>
                </G>
                <G data-name="Group 87">
                  <G data-name="Group 86">
                    <Path
                      data-name="Path 147"
                      d="M12.058 15.065a5.856 5.856 0 01-7.13-1.913l-3.286 1.9a9.671 9.671 0 008.036 4.312 9.483 9.483 0 004.269-1.024z"
                      fill="#fff"
                    />
                  </G>
                </G>
              </Svg>
            </TouchableOpacity>
          </View>
          <Image
            source={require('../../assets/ContactUsPic.png')}
            style={{marginVertical: 20}}
          />
        </View>
      </ScrollView>
    </Animated.View>
  );
}
