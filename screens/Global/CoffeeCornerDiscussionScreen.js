import React, {useState} from 'react';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import HeaderSimple from '../../components/HeaderSimple';
import SearchInput from '../../components/SearchInput';
import Svg, {Path} from 'react-native-svg';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import SharePopup from './SharePopup';
import {dark, lightGreen, white} from '../../constants/colors';

export default function CoffeeCornerDiscussionScreen({
  setProgress,
  drawerAnimationStyle,
}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  const [shareModalVisible, setShareModalVisible] = useState(false);
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderSimple backPath="Coffee Corner" title="Coffee Corner" />
      <SharePopup
        setModalVisible={setShareModalVisible}
        modalVisible={shareModalVisible}
      />
      <ScrollView
        style={{
          flex: 1,
          width: '100%',
          paddingVertical: 30,
          paddingHorizontal: 15,
          paddingTop: 0,
        }}>
        <Text
          style={{
            fontSize: 22,
            fontWeight: '600',
            color: dark,
            marginBottom: 15,
          }}>
          Topic of Discussion
        </Text>
        <ScrollView horizontal={true} style={{width: '100%'}}>
          <Text style={styles.badge}>Machine Learning</Text>
          <Text style={styles.badge}>Python</Text>
          <Text style={styles.badge}>Artificial Intellegance </Text>
          <Text style={styles.badge}>Machine Learning</Text>
          <Text style={styles.badge}>Python</Text>
          <Text style={styles.badge}>Artificial Intellegance </Text>
        </ScrollView>
        <Text
          style={{
            fontSize: 12,
            width: '100%',
            color: dark,
            marginTop: 10,
            borderBottomColor: '#707070',
            borderBottomWidth: 1,
            paddingBottom: 10,
          }}>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries, but also the leap into electronic typesetting,
          remaining essentially unchanged. It was popularised in the 1960s with
          the release of Letraset sheets containing Lorem Ipsum passages, and
          more recently with desktop publishing software like Aldus PageMaker
          including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of
          the printing and typesetting industry. Lorem Ipsum has been the
          industry's
        </Text>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 10,
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity>
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={15.034}
                height={10.31}
                viewBox="0 0 18.034 10.31">
                <Path
                  data-name="Icon ionic-ios-arrow-back"
                  d="M3.108 9.015L9.932 2.2A1.289 1.289 0 008.106.376L.375 8.1a1.286 1.286 0 00-.037 1.779L8.1 17.658a1.289 1.289 0 001.825-1.82z"
                  transform="rotate(90 9.017 9.017)"
                  fill={dark}
                />
              </Svg>
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 12,
                color: dark,
                fontWeight: '600',
                marginHorizontal: 10,
              }}>
              3242
            </Text>
            <TouchableOpacity>
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={15.034}
                height={10.31}
                viewBox="0 0 18.034 10.31">
                <Path
                  data-name="Icon ionic-ios-arrow-back"
                  d="M3.108 9.015L9.932 2.2A1.289 1.289 0 008.106.376L.375 8.1a1.286 1.286 0 00-.037 1.779L8.1 17.658a1.289 1.289 0 001.825-1.82z"
                  transform="rotate(-90 5.155 5.155)"
                  fill={dark}
                />
              </Svg>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                marginLeft: 20,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={13.252}
                height={13.252}
                viewBox="0 0 13.252 13.252">
                <Path
                  data-name="Icon material-mode-comment"
                  d="M16.245 4.325A1.323 1.323 0 0014.927 3H4.325A1.329 1.329 0 003 4.325v7.951A1.329 1.329 0 004.325 13.6H13.6l2.65 2.65z"
                  transform="translate(-3 -3)"
                  fill={dark}
                />
              </Svg>
              <Text
                style={{
                  fontSize: 12,
                  color: dark,
                  fontWeight: '600',
                  marginHorizontal: 10,
                }}>
                3242
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setShareModalVisible(true);
              }}>
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={12.638}
                height={14.443}
                viewBox="0 0 12.638 14.443">
                <Path
                  data-name="Icon awesome-share-alt"
                  d="M9.929 9.027a2.7 2.7 0 00-1.687.589l-2.89-1.807a2.724 2.724 0 000-1.176l2.891-1.806A2.7 2.7 0 107.286 3.3L4.395 5.1a2.708 2.708 0 100 4.237l2.891 1.807a2.708 2.708 0 102.644-2.12z"
                  fill={dark}
                />
              </Svg>
            </TouchableOpacity>
          </View>
          <Text
            style={{
              fontSize: 12,
              color: dark,
              marginHorizontal: 10,
              width: '35%',
            }}>
            Posted by john doe 7 hours ago
          </Text>
        </View>
        <Text
          style={{
            fontSize: 18,
            color: dark,
            fontWeight: '600',
            marginTop: 10,
          }}>
          Comments
        </Text>
        <View
          style={{
            width: 60,
            height: 2,
            backgroundColor: '#0DC5A1',
            marginTop: 1,
            marginBottom: 10,
          }}
        />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginBottom: 10,
          }}>
          <Image
            source={require('../../assets/userPic.png')}
            style={{width: 45, height: 45, borderRadius: 45, marginRight: 10}}
          />
          <SearchInput
            placeholder="Write Reply"
            style={{height: 45, flex: 1}}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginBottom: 10,
          }}>
          <Image
            source={require('../../assets/userPic.png')}
            style={{width: 45, height: 45, borderRadius: 45, marginRight: 10}}
          />
          <View
            style={{
              backgroundColor: lightGreen,
              padding: 10,
              borderRadius: 10,
              flex: 1,
            }}>
            <Text style={{color: dark, fontSize: 12, fontWeight: '500'}}>
              User Name
            </Text>
            <Text style={{color: '#949494', fontSize: 12}}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginBottom: 10,
          }}>
          <Image
            source={require('../../assets/userPic.png')}
            style={{width: 45, height: 45, borderRadius: 45, marginRight: 10}}
          />
          <View
            style={{
              backgroundColor: lightGreen,
              padding: 10,
              borderRadius: 10,
              flex: 1,
            }}>
            <Text style={{color: dark, fontSize: 12, fontWeight: '500'}}>
              User Name
            </Text>
            <Text style={{color: '#949494', fontSize: 12}}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginBottom: 10,
          }}>
          <Image
            source={require('../../assets/userPic.png')}
            style={{width: 45, height: 45, borderRadius: 45, marginRight: 10}}
          />
          <View
            style={{
              backgroundColor: lightGreen,
              padding: 10,
              borderRadius: 10,
              flex: 1,
            }}>
            <Text style={{color: dark, fontSize: 12, fontWeight: '500'}}>
              User Name
            </Text>
            <Text style={{color: '#949494', fontSize: 12}}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginBottom: 10,
          }}>
          <Image
            source={require('../../assets/userPic.png')}
            style={{width: 45, height: 45, borderRadius: 45, marginRight: 10}}
          />
          <View
            style={{
              backgroundColor: lightGreen,
              padding: 10,
              borderRadius: 10,
              flex: 1,
            }}>
            <Text style={{color: dark, fontSize: 12, fontWeight: '500'}}>
              User Name
            </Text>
            <Text style={{color: '#949494', fontSize: 12}}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginBottom: 10,
          }}>
          <Image
            source={require('../../assets/userPic.png')}
            style={{width: 45, height: 45, borderRadius: 45, marginRight: 10}}
          />
          <View
            style={{
              backgroundColor: lightGreen,
              padding: 10,
              borderRadius: 10,
              flex: 1,
            }}>
            <Text style={{color: dark, fontSize: 12, fontWeight: '500'}}>
              User Name
            </Text>
            <Text style={{color: '#949494', fontSize: 12}}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's
            </Text>
          </View>
        </View>
      </ScrollView>
    </Animated.View>
  );
}
const styles = StyleSheet.create({
  badge: {
    backgroundColor: '#0DCAA0',
    paddingHorizontal: 10,
    paddingVertical: 6,
    borderRadius: 10,
    marginRight: 5,
    color: white,
    fontSize: 10,
    fontWeight: '500',
  },
});
