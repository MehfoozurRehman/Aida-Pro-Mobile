import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import {detailsActive} from '../../assets/icons';
import Svg, {G, Path} from 'react-native-svg';
import ButtonPrimary from '../../components/ButtonPrimary';
import HeaderPrimary from '../../components/HeaderPrimary';
import {dark, lightGreen, white} from '../../constants/colors';

function ProfilePreviewRow({
  svgLeft,
  titleLeft,
  headingLeft,
  titleRight,
  headingRight,
}) {
  return (
    <View
      style={{
        width: '100%',
        borderRadius: 10,
        display: 'flex',
        flexDirection: 'row',
        marginVertical: 20,
      }}>
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          width: '60%',
        }}>
        <View style={{marginRight: 10}}>{svgLeft}</View>
        <View>
          <Text style={{fontSize: 13, color: dark}}>{titleLeft}</Text>
          <Text style={{fontSize: 12, color: dark, fontWeight: 'bold'}}>
            {headingLeft}
          </Text>
        </View>
      </View>
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          width: '40%',
        }}>
        <View style={{marginRight: 10}}>{svgLeft}</View>
        <View>
          <Text style={{fontSize: 13, color: dark}}>{titleRight}</Text>
          <Text style={{fontSize: 12, color: dark, fontWeight: 'bold'}}>
            {headingRight}
          </Text>
        </View>
      </View>
    </View>
  );
}

export default function CompanyDetailsPreviewScreen({
  setProgress,
  navigation,
  drawerAnimationStyle,
}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary
        title="Company Details"
        backPath="Profile"
        icon={detailsActive}
      />
      <ScrollView
        style={{
          flex: 1,
          width: '100%',
          padding: 20,
          marginVertical: 10,
        }}>
        <View
          style={{
            padding: 10,
            backgroundColor: lightGreen,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            borderRadius: 10,
          }}>
          <ProfilePreviewRow
            svgLeft={
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={11.907}
                height={15.9}>
                <G data-name="Group 1478" fill={dark}>
                  <Path
                    data-name="Path 285"
                    d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
                  />
                  <Path
                    data-name="Path 286"
                    d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
                  />
                  <Path
                    data-name="Path 287"
                    d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
                  />
                  <Path
                    data-name="Path 288"
                    d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
                  />
                </G>
              </Svg>
            }
            titleLeft="Company Name"
            headingLeft="John Doe"
            svgRight={
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={17.064}
                height={17.078}>
                <Path
                  data-name="Path 21772"
                  d="M13.375 0v1h1.981l-2.572 2.572a4.51 4.51 0 00-5.543-.077 4.519 4.519 0 10-3.222 8.1V14.4H2.67v1h1.349v1.675h1V15.4H6.37v-1H5.02v-2.807a4.5 4.5 0 002.222-.886 4.517 4.517 0 006.252-6.427l2.57-2.572V3.69h1V0zM7.242 9.329a3.512 3.512 0 010-4.456 3.512 3.512 0 010 4.456zM1.001 7.1a3.517 3.517 0 015.5-2.9 4.51 4.51 0 000 5.81 3.517 3.517 0 01-5.5-2.9zm8.963 3.519a3.5 3.5 0 01-1.984-.614 4.51 4.51 0 000-5.81 3.519 3.519 0 111.984 6.424z"
                  fill={dark}
                />
              </Svg>
            }
            titleRight="Contact Name"
            headingRight="Mehfooz"
          />
          <ProfilePreviewRow
            svgLeft={
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={17.064}
                height={17.078}>
                <Path
                  data-name="Path 21772"
                  d="M13.375 0v1h1.981l-2.572 2.572a4.51 4.51 0 00-5.543-.077 4.519 4.519 0 10-3.222 8.1V14.4H2.67v1h1.349v1.675h1V15.4H6.37v-1H5.02v-2.807a4.5 4.5 0 002.222-.886 4.517 4.517 0 006.252-6.427l2.57-2.572V3.69h1V0zM7.242 9.329a3.512 3.512 0 010-4.456 3.512 3.512 0 010 4.456zM1.001 7.1a3.517 3.517 0 015.5-2.9 4.51 4.51 0 000 5.81 3.517 3.517 0 01-5.5-2.9zm8.963 3.519a3.5 3.5 0 01-1.984-.614 4.51 4.51 0 000-5.81 3.519 3.519 0 111.984 6.424z"
                  fill={dark}
                />
              </Svg>
            }
            titleLeft="Age"
            headingLeft="Not Spacified"
            svgRight={
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={11.907}
                height={15.9}>
                <G data-name="Group 1478" fill={dark}>
                  <Path
                    data-name="Path 285"
                    d="M5.95 8.439a1.811 1.811 0 10-1.811-1.811A1.811 1.811 0 005.95 8.439zm0-2.917a1.11 1.11 0 11-1.11 1.11 1.11 1.11 0 011.11-1.114zm0 0"
                  />
                  <Path
                    data-name="Path 286"
                    d="M5.954 9.108a2.717 2.717 0 00-1.968.823 2.9 2.9 0 00-.812 2.056.351.351 0 00.35.35h4.861a.351.351 0 00.35-.35 2.9 2.9 0 00-.812-2.056 2.717 2.717 0 00-1.969-.823zm-2.056 2.529a2.142 2.142 0 01.588-1.215 2.063 2.063 0 012.935 0 2.153 2.153 0 01.588 1.215zm0 0"
                  />
                  <Path
                    data-name="Path 287"
                    d="M10.156 0H1.751A1.752 1.752 0 000 1.751v12.4A1.752 1.752 0 001.751 15.9h8.405a1.752 1.752 0 001.751-1.751V1.751A1.752 1.752 0 0010.156 0zm1.051 14.149a1.054 1.054 0 01-1.051 1.051H1.751A1.054 1.054 0 01.7 14.149V1.751A1.054 1.054 0 011.751.7h8.405a1.054 1.054 0 011.051 1.051zm0 0"
                  />
                  <Path
                    data-name="Path 288"
                    d="M4.493 2.349h2.8a.35.35 0 100-.7h-2.8a.35.35 0 100 .7zm0 0"
                  />
                </G>
              </Svg>
            }
            titleRight="Gender"
            headingRight="Male"
          />
        </View>
        <View
          style={{
            padding: 10,
            backgroundColor: white,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            borderRadius: 10,
            marginVertical: 20,
            borderWidth: 1,
          }}>
          <ProfilePreviewRow
            svgLeft={
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={15.203}
                height={10.135}>
                <G data-name="Group 1744">
                  <Path
                    data-name="Path 292"
                    d="M14.423 0H.78A.782.782 0 000 .78v8.576a.782.782 0 00.78.78h13.643a.782.782 0 00.78-.78V.78a.782.782 0 00-.78-.78zm-.293.585L8.05 5.146a.815.815 0 01-.9 0L1.072.585zm-3.248 4.862L14.2 9.54l.011.011H1l.011-.011L4.32 5.447a.293.293 0 00-.455-.368L.585 9.132V.95L6.8 5.613a1.4 1.4 0 001.6 0L14.618.95v8.181l-3.281-4.053a.293.293 0 00-.455.368z"
                    fill={dark}
                  />
                </G>
              </Svg>
            }
            titleLeft="Email"
            headingLeft="John@gmail.com"
            svgRight={
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={15.428}
                height={15.45}>
                <G data-name="Group 201" fill={dark}>
                  <Path
                    data-name="Path 289"
                    d="M12.2 9.571a1.52 1.52 0 00-1.1-.505 1.571 1.571 0 00-1.115.5l-1.029 1.029c-.085-.046-.17-.088-.251-.13-.117-.059-.228-.114-.323-.173a11.2 11.2 0 01-2.684-2.446 6.607 6.607 0 01-.88-1.389c.267-.245.515-.5.757-.743l.274-.277a1.5 1.5 0 000-2.257l-.893-.892c-.1-.1-.205-.205-.3-.31-.2-.2-.4-.411-.613-.607A1.544 1.544 0 002.951.892a1.6 1.6 0 00-1.109.479l-.007.007-1.113 1.12a2.386 2.386 0 00-.708 1.516 5.716 5.716 0 00.417 2.42 14.041 14.041 0 002.495 4.161 15.346 15.346 0 005.11 4 7.961 7.961 0 002.87.848c.068 0 .14.007.205.007a2.457 2.457 0 001.882-.809c0-.007.01-.01.013-.016a7.413 7.413 0 01.571-.59 12.9 12.9 0 00.424-.421 1.627 1.627 0 00.491-1.126 1.567 1.567 0 00-.5-1.118zm1.167 3.434c-.127.137-.258.261-.4.4a8.574 8.574 0 00-.629.652 1.572 1.572 0 01-1.226.518h-.15a7.073 7.073 0 01-2.544-.763 14.485 14.485 0 01-4.813-3.77 13.239 13.239 0 01-2.349-3.914 4.656 4.656 0 01-.362-2.04 1.5 1.5 0 01.45-.968l1.112-1.114a.741.741 0 01.5-.232.7.7 0 01.476.228l.01.01c.2.186.388.378.587.584.1.1.205.209.31.316l.89.89a.622.622 0 010 1.011c-.095.095-.186.189-.28.28-.274.28-.535.541-.818.8-.007.007-.013.01-.016.016a.665.665 0 00-.17.74l.01.029a7.147 7.147 0 001.053 1.719 11.969 11.969 0 002.9 2.635 4.451 4.451 0 00.4.218c.117.059.228.114.323.173l.039.023a.707.707 0 00.323.082.7.7 0 00.5-.225l1.106-1.115a.738.738 0 01.492-.245.664.664 0 01.47.238l.007.007 1.8 1.8a.645.645 0 01-.001 1.017z"
                  />
                  <Path
                    data-name="Path 290"
                    d="M8.338 3.677a4.2 4.2 0 013.417 3.417.438.438 0 00.434.365.582.582 0 00.075-.007.441.441 0 00.362-.509 5.075 5.075 0 00-4.135-4.135.443.443 0 00-.509.359.435.435 0 00.356.51z"
                  />
                  <Path
                    data-name="Path 291"
                    d="M15.426 6.816A8.356 8.356 0 008.613.007a.44.44 0 10-.143.867 7.464 7.464 0 016.085 6.085.438.438 0 00.434.365.582.582 0 00.075-.007.432.432 0 00.362-.501z"
                  />
                </G>
              </Svg>
            }
            titleRight="Phone"
            headingRight="+9234202312"
          />
        </View>
        <View
          style={{
            padding: 10,
            backgroundColor: lightGreen,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            borderRadius: 10,
          }}>
          <ProfilePreviewRow
            svgLeft={
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={9.581}
                height={13.36}>
                <Path
                  data-name="Path 280"
                  d="M8.449 2.616A4.194 4.194 0 004.885.5h-.19a4.2 4.2 0 00-3.564 2.116 4.3 4.3 0 00-.058 4.252l3.065 5.61v.007a.745.745 0 001.292 0v-.007l3.065-5.61a4.3 4.3 0 00-.056-4.252zM4.79 6.1a1.738 1.738 0 111.738-1.738A1.74 1.74 0 014.79 6.1z"
                  fill="none"
                  stroke={dark}
                />
              </Svg>
            }
            titleLeft="Address"
            headingLeft="Lahore, Johar Town"
            svgRight={
              <Svg
                data-name="Group 204"
                xmlns="http://www.w3.org/2000/svg"
                width={18.43}
                height={18.373}>
                <G data-name="Group 203">
                  <Path
                    data-name="Path 304"
                    d="M18.157 13.868h-1.8v-2.237a.274.274 0 10-.547 0v6.194h-4.7V9.694h4.7v.66a.274.274 0 10.547 0V9.42a.274.274 0 00-.274-.274H12.34V.274a.274.274 0 00-.274-.27H6.373a.274.274 0 00-.273.27v4.094H2.558a.274.274 0 00-.274.274v6.73H.274a.274.274 0 00-.274.274v6.458a.274.274 0 00.274.274h17.883a.274.274 0 00.274-.274v-3.962a.274.274 0 00-.274-.274zM2.285 17.826H.547v-5.907h1.738zm5.693 0H2.832v-1.514h5.146zm0-3.9H5.511a.274.274 0 000 .547h2.467v1.3H2.832v-1.3h1.4a.274.274 0 000-.547h-1.4v-1.3h5.146zm0-1.843H2.832v-1.3h5.146zm0-1.843H2.832v-1.3h5.146zm0-1.843H2.832v-1.3h5.146zm0-1.843H2.832V4.916h5.146zm2.583 2.87v8.406H8.525V4.642a.274.274 0 00-.274-.274h-.373v-.547a.274.274 0 10-.547 0v.547h-.684V.547h5.146v8.6h-.958a.274.274 0 00-.274.273zm7.322 8.405h-1.53v-3.41h1.53z"
                    fill={dark}
                  />
                </G>
              </Svg>
            }
            titleRight="City"
            headingRight="Not Spacified"
          />
          <ProfilePreviewRow
            svgLeft={
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={9.581}
                height={13.36}>
                <Path
                  data-name="Path 280"
                  d="M8.449 2.616A4.194 4.194 0 004.885.5h-.19a4.2 4.2 0 00-3.564 2.116 4.3 4.3 0 00-.058 4.252l3.065 5.61v.007a.745.745 0 001.292 0v-.007l3.065-5.61a4.3 4.3 0 00-.056-4.252zM4.79 6.1a1.738 1.738 0 111.738-1.738A1.74 1.74 0 014.79 6.1z"
                  fill="none"
                  stroke={dark}
                />
              </Svg>
            }
            titleLeft="Zip Code"
            headingLeft="37300"
            svgRight={
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={14.906}
                height={17.39}>
                <Path
                  data-name="Icon metro-language"
                  d="M6.346 10.462q-.01.029-.121 0t-.306-.112l-.194-.087a5.814 5.814 0 01-.844-.476q-.068-.049-.4-.306t-.367-.284a16.532 16.532 0 01-1.3 1.756 7.214 7.214 0 01-1.019 1.067.769.769 0 01-.189.039.374.374 0 01-.18 0q.058-.039.8-.893.2-.233.83-1.116a13.567 13.567 0 00.762-1.145q.165-.291.495-.956a7.564 7.564 0 00.345-.752 7.974 7.974 0 00-1.067.32q-.078.019-.267.073l-.335.092a1.662 1.662 0 00-.165.049.165.165 0 00-.019.1.215.215 0 01-.01.092q-.049.1-.3.146a.782.782 0 01-.456 0 .409.409 0 01-.272-.2.494.494 0 01-.049-.223 1.875 1.875 0 01.237-.049 2.9 2.9 0 00.286-.058q.563-.155 1.019-.311.97-.34.99-.34a2.153 2.153 0 00.418-.191 4.39 4.39 0 01.427-.2q.087-.029.209-.078t.141-.053a.13.13 0 01.058 0 1.1 1.1 0 01-.01.32 2.066 2.066 0 01-.121.262l-.257.519q-.136.276-.166.33a14.555 14.555 0 01-.747 1.271l.621.272q.116.058.723.311l.655.272q.039.01.1.247a.735.735 0 01.044.3zM4.357 5.745a.394.394 0 01-.039.272.88.88 0 01-.485.369 1.558 1.558 0 01-.582.116.8.8 0 01-.476-.252.73.73 0 01-.175-.4l.01-.029a.415.415 0 00.189.049.844.844 0 00.257 0q.1-.019.563-.155a2.771 2.771 0 01.534-.136.192.192 0 01.2.165zm6.773 1.252l.611 2.2-1.348-.4zM.378 14.761l6.735-2.251V2.497L.378 4.756v10.005zm12.043-3.076l.99.3-1.756-6.376-.97-.3-2.1 5.2.99.3.437-1.067 2.048.631zM7.54 2.349l5.561 1.786V.447zm3.018 12.839l1.533.126-.524 1.553-.388-.64a7.194 7.194 0 01-2.678 1.048 4.651 4.651 0 01-.883.116h-.815a6.559 6.559 0 01-1.936-.378 6.444 6.444 0 01-1.781-.825.2.2 0 01-.078-.155.188.188 0 01.049-.131.163.163 0 01.126-.053.546.546 0 01.175.073l.3.16.2.107a8.6 8.6 0 001.548.6 5.655 5.655 0 001.528.238 8.281 8.281 0 001.621-.141 8 8 0 001.524-.49q.146-.068.3-.15t.33-.184q.179-.1.277-.16zm4.348-10.471v10.471l-7.512-2.391q-.136.058-3.639 1.237T.184 15.217a.176.176 0 01-.175-.126.075.075 0 00-.01-.029V4.597a.433.433 0 01.039-.1.437.437 0 01.192-.1q1.029-.34 1.446-.485V.185l5.415 1.921q.019 0 1.558-.534T11.716.519q1.53-.522 1.569-.522.194 0 .194.2v4.064z"
                  fill={dark}
                />
              </Svg>
            }
            titleRight="Country"
            headingRight="Pakistan"
          />
        </View>
        <View style={{marginVertical: 20}}>
          <ButtonPrimary
            onPress={() => {
              navigation.navigate('Company Details Edit');
            }}
            label="Edit"
          />
        </View>
      </ScrollView>
    </Animated.View>
  );
}
