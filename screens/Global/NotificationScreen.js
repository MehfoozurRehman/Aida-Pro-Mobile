import React from 'react';
import {FlatList, ScrollView} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import {notificationActive} from '../../assets/icons';
import NotificationCard from '../../components/NotificationCard';
import HeaderPrimary from '../../components/HeaderPrimary';
import {white} from '../../constants/colors';

export default function NotificationScreen({
  setProgress,
  drawerAnimationStyle,
}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  const DATA = [
    {
      id: '1',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '2',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '3',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '4',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '5',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '6',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '7',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '8',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '9',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
  ];
  const renderItem = ({item}) => (
    <NotificationCard
      title={item.title}
      content={item.content}
      onPress={() => {}}
    />
  );
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary
        title="Notifications"
        backPath="Home"
        icon={notificationActive}
        imgStyle={{resizeMode: 'contain'}}
      />
      <FlatList
        style={{
          flex: 1,
          width: '100%',
          padding: 20,
          marginBottom: 20,
        }}
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </Animated.View>
  );
}
