import React from 'react';
import {ScrollView, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import {detailsActive} from '../../assets/icons';
import {Input} from '../../components';
import Svg, {Rect, Path} from 'react-native-svg';
import ButtonPrimary from '../../components/ButtonPrimary';
import HeaderPrimary from '../../components/HeaderPrimary';
import {white} from '../../constants/colors';

export default function ChangePassword({
  setProgress,
  drawerAnimationStyle,
  navigation,
}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary
        title="Change Password"
        backPath="Profile"
        icon={detailsActive}
      />
      <ScrollView
        style={{
          flex: 1,
          width: '100%',
          padding: 20,
          marginVertical: 10,
        }}>
        <Input
          label="Old Password"
          placeholder="Enter Old Password"
          secureTextEntry={true}
          svg={
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={24}
              height={24}
              fill="none"
              stroke={dark}
              strokeWidth={1}
              strokeLinecap="round"
              strokeLinejoin="round"
              className="feather feather-lock">
              <Rect x={3} y={11} width={18} height={11} rx={2} ry={2} />
              <Path d="M7 11V7a5 5 0 0 1 10 0v4" />
            </Svg>
          }
        />
        <Input
          label="New Password"
          placeholder="Enter New Password"
          secureTextEntry={true}
          svg={
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={24}
              height={24}
              fill="none"
              stroke={dark}
              strokeWidth={1}
              strokeLinecap="round"
              strokeLinejoin="round"
              className="feather feather-lock">
              <Rect x={3} y={11} width={18} height={11} rx={2} ry={2} />
              <Path d="M7 11V7a5 5 0 0 1 10 0v4" />
            </Svg>
          }
        />
        <Input
          label="Confirm Password"
          placeholder="Enter Confirm Password"
          secureTextEntry={true}
          svg={
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={24}
              height={24}
              fill="none"
              stroke={dark}
              strokeWidth={1}
              strokeLinecap="round"
              strokeLinejoin="round"
              className="feather feather-lock">
              <Rect x={3} y={11} width={18} height={11} rx={2} ry={2} />
              <Path d="M7 11V7a5 5 0 0 1 10 0v4" />
            </Svg>
          }
        />
        <View style={{marginVertical: 20}}>
          <ButtonPrimary
            onPress={() => {
              navigation.navigate('Profile');
            }}
            label="Change Password"
          />
        </View>
      </ScrollView>
    </Animated.View>
  );
}
