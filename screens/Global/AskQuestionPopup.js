import React from 'react';
import {Input, Select, Textarea, Popup} from '../../components';

const AskQuestionPopup = ({modalVisible, setModalVisible}) => {
  return (
    <Popup
      small={true}
      title="Ask Question"
      buttonLabel="Post Your Question"
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      onButtonPress={() => {
        setModalVisible(!modalVisible);
      }}>
      <Input
        placeholder="Question here"
        label="Question"
        error={false}
        errorMessage="error"
      />
      <Textarea placeholder="Description" error={false} errorMessage="error" />
      <Select
        placeholder="Tags here"
        label="Tags"
        error={false}
        errorMessage="error"
      />
    </Popup>
  );
};

export default AskQuestionPopup;
