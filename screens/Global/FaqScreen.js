import React from 'react';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import {faqActive} from '../../assets/icons';
import Svg, {Path} from 'react-native-svg';
import HeaderPrimary from '../../components/HeaderPrimary';
import {role} from '../../constants/constants';
import {useNavigation} from '@react-navigation/native';
import {Text, View, TouchableOpacity, FlatList} from 'react-native';
import {dark, lightGreen, white} from '../../constants/colors';

function FAQCard() {
  const navigation = useNavigation();
  const DATA = [
    {
      id: '1',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '2',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '3',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '4',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '5',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '6',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '7',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '8',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '9',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
  ];
  const renderItem = ({item}) => (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate('FAQ Details');
      }}
      style={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginVertical: 6,
        width: '96%',
        padding: 2,
      }}>
      <Svg
        xmlns="http://www.w3.org/2000/svg"
        width={18}
        height={18}
        viewBox="0 0 24 24"
        fill="#0CB49E"
        stroke={lightGreen}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
        className="feather feather-file">
        <Path d="M13 2H6a2 2 0 00-2 2v16a2 2 0 002 2h12a2 2 0 002-2V9z" />
        <Path d="M13 2L13 9 20 9" />
      </Svg>
      <Text
        style={{
          fontSize: 12,
          color: '#5B5B5B',
          textDecorationLine: 'underline',
        }}>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Accusantium,
        aperiam dicta! Libero.?
      </Text>
    </TouchableOpacity>
  );
  return (
    <View
      style={{
        width: '100%',
        backgroundColor: lightGreen,
        borderRadius: 10,
        padding: 12,
        marginVertical: 10,
        display: 'flex',
        flexDirection: 'column',
      }}>
      <Text style={{fontSize: 14, fontWeight: 'bold', color: dark}}>
        How can I clear Aida's vetting process?
      </Text>
      <FlatList
        style={{
          width: '100%',
          flex: 1,
        }}
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        ListFooterComponent={<View style={{height: 20}} />}
      />
    </View>
  );
}

export default function FaqScreen({setProgress, drawerAnimationStyle}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  const DATA = [
    {
      id: '1',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '2',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '3',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '4',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '5',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '6',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '7',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '8',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '9',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
  ];
  const renderItem = ({item}) => (
    <>
      {role === 'Company' ? (
        <FAQCard />
      ) : role === 'Freelancer' ? (
        <FAQCard />
      ) : role === 'Professional' ? (
        <FAQCard />
      ) : null}
    </>
  );
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary title="FAQ" backPath="Profile" icon={faqActive} />
      <FlatList
        style={{
          flex: 1,
          width: '100%',
          padding: 15,
        }}
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        ListFooterComponent={<View style={{height: 20}} />}
      />
    </Animated.View>
  );
}
