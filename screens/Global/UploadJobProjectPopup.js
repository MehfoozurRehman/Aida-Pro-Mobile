import React from 'react';
import {Input, Select, Textarea, Popup} from '../../components';
import UploadImage from '../../components/UploadImage';

const UploadJobProjectPopup = ({modalVisible, setModalVisible, title}) => {
  return (
    <Popup
      small={true}
      title={title}
      buttonLabel="Upload"
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      onButtonPress={() => {
        setModalVisible(!modalVisible);
      }}>
      <Input
        label="Project Name"
        placeholder="Enter Project Name"
        error={false}
        errorMessage="error"
      />
      <Textarea placeholder="Description" error={false} errorMessage="error" />
      <Select
        placeholder="Tags here"
        label="Tags"
        error={false}
        errorMessage="error"
      />
      <UploadImage />
    </Popup>
  );
};

export default UploadJobProjectPopup;
