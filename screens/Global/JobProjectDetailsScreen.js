import React, {useState} from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import {home, homeActive} from '../../assets/icons';
import ButtonSecondary from '../../components/ButtonSecondary';
import ButtonPrimary from '../../components/ButtonPrimary';
import HeaderPrimary from '../../components/HeaderPrimary';
import UploadJobProjectPopup from './UploadJobProjectPopup';
import HeaderSimple from '../../components/HeaderSimple';
import {dark, lightGreen, white} from '../../constants/colors';

function DetailsTag({text}) {
  return (
    <TouchableOpacity
      style={{
        padding: 6,
        backgroundColor: '#3DE3B5',
        alignSelf: 'flex-start',
        borderRadius: 6,
        marginRight: 8,
      }}>
      <Text style={{fontSize: 12, color: white}}>{text}</Text>
    </TouchableOpacity>
  );
}

export default function JobProjectDetailsScreen({
  setProgress,
  drawerAnimationStyle,
  isJob,
}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  const [isModalOpen, setIsModalOpen] = useState(false);
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderSimple
        title={isJob ? 'Job Details' : 'Project Details'}
        icon={homeActive}
        backPath="My Projects"
      />
      <ScrollView
        style={{
          flex: 1,
          width: '100%',
          paddingHorizontal: 20,
        }}>
        <Text style={{fontSize: 18, fontWeight: 'bold', color: dark}}>
          Machine Learning Project
        </Text>
        <Text style={{fontSize: 14, color: dark}}>Computer Scientist</Text>
        <ScrollView
          horizontal={true}
          style={{width: '100%', flex: 1, marginVertical: 10}}>
          <DetailsTag text="Machine Learning" />
          <DetailsTag text="Python" />
          <DetailsTag text="React JS" />
          <DetailsTag text="Machine Learning" />
        </ScrollView>
        <View
          style={{
            padding: 20,
            backgroundColor: lightGreen,
            borderRadius: 6,
            marginTop: 5,
          }}>
          <Text style={{fontSize: 14, color: dark}}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived not
            only five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged. It was popularised in the 1960s
            with the release of Letraset sheets containing Lorem Ipsum passages,
            and more recently with desktop publishing software like Aldus
            PageMaker including versions of Lorem Ipsum. with the release of
            Letraset sheets containing Lorem Ipsum passages, and more recently
            with desktop publishing software like Aldus PageMaker including
            versions of Lorem Ipsum.
          </Text>
        </View>
      </ScrollView>
      <View
        style={{
          width: '100%',
          paddingHorizontal: 20,
          marginVertical: 10,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <ButtonPrimary
          label={isJob ? 'Edit Job' : 'Edit Project'}
          onPress={() => {
            setIsModalOpen(true);
          }}
          style={{width: '49%'}}
        />
        <ButtonSecondary
          label={isJob ? 'Delete Job' : 'Delete Project'}
          onPress={() => {}}
          style={{width: '49%'}}
        />
      </View>
      <UploadJobProjectPopup
        modalVisible={isModalOpen}
        setModalVisible={setIsModalOpen}
        title={isJob ? 'Edit Job' : 'Edit Project'}
      />
    </Animated.View>
  );
}
