import React from 'react';
import {Input, Popup} from '../../components';
import DatePickerInput from '../../components/DatePickerInput';
import Select from '../../components/Select';
import Textarea from '../../components/Textarea';

const ExperiencePopup = ({modalVisible, setModalVisible, title}) => {
  return (
    <Popup
      title={title}
      buttonLabel="Save"
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      onButtonPress={() => {
        setModalVisible(!modalVisible);
      }}>
      <Input placeholder="Ttile" />
      <Input placeholder="Company Name" />
      <Select placeholder="Industry Type" />
      <Select placeholder="Employment Type" />
      <Textarea placeholder="Description" />
      <DatePickerInput label="Start" />
      <DatePickerInput label="End" />
    </Popup>
  );
};

export default ExperiencePopup;
