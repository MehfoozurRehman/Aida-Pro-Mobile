import React from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import {userActive} from '../../assets/icons';
import HeaderPrimary from '../../components/HeaderPrimary';
import {dark, white} from '../../constants/colors';

const aboutInfoList = [
  {
    heading: 'Our Mission',
    info: 'Through AIDApro we can create a global community connecting companies and professionals in the fields of AI and Data. AIDApro’s sincere commitment to explore and undertake AI and Data initiatives in developing countries. Helping local professionals with better careers and local poor communities using AI and Data solutions that can improve their basic life necessities.',
    image: require('../../assets/bestIdea.d9a31e76.png'),
  },
  {
    heading: 'Our Vision',
    info: 'We strongly believe AI & Data developments can contribute to a better and more sustainable world.',
    image: require('../../assets/ourVision.5f2ae427.png'),
  },
  {
    heading: 'Our Team',
    info: 'We are an experienced and talented team having worked for international corporations in IT, engineering, marketing and recruitment. All founders share a common passion for AI & Data. This experience and expertise is bundled in AIDApro, creating a dedicated community where companies and professionals communicate directly, share experiences, knowledge, and ask questions.',
    image: require('../../assets/ourTeam.daefc3d0.png'),
  },
];

export default function AboutUsScreen({setProgress, drawerAnimationStyle}) {
  const progress = useDrawerProgress();
  setProgress(progress);

  return (
    <Animated.View
      style={{
        flex: 1,
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary title="About Us" backPath="Profile" icon={userActive} />
      <ScrollView style={{flex: 1, paddingVertical: 20, paddingHorizontal: 15}}>
        <View style={{width: '100%', marginBottom: 20}}>
          <Text style={styles.aboutHeading}>About Us</Text>
          <Text style={{fontSize: 22, color: dark}}>Who we are?</Text>
          <View style={styles.aboutBottomBorder} />
          <Text style={styles.aboutinfo}>
            AIDApro is founded by a group of managers and data freaks who are
            all truly passionate about AI and data. With different professional
            backgrounds and from all over the world, Our platform helps the AI
            and data community to further flourish.
          </Text>
          <Image
            style={styles.aboutImage}
            source={require('../../assets/aboutUsSvg.e5bedfa6.png')}
          />
        </View>
        <View style={{width: '100%', marginBottom: 20}}>
          <Text style={styles.aboutHeading}>Our goals</Text>
          <View style={styles.aboutBottomBorder} />
          <Text style={styles.aboutinfo}>
            • Assist our platform professionals in their careers
          </Text>
          <Text style={styles.aboutinfo}>
            • Develop AI and data initiatives in third world countries in Africa
            and Asia
          </Text>
          <Text style={styles.aboutinfo}>
            • Match professionals with the right companies, and vice versa
          </Text>
          <Text style={styles.aboutinfo}>
            • Knowledge platform for technical and other issues
          </Text>
          <Image
            style={styles.aboutImage}
            source={require('../../assets/ourStorySvg.f747d579.png')}
          />
        </View>
        {aboutInfoList.map(item => {
          return (
            <View style={{width: '100%', marginBottom: 20}}>
              <Text style={styles.aboutHeading}>{item.heading}</Text>
              <View style={styles.aboutBottomBorder} />
              <Text style={styles.aboutinfo}>{item.info}</Text>
              <Image style={styles.aboutImage} source={item.image} />
            </View>
          );
        })}
      </ScrollView>
    </Animated.View>
  );
}
const styles = StyleSheet.create({
  aboutHeading: {
    fontSize: 22,
    fontWeight: 'bold',
    color: dark,
  },
  aboutBottomBorder: {
    width: 60,
    height: 2,
    backgroundColor: '#0DC5A1',
    marginVertical: 2,
    marginBottom: 10,
  },
  aboutinfo: {
    fontSize: 12,
    color: '#818181',
  },
  aboutImage: {
    marginVertical: 20,
    width: '100%',
    height: 250,
    resizeMode: 'contain',
  },
});
