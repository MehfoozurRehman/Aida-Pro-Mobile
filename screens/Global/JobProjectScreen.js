import React, {useState} from 'react';
import {FlatList, ScrollView, TouchableOpacity, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import {homeActive} from '../../assets/icons';
import {ProjectCard} from '../../components';
import HeaderPrimary from '../../components/HeaderPrimary';
import LinearGradient from 'react-native-linear-gradient';
import Svg, {Path} from 'react-native-svg';
import UploadJobProjectPopup from './UploadJobProjectPopup';
import {white} from '../../constants/colors';

export default function JobProjectScreen({
  setProgress,
  drawerAnimationStyle,
  isJob,
  navigation,
}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const DATA = [
    {
      id: '1',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '2',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '3',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '4',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '5',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '6',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '7',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '8',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '9',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
  ];
  const renderItem = ({item}) => (
    <ProjectCard
      onPress={() => {
        navigation.navigate('Project Details');
      }}
    />
  );
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary
        title={isJob ? 'My Jobs' : 'My Projects'}
        backPath="Home"
        icon={homeActive}
      />
      <FlatList
        style={{
          flex: 1,
          width: '100%',
          paddingHorizontal: 10,
          marginBottom: 20,
        }}
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
      <UploadJobProjectPopup
        modalVisible={isModalOpen}
        setModalVisible={setIsModalOpen}
        title={isJob ? 'Upload Job' : 'Upload Project'}
      />
      <TouchableOpacity
        style={{
          width: 60,
          height: 60,
          position: 'absolute',
          bottom: 30,
          right: 20,
          zIndex: 500,
        }}
        onPress={() => {
          setIsModalOpen(true);
        }}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#F6B038', '#F5833C']}
          style={{
            width: '100%',
            height: '100%',
            borderRadius: 50,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={24}
            height={24}
            fill="none"
            stroke={white}
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
            className="feather feather-plus">
            <Path d="M12 5v14M5 12h14" />
          </Svg>
        </LinearGradient>
      </TouchableOpacity>
    </Animated.View>
  );
}
