import React from 'react';
import {FlatList, Image, ScrollView, Text, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import CustomBottomTabBar from '../../navigation/CustomBottomTabBar';
import {chatActive} from '../../assets/icons';
import Svg, {G, Path} from 'react-native-svg';
import ChatUser from '../../components/ChatUser';
import HeaderPrimary from '../../components/HeaderPrimary';
import SearchInput from '../../components/SearchInput';
import {dark, white} from '../../constants/colors';

export default function MessengerScreen({
  setProgress,
  drawerAnimationStyle,
  animatedBar,
  navigation,
  isSelected,
  setIsSelected,
}) {
  const progress = useDrawerProgress();
  const contacts = true;
  setProgress(progress);
  const DATA = [
    {
      id: '1',
      image: require('../../assets/userPic.png'),
      name: 'John Doe',
      message: 'You massive peice of beauty',
      time: '2h',
      badgeValue: 3,
    },
    {
      id: '2',
      image: require('../../assets/userPic.png'),
      name: 'John Doe',
      message: 'You massive peice of beauty',
      time: '2h',
      badgeValue: 10,
    },
    {
      id: '3',
      image: require('../../assets/userPic.png'),
      name: 'John Doe',
      message: 'You massive peice of beauty',
      time: '2h',
      badgeValue: 0,
    },
    {
      id: '4',
      image: require('../../assets/userPic.png'),
      name: 'John Doe',
      message: 'You massive peice of beauty',
      time: '2h',
      badgeValue: 0,
    },
    {
      id: '5',
      image: require('../../assets/userPic.png'),
      name: 'John Doe',
      message: 'You massive peice of beauty',
      time: '2h',
      badgeValue: 0,
    },
    {
      id: '6',
      image: require('../../assets/userPic.png'),
      name: 'John Doe',
      message: 'You massive peice of beauty',
      time: '2h',
      badgeValue: 0,
    },
    {
      id: '7',
      image: require('../../assets/userPic.png'),
      name: 'John Doe',
      message: 'You massive peice of beauty',
      time: '2h',
      badgeValue: 0,
    },
    {
      id: '8',
      image: require('../../assets/userPic.png'),
      name: 'John Doe',
      message: 'You massive peice of beauty',
      time: '2h',
      badgeValue: 0,
    },
    {
      id: '9',
      image: require('../../assets/userPic.png'),
      name: 'John Doe',
      message: 'You massive peice of beauty',
      time: '2h',
      badgeValue: 0,
    },
  ];
  const renderItem = ({item}) => (
    <ChatUser
      image={item.image}
      name={item.name}
      message={item.message}
      time={item.time}
      badgeValue={item.badgeValue}
      navigation={navigation}
    />
  );
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary
        title="Messenger"
        backPath="Home"
        icon={chatActive}
        noBackButton
      />
      <View style={{marginHorizontal: 15, marginTop: 10}}>
        <SearchInput
          placeholder="Search"
          svg={
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={20.681}
              height={20.681}>
              <G
                data-name="Icon feather-search"
                fill="none"
                stroke="#545454"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={1}>
                <Path
                  data-name="Path 2299"
                  d="M17.553 9.526A8.026 8.026 0 119.526 1.5a8.026 8.026 0 018.027 8.026z"
                />
                <Path data-name="Path 2300" d="M19.559 19.559l-4.364-4.364" />
              </G>
            </Svg>
          }
        />
      </View>
      {contacts ? (
        <FlatList
          style={{
            flex: 1,
            width: '100%',
            paddingHorizontal: 15,
            paddingTop: 10,
            marginBottom: 70,
          }}
          data={DATA}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
      ) : (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: 70,
          }}>
          <Image
            source={require('../../assets/noContacts.png')}
            style={{width: 200, height: 200}}
          />
          <Text style={{color: dark}}>No contacts here yet</Text>
        </View>
      )}
      <CustomBottomTabBar
        animatedBar={animatedBar}
        navigation={navigation}
        isSelected={isSelected}
        setIsSelected={setIsSelected}
      />
    </Animated.View>
  );
}
