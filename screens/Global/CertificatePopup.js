import React from 'react';
import {View} from 'react-native';
import {Checkbox, Input, Popup} from '../../components';
import DatePickerInput from '../../components/DatePickerInput';

const CertificatePopup = ({modalVisible, setModalVisible, title}) => {
  return (
    <Popup
      title={title}
      buttonLabel="Save"
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      onButtonPress={() => {
        setModalVisible(!modalVisible);
      }}>
      <Input placeholder="Certificate Name" />
      <Input placeholder="University-Institute" />
      <Input placeholder="Certificate ID" />
      <Input placeholder="Certificate Validation URL" />
      <View style={{marginVertical: 10}}>
        <Checkbox label="This Certificate does not expire" />
      </View>
      <DatePickerInput label="Start" />
      <DatePickerInput label="End" />
    </Popup>
  );
};

export default CertificatePopup;
