import React from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import Svg, {G, Path, Circle, Polyline, Line} from 'react-native-svg';
import LinearGradient from 'react-native-linear-gradient';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import SearchInput from '../../components/SearchInput';
import {dark, green200, lightGreen, white} from '../../constants/colors';

function Message({isUser}) {
  return (
    <View
      style={{
        width: '100%',
        alignItems: isUser ? 'flex-start' : 'flex-end',
        marginBottom: 10,
      }}>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={isUser ? [lightGreen, lightGreen] : ['#0DC5A1', green200]}
        style={{
          width: '80%',
          borderRadius: 5,
          padding: 15,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{fontSize: 12, color: isUser ? dark : white}}>
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quas
          voluptas perferendis eaque dicta eos pariatur, ullam quod nostrum
          eveniet ipsa.
        </Text>
        <Text
          style={{
            fontSize: 10,
            color: isUser ? dark : white,
            textAlign: 'right',
            width: '100%',
          }}>
          14:30pm
        </Text>
      </LinearGradient>
    </View>
  );
}
function MessageImage({isUser}) {
  return (
    <TouchableOpacity
      style={{
        width: '100%',
        alignItems: isUser ? 'flex-start' : 'flex-end',
        marginBottom: 10,
      }}>
      <Image
        style={{
          width: '80%',
          borderRadius: 5,
        }}
        source={require('../../assets/portfiolioPic.png')}
      />
    </TouchableOpacity>
  );
}
function MessageDocument({isUser}) {
  return (
    <View
      style={{
        width: '100%',
        alignItems: isUser ? 'flex-start' : 'flex-end',
        marginBottom: 10,
      }}>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={isUser ? [lightGreen, lightGreen] : ['#0DC5A1', green200]}
        style={{
          width: '80%',
          borderRadius: 5,
          padding: 15,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            width: '100%',
            marginBottom: 10,
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              style={{marginRight: 8}}
              viewBox="0 0 24 24"
              fill="none"
              stroke={isUser ? '#242424' : '#ffffff'}
              stroke-width="2"
              stroke-linecap="round"
              stroke-linejoin="round"
              class="feather feather-file">
              <Path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></Path>
              <Polyline points="13 2 13 9 20 9"></Polyline>
            </Svg>
            <Text style={{color: isUser ? '#242424' : '#ffffff'}}>
              Document
            </Text>
          </View>
          <TouchableOpacity
            style={{
              borderRadius: 50,
              borderColor: isUser ? '#242424' : '#ffffff',
              borderWidth: 1,
              width: 24,
              height: 24,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width="18"
              height="18"
              viewBox="0 0 24 24"
              fill="none"
              stroke={isUser ? '#242424' : '#ffffff'}
              stroke-width="2"
              stroke-linecap="round"
              stroke-linejoin="round"
              class="feather feather-arrow-down">
              <Line x1="12" y1="5" x2="12" y2="19"></Line>
              <Polyline points="19 12 12 19 5 12"></Polyline>
            </Svg>
          </TouchableOpacity>
        </View>
        <Text
          style={{
            fontSize: 10,
            color: isUser ? dark : white,
            textAlign: 'right',
            width: '100%',
          }}>
          14:30pm
        </Text>
      </LinearGradient>
    </View>
  );
}
export default function ChatScreen({
  setProgress,
  drawerAnimationStyle,
  navigation,
}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  const chats = true;
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
        width: '100%',
      }}>
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: lightGreen,
          width: '100%',
          padding: 10,
          borderRadius: 10,
          alignItems: 'center',
        }}>
        <TouchableOpacity
          style={{padding: 8}}
          onPress={() => {
            navigation.navigate('Messenger');
          }}>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={15.189}
            height={13.026}>
            <G data-name="Group 505">
              <Path
                data-name="Path 730"
                d="M14.276 5.447a1.086 1.086 0 0 0-.188-.014H3.372l.234-.109a2.174 2.174 0 0 0 .614-.435l3-3a1.125 1.125 0 0 0 .158-1.44 1.087 1.087 0 0 0-1.63-.141L.318 5.737a1.087 1.087 0 0 0 0 1.537l5.434 5.434a1.087 1.087 0 0 0 1.63-.109 1.125 1.125 0 0 0-.157-1.441l-3-3.01a2.173 2.173 0 0 0-.543-.4l-.326-.147h10.672a1.125 1.125 0 0 0 1.147-.913 1.087 1.087 0 0 0-.899-1.241Z"
                fill={dark}
              />
            </G>
          </Svg>
        </TouchableOpacity>
        <Image
          source={require('../../assets/userPic.png')}
          style={{width: 45, height: 45, borderRadius: 45, marginRight: 15}}
        />
        <View style={{flex: 1, justifyContent: 'center'}}>
          <Text
            style={{
              fontSize: 14,
              fontWeight: 'bold',
              color: '#343434',
              marginBottom: 1,
            }}>
            John Doe
          </Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View
              style={{
                width: 12,
                height: 12,
                borderRadius: 12,
                backgroundColor: 'red',
                marginRight: 8,
              }}
            />
            <Text style={{color: dark, fontSize: 12}}>Offline</Text>
          </View>
        </View>
        <TouchableOpacity style={{padding: 8}}>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={22}
            height={22}
            viewBox="0 0 24 24"
            fill="none"
            stroke={dark}
            strokeWidth={1.5}
            strokeLinecap="round"
            strokeLinejoin="round">
            <Path d="M3 6h18M19 6v14a2 2 0 01-2 2H7a2 2 0 01-2-2V6m3 0V4a2 2 0 012-2h4a2 2 0 012 2v2M10 11v6M14 11v6" />
          </Svg>
        </TouchableOpacity>
      </View>
      {chats ? (
        <ScrollView style={{flex: 1, width: '100%', padding: 10}}>
          <Message isUser={true} />
          <Message />
          <Message />
          <Message isUser={true} />
          <Message />
          <Message isUser={true} />
          <Message />
          <MessageDocument />
          <MessageDocument isUser={true} />
          <MessageImage />
          <MessageImage isUser={true} />
          <Message isUser={true} />
          <Message />
          <Message isUser={true} />
          <Message />
          <Message />
          <Message />
          <Message />
        </ScrollView>
      ) : (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: 70,
          }}>
          <Image
            source={require('../../assets/noChat.png')}
            style={{width: 200, height: 200}}
          />
          <Text style={{color: dark}}>No conversation yet</Text>
        </View>
      )}

      <View
        style={{
          flexDirection: 'row',
          backgroundColor: lightGreen,
          width: '100%',
          padding: 10,
          height: 66,
          borderRadius: 10,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          style={{padding: 8}}
          onPress={() => {
            launchCamera(
              {
                cameraType: 'back',
                includeBase64: true,
              },
              () => {},
            );
          }}>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={20}
            height={20}
            viewBox="0 0 24 24"
            fill="none"
            stroke={dark}
            strokeWidth={1.5}
            strokeLinecap="round"
            strokeLinejoin="round">
            <Path d="M23 19a2 2 0 01-2 2H3a2 2 0 01-2-2V8a2 2 0 012-2h4l2-3h6l2 3h4a2 2 0 012 2z" />
            <Circle cx={12} cy={13} r={4} />
          </Svg>
        </TouchableOpacity>
        <TouchableOpacity
          style={{padding: 8}}
          onPress={() => {
            launchImageLibrary(
              {
                mediaType: 'mixed',
              },
              () => {},
            );
          }}>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={20}
            height={20}
            viewBox="0 0 24 24"
            fill="none"
            stroke={dark}
            strokeWidth={1.5}
            strokeLinecap="round"
            strokeLinejoin="round">
            <Path d="M21.44 11.05l-9.19 9.19a6 6 0 01-8.49-8.49l9.19-9.19a4 4 0 015.66 5.66l-9.2 9.19a2 2 0 01-2.83-2.83l8.49-8.48" />
          </Svg>
        </TouchableOpacity>

        <SearchInput
          style={{width: '60%', height: 44}}
          placeholder="Write Something"
        />
        <TouchableOpacity
          style={{
            marginLeft: 8,
          }}>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={['#0DC5A1', green200]}
            style={{
              width: 50,
              height: 50,
              borderRadius: 50,
              padding: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={22}
              height={22}
              fill="none"
              stroke={white}
              strokeWidth={1.5}
              strokeLinecap="round"
              strokeLinejoin="round"
              className="prefix__feather prefix__feather-arrow-right">
              <Path d="M5 12h14M12 5l7 7-7 7" />
            </Svg>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </Animated.View>
  );
}
