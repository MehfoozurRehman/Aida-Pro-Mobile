import React from 'react';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import {faqActive} from '../../assets/icons';
import HeaderPrimary from '../../components/HeaderPrimary';
import ButtonPrimary from '../../components/ButtonPrimary';
import {ScrollView, Text, View, Image} from 'react-native';
import {dark, lightGreen, white} from '../../constants/colors';

export default function FaqScreen({setProgress, drawerAnimationStyle}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary title="FAQ Details" backPath="FAQ" icon={faqActive} />
      <ScrollView
        style={{
          flex: 1,
          width: '100%',
          padding: 15,
        }}>
        <View
          style={{
            width: '100%',
            backgroundColor: lightGreen,
            borderRadius: 10,
            padding: 12,
            marginVertical: 10,
            display: 'flex',
            flexDirection: 'column',
          }}>
          <Text
            style={{
              fontSize: 16,
              fontWeight: 'bold',
              color: dark,
              marginVertical: 4,
            }}>
            What is Aida?
          </Text>
          <Text
            style={{
              fontSize: 14,
              fontWeight: 'bold',
              color: dark,
              marginTop: 4,
            }}>
            What does Aida do?
          </Text>
          <Text
            style={{
              fontSize: 12,
              color: dark,
              marginBottom: 4,
            }}>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed
            veritatis accusamus nemo quisquam alias delectus cororis recusandae
            aliquam, inventore a nulla blanditiis ut, quibusdam numquam
            praesentium? Quibusdam nemo hic in!
          </Text>
          <Text
            style={{
              fontSize: 14,
              fontWeight: 'bold',
              color: dark,
              marginTop: 6,
            }}>
            Who are the founders of Aida?
          </Text>
          <Text
            style={{
              fontSize: 12,
              color: dark,
              marginBottom: 4,
            }}>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolore
            fugit aspernatur libero iusto at, officiis eaque quasi tenetur
            possimus expedita enim, esse aliquid cupiditate qui dolor, a quae id
            tempora velit! Adipisci excepturi repudiandae distinctio, doloribus
            consequuntur ipsum, exercitationem aspernatur optio fugiat porro
            itaque ducimus dolores totam quas nulla reprehenderit.
          </Text>
          <View style={{width: '100%', height: 150, marginVertical: 10}}>
            <Image
              style={{width: '100%', height: '100%', borderRadius: 10}}
              source={require('../../assets/blogImage.png')}
            />
          </View>
          <Text
            style={{
              fontSize: 14,
              fontWeight: 'bold',
              color: dark,
              marginTop: 6,
            }}>
            Who are the founders of Aida?
          </Text>
          <Text
            style={{
              fontSize: 12,
              color: dark,
              marginBottom: 4,
            }}>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolore
            iusto beatae itaque consequatur tempore suscipit nemo, deleniti
            temporibus aspernatur totam et earum adipisci error ea tempora
            pariatur eveniet sint eaque laudantium quisquam optio nulla. Magni
            minus laborum suscipit, iusto culpa totam voluptatibus eligendi
            libero veritatis possimus perspiciatis fuga harum expedita id
            voluptas quidem inventore odit tempore iste debitis excepturi
            cupiditate porro. Dolorem illo vero reprehenderit fuga! Impedit,
            natus. Eaque praesentium a ut voluptatem qui laborum quibusdam quae
            soluta incidunt mollitia?
          </Text>
          <View style={{width: '100%', height: 150, marginVertical: 10}}>
            <Image
              style={{width: '100%', height: '100%', borderRadius: 10}}
              source={require('../../assets/blogImage.png')}
            />
          </View>
        </View>
        <View
          style={{
            width: '100%',
            backgroundColor: lightGreen,
            borderRadius: 10,
            padding: 12,
            display: 'flex',
            flexDirection: 'column',
            marginBottom: 40,
          }}>
          <Text
            style={{
              fontSize: 14,
              fontWeight: 'bold',
              color: dark,
              marginTop: 6,
            }}>
            Still need help?
          </Text>
          <Text
            style={{
              fontSize: 12,
              color: dark,
              marginBottom: 4,
            }}>
            Our helpful customer service team is here to assist you
          </Text>
          <ButtonPrimary label="Contact Us" />
        </View>
      </ScrollView>
    </Animated.View>
  );
}
