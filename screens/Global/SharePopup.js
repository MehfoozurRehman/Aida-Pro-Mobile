import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import {Popup} from '../../components';
import Svg, {Circle, Path, Rect} from 'react-native-svg';
import SearchInput from '../../components/SearchInput';

const SharePopup = ({modalVisible, setModalVisible}) => {
  return (
    <Popup
      small={true}
      title="Share this discussion"
      buttonLabel="Copy Link"
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      onButtonPress={() => {
        setModalVisible(!modalVisible);
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginVertical: 10,
        }}>
        <TouchableOpacity>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={24}
            height={24}
            viewBox="0 0 24 24"
            fill="none"
            stroke="#3b5998"
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
            className="feather feather-facebook">
            <Path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z" />
          </Svg>
        </TouchableOpacity>
        <TouchableOpacity>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={24}
            height={24}
            viewBox="0 0 24 24"
            fill="none"
            stroke="#f80"
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
            className="feather feather-instagram">
            <Rect x={2} y={2} width={20} height={20} rx={5} ry={5} />
            <Path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37z" />
            <Path d="M17.5 6.5L17.51 6.5" />
          </Svg>
        </TouchableOpacity>
        <TouchableOpacity>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={24}
            height={24}
            viewBox="0 0 24 24"
            fill="none"
            stroke="#0e76a8"
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
            className="feather feather-linkedin">
            <Path d="M16 8a6 6 0 016 6v7h-4v-7a2 2 0 00-2-2 2 2 0 00-2 2v7h-4v-7a6 6 0 016-6z" />
            <Path d="M2 9H6V21H2z" />
            <Circle cx={4} cy={4} r={2} />
          </Svg>
        </TouchableOpacity>
        <TouchableOpacity>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={24}
            height={24}
            viewBox="0 0 24 24"
            fill="none"
            stroke="#00acee"
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
            className="feather feather-twitter">
            <Path d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z" />
          </Svg>
        </TouchableOpacity>
        <TouchableOpacity>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={24}
            height={24}
            viewBox="0 0 24 24"
            fill="none"
            stroke="#833ab4"
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
            className="feather feather-twitch">
            <Path d="M21 2H3v16h5v4l4-4h5l4-4V2zm-10 9V7m5 4V7" />
          </Svg>
        </TouchableOpacity>
      </View>
      <SearchInput placeholder="link" error={false} errorMessage="error" />
    </Popup>
  );
};

export default SharePopup;
