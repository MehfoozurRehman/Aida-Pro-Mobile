import React, {useState} from 'react';
import Animated from 'react-native-reanimated';
import {coffeeActive} from '../../assets/icons';
import Svg, {G, Path} from 'react-native-svg';
import CoffeeCornerCard from '../../components/CoffeeCornerCard';
import {useNavigation} from '@react-navigation/native';
import {useDrawerProgress} from '@react-navigation/drawer';
import AskQuestionPopup from './AskQuestionPopup';
import LinearGradient from 'react-native-linear-gradient';
import CustomBottomTabBar from '../../navigation/CustomBottomTabBar';
import HeaderPrimary from '../../components/HeaderPrimary';
import SearchInput from '../../components/SearchInput';
import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import SharePopup from './SharePopup';
import {dark, white} from '../../constants/colors';

function CoffeeCornerSearchFilter() {
  return (
    <Text
      style={{
        backgroundColor: '#0DCAA0',
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderRadius: 8,
        color: white,
        marginRight: 10,
        fontSize: 10,
        fontWeight: '500',
      }}>
      Machine Learning
    </Text>
  );
}

export default function CoffeeCornerScreen({
  setProgress,
  drawerAnimationStyle,
  animatedBar,
  isSelected,
  setIsSelected,
}) {
  const navigation = useNavigation();

  const progress = useDrawerProgress();
  setProgress(progress);
  const [modalVisible, setModalVisible] = useState(false);
  const [shareModalVisible, setShareModalVisible] = useState(false);
  const DATA = [
    {
      id: '1',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '2',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '3',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '4',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '5',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '6',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '7',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '8',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '9',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
  ];
  const renderItem = ({item}) => (
    <CoffeeCornerCard
      onPress={() => {
        navigation.navigate('Coffee Corner Discussion');
      }}
      onShare={() => {
        setShareModalVisible(true);
      }}
    />
  );
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <AskQuestionPopup
        setModalVisible={setModalVisible}
        modalVisible={modalVisible}
      />
      <SharePopup
        setModalVisible={setShareModalVisible}
        modalVisible={shareModalVisible}
      />
      <TouchableOpacity
        style={{
          width: 60,
          height: 60,
          position: 'absolute',
          bottom: 70,
          right: 20,
          zIndex: 500,
        }}
        onPress={() => setModalVisible(true)}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#F6B038', '#F5833C']}
          style={{
            width: '100%',
            height: '100%',
            borderRadius: 50,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={24}
            height={24}
            fill="none"
            stroke={white}
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
            className="feather feather-plus">
            <Path d="M12 5v14M5 12h14" />
          </Svg>
        </LinearGradient>
      </TouchableOpacity>
      <HeaderPrimary
        title="Coffee Coner"
        backPath="Home"
        icon={coffeeActive}
        noBackButton
      />

      <FlatList
        ListHeaderComponent={
          <>
            <Text
              style={{
                fontSize: 20,
                color: dark,
                fontWeight: '600',
                marginTop: -5,
              }}>
              Ask questions, participate in discussions
            </Text>
            <SearchInput
              placeholder="Search"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={20.681}
                  height={20.681}>
                  <G
                    data-name="Icon feather-search"
                    fill="none"
                    stroke="#545454"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={1}>
                    <Path
                      data-name="Path 2299"
                      d="M17.553 9.526A8.026 8.026 0 119.526 1.5a8.026 8.026 0 018.027 8.026z"
                    />
                    <Path
                      data-name="Path 2300"
                      d="M19.559 19.559l-4.364-4.364"
                    />
                  </G>
                </Svg>
              }
            />
            <FlatList
              style={{flexDirection: 'row', marginTop: 10}}
              data={DATA}
              horizontal={true}
              renderItem={() => <CoffeeCornerSearchFilter text="" />}
              keyExtractor={item => item.id}
            />
            <Text
              style={{
                fontSize: 18,
                color: dark,
                fontWeight: '600',
                marginTop: 10,
              }}>
              Recent Discussions
            </Text>
            <View
              style={{
                width: 60,
                height: 2,
                backgroundColor: '#0DC5A1',
                marginTop: 1,
                marginBottom: 10,
              }}
            />
          </>
        }
        style={{
          flex: 1,
          width: '100%',
          paddingVertical: 20,
          paddingHorizontal: 15,
          marginBottom: 60,
        }}
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />

      <CustomBottomTabBar
        animatedBar={animatedBar}
        navigation={navigation}
        isSelected={isSelected}
        setIsSelected={setIsSelected}
      />
    </Animated.View>
  );
}
const styles = StyleSheet.create({
  badge: {
    backgroundColor: white,
    padding: 10,
    borderRadius: 10,
    marginRight: 10,
  },
});
