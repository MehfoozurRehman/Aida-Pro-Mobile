import React from 'react';
import {Input, Popup} from '../../components';
import Select from '../../components/Select';
import DatePickerInput from '../../components/DatePickerInput';

const DegreePopup = ({modalVisible, setModalVisible, title}) => {
  return (
    <Popup
      title={title}
      buttonLabel="Save"
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      onButtonPress={() => {
        setModalVisible(!modalVisible);
      }}>
      <Select placeholder="Degree" />
      <Input placeholder="University-Institute" />
      <Input placeholder="Field of Study" />
      <Input placeholder="Grades" />
      <DatePickerInput label="Start" />
      <DatePickerInput label="End" />
    </Popup>
  );
};

export default DegreePopup;
