import React from 'react';
import {ScrollView, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import {details, detailsActive} from '../../assets/icons';
import {Input} from '../../components';
import {personalDetailsInputs} from '../../constants/constants';
import ButtonPrimary from '../../components/ButtonPrimary';
import HeaderPrimary from '../../components/HeaderPrimary';
import CustomBottomTabBar from '../../navigation/CustomBottomTabBar';
import {white} from '../../constants/colors';

export default function PersonalDetailsEdit({
  setProgress,
  drawerAnimationStyle,
  navigation,
  animatedBar,

  isSelected,
  setIsSelected,
}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary
        title="Personal Details"
        backPath="Personal Details"
        icon={detailsActive}
      />
      <ScrollView
        style={{
          flex: 1,
          width: '100%',
          paddingHorizontal: 20,
          marginVertical: 10,
        }}>
        {personalDetailsInputs.map((inputData, i) => (
          <Input
            key={i}
            placeholder={inputData.placeholder}
            secureTextEntry={inputData.secureTextEntry}
            svg={inputData.svg}
            transparent={inputData.transparent}
            variant={inputData.variant}
          />
        ))}
        <View style={{marginVertical: 20, marginBottom: 50}}>
          <ButtonPrimary
            onPress={() => {
              navigation.navigate('Personal Details');
            }}
            label="Save"
          />
        </View>
      </ScrollView>
      <CustomBottomTabBar
        animatedBar={animatedBar}
        navigation={navigation}
        isSelected={isSelected}
        setIsSelected={setIsSelected}
      />
    </Animated.View>
  );
}
