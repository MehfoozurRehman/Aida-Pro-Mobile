import React, {useState} from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import {details, detailsActive} from '../../assets/icons';
import {Checkbox, Input, Select} from '../../components';
import EducationDetailsCard from '../../components/EducationDetailsCard';
import ProfessionalDetailsCard from '../../components/ProfessionalDetailsCard';
import SkillsTag from '../../components/SkillsTag';
import CertificatesCard from '../../components/CertificatesCard';
import WorkExperienceCard from '../../components/WorkExperienceCard';
import CustomBottomTabBar from '../../navigation/CustomBottomTabBar';
import {CertificatePopup, DegreePopup, ExperiencePopup, SkillPopup} from '.';
import SocialsPopup from './SocialsPopup';
import SocialsCard from '../../components/SocialsCard';
import ButtonPrimary from '../../components/ButtonPrimary';
import HeaderPrimary from '../../components/HeaderPrimary';
import Svg, {
  Defs,
  LinearGradient,
  Stop,
  G,
  Path,
  Circle,
} from 'react-native-svg';
import {
  dark,
  green100,
  green200,
  lightGreen,
  white,
  yellow,
} from '../../constants/colors';

export default function ProfessionalDetailsPreview({
  setProgress,
  drawerAnimationStyle,
  isJob,
  animatedBar,
  navigation,
  isSelected,
  setIsSelected,
}) {
  const [addDegreesModalVisible, setAddDegreesModalVisible] = useState(false);
  const [editDegreesModalVisible, setEditDegreesModalVisible] = useState(false);
  const [addSkillsModalVisible, setAddSkillsModalVisible] = useState(false);
  const [editSkillsModalVisible, setEditSkillsModalVisible] = useState(false);
  const [addSocialsModalVisible, setAddSocialsModalVisible] = useState(false);
  const [editSocialsModalVisible, setEditSocialsModalVisible] = useState(false);
  const [addCertificationsModalVisible, setAddCertificationsModalVisible] =
    useState(false);
  const [editCertificationsModalVisible, setEditCertificationsModalVisible] =
    useState(false);
  const [addExperienceModalVisible, setAddExperienceModalVisible] =
    useState(false);
  const [editExperienceModalVisible, setEditExperienceModalVisible] =
    useState(false);
  // const [modalVisible, setModalVisible] = useState(false);
  // const [modalVisible, setModalVisible] = useState(false);
  const [isWorkLocation, setIsWorkLocation] = useState(false);
  const progress = useDrawerProgress();
  setProgress(progress);
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary
        title="Professional Details"
        backPath="Home"
        icon={detailsActive}
        noBackButton
      />

      <ScrollView
        style={{
          flex: 1,
          width: '100%',
          padding: 20,
        }}>
        <ProfessionalDetailsCard
          text="Degree"
          btnLabel="Add New"
          onPress={() => {
            setAddDegreesModalVisible(true);
          }}
          svg={
            <Svg xmlns="http://www.w3.org/2000/svg" width={43.49} height={34}>
              <Defs>
                <LinearGradient
                  id="a"
                  x1={0.5}
                  x2={0.5}
                  y2={1}
                  gradientUnits="objectBoundingBox">
                  <Stop offset={0} stopColor="#f6a938" />
                  <Stop offset={1} stopColor="#f5833c" />
                </LinearGradient>
                <LinearGradient
                  id="b"
                  x1={0.5}
                  x2={0.5}
                  y2={1}
                  gradientUnits="objectBoundingBox">
                  <Stop offset={0} stopColor="#fff" />
                  <Stop offset={1} stopColor="#f5833c" />
                </LinearGradient>
              </Defs>
              <G data-name="Group 1490">
                <G data-name="Group 1489">
                  <G data-name="Group 1488">
                    <G data-name="Group 1472">
                      <G data-name="Group 1471">
                        <G data-name="Group 1470">
                          <Path
                            data-name="Path 21312"
                            d="m82.923 116.121-12.081 5.253a2.543 2.543 0 0 1-2.028 0l-12.081-5.253a2.543 2.543 0 0 1-1.529-2.333V103.11h29.248v10.679a2.544 2.544 0 0 1-1.529 2.333Z"
                            transform="translate(-48.082 -87.585)"
                            fill="url(#a)"
                          />
                        </G>
                      </G>
                    </G>
                    <G data-name="Group 1473">
                      <Path
                        data-name="Path 21313"
                        d="m82.923 148.6-12.081 5.253a2.543 2.543 0 0 1-2.028 0L56.732 148.6a2.543 2.543 0 0 1-1.529-2.333v2.009a2.544 2.544 0 0 0 1.529 2.333l12.081 5.253a2.543 2.543 0 0 0 2.028 0l12.081-5.253a2.543 2.543 0 0 0 1.529-2.333v-2.009a2.544 2.544 0 0 1-1.528 2.333Z"
                        transform="translate(-48.082 -122.072)"
                        fill="url(#a)"
                      />
                    </G>
                    <G data-name="Group 1476">
                      <G data-name="Group 1475">
                        <G data-name="Group 1474">
                          <Path
                            data-name="Path 21314"
                            d="m40.465 47.863-19.2-8.583a2.543 2.543 0 0 1 0-4.644l19.2-8.583a2.543 2.543 0 0 1 2.076 0l19.2 8.583a2.543 2.543 0 0 1 0 4.644l-19.2 8.583a2.543 2.543 0 0 1-2.076 0Z"
                            transform="translate(-19.758 -25.832)"
                            fill="url(#a)"
                          />
                        </G>
                      </G>
                    </G>
                    <G data-name="Group 1478">
                      <G data-name="Group 1477">
                        <Path
                          data-name="Path 21315"
                          d="M53.6 37.044a.4.4 0 0 1-.163-.769l14.079-6.292a1.716 1.716 0 0 1 .707-.153 1.716 1.716 0 0 1 .707.153l3.236 1.444a.4.4 0 0 1 .2.532.415.415 0 0 1-.532.2L68.6 30.713a.926.926 0 0 0-.763 0l-14.068 6.295a.386.386 0 0 1-.165.036Zm19.872-4.193a.418.418 0 0 1-.163-.034.4.4 0 0 1-.211-.223.389.389 0 0 1 .008-.307.408.408 0 0 1 .53-.2.4.4 0 0 1 .2.53.4.4 0 0 1-.368.237Z"
                          transform="translate(-46.482 -29.027)"
                          fill="url(#a)"
                        />
                      </G>
                    </G>
                    <G data-name="Group 1479">
                      <Path
                        data-name="Path 21316"
                        d="M62.2 70.963a2.627 2.627 0 0 1-.456.262l-19.2 8.583a2.543 2.543 0 0 1-2.076 0l-19.2-8.583a2.64 2.64 0 0 1-.456-.262 2.545 2.545 0 0 0 .456 4.382l19.2 8.583a2.543 2.543 0 0 0 2.076 0l19.2-8.583a2.545 2.545 0 0 0 .456-4.382Z"
                        transform="translate(-19.761 -61.896)"
                        fill="url(#a)"
                      />
                    </G>
                    <G data-name="Group 1482">
                      <G data-name="Group 1481">
                        <G data-name="Group 1480">
                          <Path
                            data-name="Path 21317"
                            d="M31.871 86.094a.636.636 0 0 1-.636-.636V74.274a.635.635 0 0 1 .478-.616l18.8-4.819a.636.636 0 0 1 .316 1.232l-18.325 4.7v10.69a.636.636 0 0 1-.636.636Z"
                            transform="translate(-28.929 -60.183)"
                            fill="url(#b)"
                          />
                        </G>
                      </G>
                    </G>
                    <G data-name="Group 1485">
                      <G data-name="Group 1484">
                        <G data-name="Group 1483">
                          <Path
                            data-name="Path 21318"
                            d="M27.641 137.056H25.3a.824.824 0 0 1-.824-.824v-2.906a1.993 1.993 0 1 1 3.986 0v2.906a.824.824 0 0 1-.821.824Z"
                            transform="translate(-23.531 -110.139)"
                            fill="url(#a)"
                          />
                        </G>
                      </G>
                    </G>
                    <G data-name="Group 1486">
                      <Path
                        data-name="Path 21319"
                        d="M26.471 131.333h-.016v2.889a.824.824 0 0 1-.824.824h-1.153v1.185a.824.824 0 0 0 .824.824h2.338a.824.824 0 0 0 .824-.824v-2.906a1.993 1.993 0 0 0-1.993-1.992Z"
                        transform="translate(-23.53 -110.139)"
                        fill="url(#a)"
                      />
                    </G>
                    <G data-name="Group 1487">
                      <Path
                        data-name="Path 21320"
                        d="M27.286 132.2a1.983 1.983 0 0 1 .175.814v2.906a.824.824 0 0 1-.824.824h-2.158v.18a.824.824 0 0 0 .824.824h2.338a.824.824 0 0 0 .824-.824v-2.906a1.992 1.992 0 0 0-1.179-1.818Z"
                        transform="translate(-23.531 -110.835)"
                        fill="url(#a)"
                      />
                    </G>
                  </G>
                </G>
              </G>
            </Svg>
          }>
          <EducationDetailsCard
            onPress={() => {
              setEditDegreesModalVisible(true);
            }}
          />
          <EducationDetailsCard
            onPress={() => {
              setEditDegreesModalVisible(true);
            }}
          />
        </ProfessionalDetailsCard>
        <ProfessionalDetailsCard
          text="Skills"
          btnLabel="Add New"
          onPress={() => {
            setAddSkillsModalVisible(true);
          }}
          svg={
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={41.749}
              height={41.749}>
              <Defs>
                <LinearGradient
                  id="a"
                  x1={0.5}
                  x2={0.5}
                  y2={1}
                  gradientUnits="objectBoundingBox">
                  <Stop offset={0} stopColor={green100} />
                  <Stop offset={1} stopColor={green200} />
                </LinearGradient>
                <LinearGradient
                  id="b"
                  x1={0.5}
                  x2={0.5}
                  y2={1}
                  gradientUnits="objectBoundingBox">
                  <Stop offset={0} stopColor={green100} />
                  <Stop offset={1} stopColor="#007e77" />
                </LinearGradient>
              </Defs>
              <Path
                data-name="Path 21160"
                d="m44.416 10.182-5.328 5.328-5.124-1.03-1.029-5.124 5.328-5.328a8.7 8.7 0 0 0-10.894 9.4 2.932 2.932 0 0 1-.877 2.377L17.7 24.6l-1.892 1.892a2.932 2.932 0 0 1-2.377.877 8.7 8.7 0 0 0-9.4 10.894l5.328-5.328 5.125 1.029 1.029 5.125-5.328 5.328a8.7 8.7 0 0 0 10.894-9.4 2.932 2.932 0 0 1 .877-2.377l10.681-10.687a2.932 2.932 0 0 1 2.377-.877 8.7 8.7 0 0 0 9.4-10.894Z"
                transform="translate(-3.436 -3.436)"
                fill="url(#a)"
              />
              <Path
                data-name="Path 21161"
                d="M7.732 30.586a8.689 8.689 0 0 1 7.147-2.493 2.932 2.932 0 0 0 2.377-.877l1.892-1.892 8.792-8.792a2.932 2.932 0 0 0 .877-2.377 8.685 8.685 0 0 1 8.994-9.676l.45-.45a8.7 8.7 0 0 0-10.894 9.4 2.932 2.932 0 0 1-.877 2.377L17.7 24.6l-1.892 1.892a2.932 2.932 0 0 1-2.377.877 8.7 8.7 0 0 0-9.4 10.894L5.2 37.087a8.647 8.647 0 0 1 2.529-6.5Z"
                transform="translate(-3.436 -3.436)"
                fill="url(#b)"
              />
              <Path
                data-name="Path 21162"
                d="m37.53 40.693 4.219 1.055-1.056-4.218a4.335 4.335 0 0 0-3.163 3.163Z"
                fill="#58595b"
              />
              <Path
                data-name="Path 21163"
                d="m36.11 36.11-2.562-.512v4.1l3.982 1a4.335 4.335 0 0 1 3.163-3.163l-1-3.979h-4.1Z"
                fill="#d8d8d8"
              />
              <Path data-name="Path 21164" d="M8.446 10.499Z" fill="#d1e7f8" />
              <Path
                data-name="Path 21165"
                d="M12.549 6.398 9.473 3.323l-6.15 6.15 3.075 3.075 2.05-2.05 2.051-2.051Z"
                fill={dark}
              />
              <Path
                data-name="Path 21166"
                d="M7.424 1.277a4.349 4.349 0 1 0-6.15 6.15l2.05 2.05 6.15-6.15Z"
                fill="#d1d3d4"
              />
              <Path
                data-name="Path 21167"
                d="m24.198 18.399-1.025-1.025 3.107-3.112a.725.725 0 0 1 1.025 1.025Z"
                fill="#fff"
              />
              <Path
                data-name="Path 21168"
                d="M14.262 27.31a.725.725 0 0 1 0-1.025l3.112-3.112 1.025 1.025-3.112 3.112a.725.725 0 0 1-1.025 0Z"
                fill="#fff"
              />
              <Path
                data-name="Path 21169"
                d="m8.448 10.5-2.05 2.052 8.241 8.239 6.147 6.147 12.762 12.753v-4.1Z"
                fill="#ff7f23"
              />
              <Path
                data-name="Path 21170"
                d="M39.691 33.548 26.933 20.786l-6.142-6.147-8.239-8.241-2.052 2.05 25.1 25.1Z"
                fill="#f6a039"
              />
              <Path
                data-name="Path 21171"
                d="m33.548 35.598 2.562.512-.512-2.562-25.1-25.1-2.05 2.05Z"
                fill="#f58e3b"
              />
            </Svg>
          }>
          <TouchableOpacity
            onPress={() => {
              setEditSkillsModalVisible(true);
            }}
            style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap'}}>
            <SkillsTag text="Computer Scientist" />
            <SkillsTag text="Python" />
            <SkillsTag text="React Js" />
            <SkillsTag text="Computer Scientist" />
            <SkillsTag text="Computer Scientist" />
          </TouchableOpacity>
        </ProfessionalDetailsCard>
        <ProfessionalDetailsCard
          text="Certificates"
          btnLabel="Add New"
          onPress={() => {
            setAddCertificationsModalVisible(true);
          }}
          svg={
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={49.049}
              height={56.167}>
              <Defs>
                <LinearGradient
                  id="a"
                  x1={0.5}
                  x2={0.5}
                  y2={1}
                  gradientUnits="objectBoundingBox">
                  <Stop offset={0} stopColor="#f6a938" />
                  <Stop offset={1} stopColor="#f5833c" />
                </LinearGradient>
              </Defs>
              <Path
                d="M30.109 32.841a19.65 19.65 0 0 1 .673 4.364 6.474 6.474 0 0 0-6.495.6 25.207 25.207 0 0 0-.641-4.975q1.7.035 3.477.035c1.02.004 2.01-.007 2.986-.024ZM28.057 47.8a4.665 4.665 0 1 1 4.665-4.665 4.665 4.665 0 0 1-4.665 4.665ZM1 39.4a26.169 26.169 0 0 1 .933-7.931 26.169 26.169 0 0 1 .932 7.931 26.169 26.169 0 0 1-.933 7.931A26.168 26.168 0 0 1 1 39.4Zm2.821 8.09a30.942 30.942 0 0 0 .912-8.09 30.949 30.949 0 0 0-.912-8.091A163.7 163.7 0 0 0 21.7 32.786a24.018 24.018 0 0 1 .761 6.614c0 .135 0 .264-.006.394a6.464 6.464 0 0 0-.3 6.113l-.041.1a162.6 162.6 0 0 0-18.295 1.484ZM24.7 54.329l-.681-1.567a.933.933 0 0 0-1.223-.485l-1.57.694 2.188-5.245a6.525 6.525 0 0 0 3.3 1.8Zm8.609-2.053a.933.933 0 0 0-1.223.485l-.69 1.552-2-4.789a6.531 6.531 0 0 0 3.3-1.8l2.183 5.225Zm18.815-4.525a151.1 151.1 0 0 0-18.1-1.679l-.066-.16a6.493 6.493 0 0 0-1.258-7.359 25.97 25.97 0 0 0-.666-5.748 155.744 155.744 0 0 0 20.091-1.756c.433.735 1.12 3.643 1.12 8.352s-.688 7.615-1.116 8.35Z"
                transform="rotate(-54.97 11.808 37.884)"
                fill="url(#a)"
                data-name="001---Degree"
              />
            </Svg>
          }>
          <CertificatesCard
            onPress={() => {
              setEditCertificationsModalVisible(true);
            }}
          />
          <CertificatesCard
            onPress={() => {
              setEditCertificationsModalVisible(true);
            }}
          />
          <CertificatesCard
            onPress={() => {
              setEditCertificationsModalVisible(true);
            }}
          />
        </ProfessionalDetailsCard>
        <ProfessionalDetailsCard
          text="Work Experience"
          btnLabel="Add New"
          onPress={() => {
            setAddExperienceModalVisible(true);
          }}
          svg={
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={45.148}
              height={39.488}>
              <Defs>
                <LinearGradient
                  id="a"
                  x1={0.5}
                  x2={0.5}
                  y2={1}
                  gradientUnits="objectBoundingBox">
                  <Stop offset={0} stopColor="#f6a938" />
                  <Stop offset={1} stopColor="#f5833c" />
                </LinearGradient>
                <LinearGradient
                  id="b"
                  x1={0.5}
                  x2={0.5}
                  y2={1}
                  gradientUnits="objectBoundingBox">
                  <Stop offset={0} stopColor="#f6a938" />
                  <Stop offset={1} stopColor="#fff4ee" />
                </LinearGradient>
                <LinearGradient
                  id="c"
                  x1={0.5}
                  x2={0.5}
                  y2={1}
                  gradientUnits="objectBoundingBox">
                  <Stop offset={0} stopColor={green100} />
                  <Stop offset={1} stopColor={green200} />
                </LinearGradient>
              </Defs>
              <G data-name="suitcase (1)">
                <G data-name="Group 1096">
                  <Path
                    data-name="Path 3425"
                    d="M183.373 34.091a8.882 8.882 0 0 0-1.961-1.047 14.434 14.434 0 0 0-10.335 0 8.882 8.882 0 0 0-1.961 1.047 1.484 1.484 0 0 0-.584 1.18V37.6l1.484.794 1.484-.794v-1.489a11.188 11.188 0 0 1 9.489 0V37.6l1.484.794 1.484-.794v-2.329a1.483 1.483 0 0 0-.584-1.18Z"
                    transform="translate(-153.671 -32.088)"
                    fill="url(#a)"
                  />
                  <G
                    data-name="Group 1095"
                    transform="translate(14.861 5.511)"
                    fill="url(#b)">
                    <Path
                      data-name="Path 3426"
                      d="M0 0v1.638l1.484.794 1.484-.794V0Z"
                    />
                    <Path
                      data-name="Path 3427"
                      d="M15.425 0h-2.973v1.638l1.484.794 1.484-.794Z"
                    />
                  </G>
                  <Path
                    data-name="Path 3428"
                    d="M13.247 298.286v13.182a2.834 2.834 0 0 0 2.834 2.834h37.144a2.834 2.834 0 0 0 2.834-2.834v-13.182Z"
                    transform="translate(-12.079 -274.813)"
                    fill="url(#c)"
                  />
                  <Path
                    data-name="Path 3429"
                    d="M1.168 21.834v1.638c2.354 1.869 8.576 5.5 21.406 5.5s19.052-3.628 21.406-5.5v-1.643H1.168Z"
                    fill={yellow}
                  />
                  <Path
                    data-name="Path 3430"
                    d="M1.045 126.91c2.256 1.837 8.469 5.6 21.529 5.6s19.273-3.759 21.529-5.6a4.733 4.733 0 0 0 1.045-2.992V116a2.834 2.834 0 0 0-2.834-2.833H2.834A2.834 2.834 0 0 0 0 116v7.916a4.733 4.733 0 0 0 1.045 2.992Z"
                    transform="translate(0 -106.019)"
                    fill="url(#c)"
                  />
                  <Path
                    data-name="Path 3431"
                    d="M44.1 237.276c-2.256 1.837-8.469 5.6-21.529 5.6s-19.273-3.759-21.529-5.6A2.834 2.834 0 0 1 0 235.078v1.638a2.834 2.834 0 0 0 1.045 2.2c2.256 1.837 8.469 5.6 21.529 5.6s19.273-3.759 21.529-5.6a2.834 2.834 0 0 0 1.045-2.2v-1.638a2.834 2.834 0 0 1-1.048 2.198Z"
                    transform="translate(0 -217.179)"
                    fill="url(#a)"
                  />
                  <Path
                    data-name="Path 3432"
                    d="M22.573 30.058c1.466 0 2.654-1.089 2.654-3.448v-1.609a.944.944 0 0 0-.944-.944h-3.419a.944.944 0 0 0-.945.944v1.61c.003 2.359 1.188 3.447 2.654 3.447Z"
                    fill="#fff"
                  />
                  <Path
                    data-name="Path 3433"
                    d="M228.551 336.528a2.654 2.654 0 0 1-2.654-2.654v1.639a2.654 2.654 0 0 0 5.308 0v-1.639a2.654 2.654 0 0 1-2.654 2.654Z"
                    transform="translate(-205.978 -307.263)"
                    fill="url(#c)"
                  />
                </G>
              </G>
            </Svg>
          }>
          <WorkExperienceCard
            onPress={() => {
              setEditExperienceModalVisible(true);
            }}
          />
          <WorkExperienceCard
            onPress={() => {
              setEditExperienceModalVisible(true);
            }}
          />
        </ProfessionalDetailsCard>
        <View
          style={{
            padding: 20,
            backgroundColor: lightGreen,
            borderRadius: 10,
            marginVertical: 10,
          }}>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              marginBottom: 20,
            }}>
            <Svg xmlns="http://www.w3.org/2000/svg" width={42} height={58}>
              <Defs>
                <LinearGradient
                  id="a"
                  x1={0.5}
                  x2={0.5}
                  y2={1}
                  gradientUnits="objectBoundingBox">
                  <Stop offset={0} stopColor="#f6a938" />
                  <Stop offset={1} stopColor="#f5833c" />
                </LinearGradient>
                <LinearGradient
                  id="b"
                  x1={0.5}
                  x2={0.5}
                  y2={1}
                  gradientUnits="objectBoundingBox">
                  <Stop offset={0} stopColor={green100} />
                  <Stop offset={1} stopColor={green200} />
                </LinearGradient>
              </Defs>
              <G transform="translate(-11 -3)">
                <Path
                  data-name="Path 21366"
                  d="M18 0c9.941 0 18 2.239 18 5s-8.059 5-18 5S0 7.761 0 5s8.059-5 18-5Z"
                  transform="translate(14 51)"
                  fill="url(#a)"
                />
                <Path
                  data-name="Path 21359"
                  d="M53 24c0 7.8-9.5 20.03-15.72 27.21C34.25 54.7 32 57 32 57s-2.25-2.3-5.28-5.79C20.5 44.03 11 31.8 11 24a21 21 0 0 1 42 0Z"
                  fill="url(#a)"
                />
                <Circle
                  data-name="Ellipse 383"
                  cx={16}
                  cy={16}
                  r={16}
                  transform="translate(16 8)"
                  fill="#d1e7f8"
                />
                <Path
                  data-name="Path 21360"
                  d="M48 24a15.98 15.98 0 0 1-26.735 11.839q-2.214 1.242-4.533 2.309A122.307 122.307 0 0 0 26.72 51.21C29.75 54.7 32 57 32 57s2.25-2.3 5.28-5.79C43.5 44.03 53 31.8 53 24a20.928 20.928 0 0 0-6.073-14.765q-1.206 2.343-2.592 4.575A15.932 15.932 0 0 1 48 24Z"
                  fill="#eb8a2d"
                />
                <Path
                  data-name="Path 21361"
                  d="M48 24a15.932 15.932 0 0 0-3.665-10.19 64.29 64.29 0 0 1-23.07 22.029A15.98 15.98 0 0 0 48 24Z"
                  fill="#b7cad9"
                />
                <Path
                  data-name="Path 21362"
                  d="M41 24v8a2.006 2.006 0 0 1-2 2H25a2.006 2.006 0 0 1-2-2v-8Z"
                  fill="url(#b)"
                />
                <Path
                  data-name="Path 21363"
                  d="M43 19v5H21v-5a2.006 2.006 0 0 1 2-2h18a2.006 2.006 0 0 1 2 2Z"
                  fill="url(#a)"
                />
                <Path
                  data-name="Path 21364"
                  d="M34 24v2a1 1 0 0 1-1 1h-2a1 1 0 0 1-1-1v-3a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1Z"
                  fill="#ff9811"
                />
                <Path
                  data-name="Path 21365"
                  d="M35 12h-6a2 2 0 0 0-2 2v3h2v-3h6v3h2v-3a2 2 0 0 0-2-2Z"
                  fill="#603913"
                />
              </G>
            </Svg>
            <Text
              style={{
                fontSize: 14,
                fontWeight: 'bold',
                color: dark,
                marginLeft: 10,
              }}>
              Work Location
            </Text>
          </View>
          <View>
            <Checkbox variant="white" label="Part time" />
          </View>
          <View style={{marginVertical: 10}}>
            <Checkbox variant="white" label="Full Time" />
          </View>
          <View>
            <Checkbox variant="white" label="Work Only Remote" />
          </View>
          <View style={{marginVertical: 10}}>
            <Checkbox
              variant="white"
              label="Work in Office"
              onPress={() => {
                isWorkLocation
                  ? setIsWorkLocation(false)
                  : setIsWorkLocation(true);
              }}
            />
          </View>
          {isWorkLocation ? (
            <View>
              <Checkbox variant="white" label="Willing to Locate" />
            </View>
          ) : null}
        </View>
        <View
          style={{
            padding: 20,
            borderRadius: 10,
            backgroundColor: dark,
            marginVertical: 10,
          }}>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={40.842}
              height={40.842}>
              <Defs>
                <LinearGradient
                  id="a"
                  x1={0.5}
                  x2={0.5}
                  y2={1}
                  gradientUnits="objectBoundingBox">
                  <Stop offset={0} stopColor="#f6a938" />
                  <Stop offset={1} stopColor="#f5833c" />
                </LinearGradient>
              </Defs>
              <G data-name="Group 998">
                <Path
                  data-name="Path 3189"
                  d="M23.543 7.567v-3.1C23.543-1.489 0-1.489 0 4.466v18.141c0 2.341 3.664 3.8 8.072 4.319-1.612.723-2.639 1.7-2.639 2.938v5.443c0 2.977 5.922 4.535 11.772 4.535s11.772-1.558 11.772-4.535V32.57c5.539-.152 10.866-1.7 10.866-4.52V11.722c-.001-3.759-9.381-5.142-16.3-4.155ZM11.772 1.811c6.445 0 9.96 1.794 9.96 2.717s-3.516 2.717-9.96 2.717S1.811 5.45 1.811 4.527s3.516-2.716 9.961-2.716ZM1.811 7.047c2.2 1.322 6.089 2.008 9.96 2.008s7.763-.685 9.961-2.008v.9c-3.147.7-5.433 1.972-5.433 3.824v.536a28.721 28.721 0 0 1-4.527.37c-6.445 0-9.96-1.794-9.96-2.717V7.047Zm16.3 18.11c.172.1.352.2.544.3a32.55 32.55 0 0 0-.544-.02ZM1.811 12.48c2.2 1.322 6.089 2.008 9.96 2.008a30.7 30.7 0 0 0 4.527-.355v4.513a28.722 28.722 0 0 1-4.527.37c-6.445 0-9.96-1.794-9.96-2.717V12.48Zm0 10.157v-3.818c2.2 1.322 6.089 2.008 9.96 2.008a30.7 30.7 0 0 0 4.527-.355v4.513a28.722 28.722 0 0 1-4.527.37c-6.444-.001-9.96-1.795-9.96-2.718Zm25.354 12.677c0 .922-3.516 2.717-9.96 2.717s-9.961-1.794-9.961-2.717V32.4c2.2 1.322 6.089 2.008 9.96 2.008 3.706 0 7.435-.626 9.675-1.84.094 0 .191 0 .285.007v2.738ZM17.2 32.6c-6.445 0-9.96-1.794-9.96-2.717s3.516-2.717 9.96-2.717 9.96 1.794 9.96 2.717S23.649 32.6 17.2 32.6Zm20.831-4.53c0 .876-3.209 2.522-9.055 2.685v-.874c0-1.131-.856-2.046-2.237-2.746.443.018.887.03 1.332.03 3.871 0 7.763-.685 9.96-2.008v2.913Zm0-5.433c0 .922-3.516 2.717-9.96 2.717s-9.96-1.794-9.96-2.717v-2.913c2.2 1.322 6.089 2.008 9.96 2.008s7.763-.685 9.96-2.008Zm0-5.433c0 .922-3.516 2.717-9.96 2.717s-9.96-1.794-9.96-2.717v-2.913c2.2 1.322 6.089 2.008 9.96 2.008s7.763-.685 9.96-2.008Zm-9.96-2.717c-6.445 0-9.96-1.794-9.96-2.717s3.516-2.717 9.96-2.717 9.96 1.794 9.96 2.717-3.516 2.718-9.96 2.718Z"
                  transform="translate(.5 .5)"
                  stroke="#f58b3b"
                  strokeLinecap="square"
                  fill="url(#a)"
                />
              </G>
            </Svg>
            <View
              style={{
                display: 'flex',
                flexDirection: 'column',
                flex: 1,
                marginLeft: 20,
              }}>
              <Text
                style={{
                  fontSize: 13,
                  color: white,
                  fontWeight: 'bold',
                  marginLeft: 10,
                }}>
                Hourly Rates
              </Text>
              <Input placeholder="Hourly Rates" style={{flex: 1, height: 45}} />
            </View>
          </View>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginVertical: 20,
            }}>
            <Image source={require('../../assets/startProjectSvg.png')} />
            <View
              style={{
                display: 'flex',
                flexDirection: 'column',
                flex: 1,
                marginLeft: 20,
              }}>
              <Text
                style={{
                  fontSize: 13,
                  color: white,
                  fontWeight: 'bold',
                  marginLeft: 10,
                }}>
                When are you normally able to start?
              </Text>
              <Select placeholder="Select" style={{flex: 1}} />
            </View>
          </View>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <ButtonPrimary label="Upload CV" onPress={() => {}} />
          </View>
          <View
            style={{
              width: '100%',
              position: 'relative',
              borderWidth: 1,
              borderColor: white,
              borderRadius: 5,
              marginTop: 10,
              flexDirection: 'row',
            }}>
            <TouchableOpacity
              style={{
                position: 'absolute',
                right: -10,
                top: -10,
                width: 20,
                height: 20,
                borderRadius: 20,
                backgroundColor: white,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={9.458}
                height={9.458}>
                <G
                  fill="none"
                  stroke={dark}
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={1.5}>
                  <Path data-name="Line 22" d="M8.397 1.061 1.061 8.397" />
                  <Path data-name="Line 23" d="m1.061 1.061 7.336 7.336" />
                </G>
              </Svg>
            </TouchableOpacity>
            <View
              style={{
                backgroundColor: '#E72D4B',
                height: 50,
                width: 50,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 5,
              }}>
              <Text>PDF</Text>
            </View>
            <View
              style={{
                justifyContent: 'center',
                padding: 10,
              }}>
              <Text>Sample File</Text>
            </View>
          </View>
        </View>
        <ProfessionalDetailsCard
          text="Socials"
          btnLabel="Upload"
          onPress={() => {
            setAddSocialsModalVisible(true);
          }}
          svg={
            <Svg
              xmlns="http://www.w3.org/2000/svg"
              width={45.148}
              height={39.488}>
              <Defs>
                <LinearGradient
                  id="a"
                  x1={0.5}
                  x2={0.5}
                  y2={1}
                  gradientUnits="objectBoundingBox">
                  <Stop offset={0} stopColor="#f6a938" />
                  <Stop offset={1} stopColor="#f5833c" />
                </LinearGradient>
                <LinearGradient
                  id="b"
                  x1={0.5}
                  x2={0.5}
                  y2={1}
                  gradientUnits="objectBoundingBox">
                  <Stop offset={0} stopColor="#f6a938" />
                  <Stop offset={1} stopColor="#fff4ee" />
                </LinearGradient>
                <LinearGradient
                  id="c"
                  x1={0.5}
                  x2={0.5}
                  y2={1}
                  gradientUnits="objectBoundingBox">
                  <Stop offset={0} stopColor={green100} />
                  <Stop offset={1} stopColor={green200} />
                </LinearGradient>
              </Defs>
              <G data-name="suitcase (1)">
                <G data-name="Group 1096">
                  <Path
                    data-name="Path 3425"
                    d="M183.373 34.091a8.882 8.882 0 0 0-1.961-1.047 14.434 14.434 0 0 0-10.335 0 8.882 8.882 0 0 0-1.961 1.047 1.484 1.484 0 0 0-.584 1.18V37.6l1.484.794 1.484-.794v-1.489a11.188 11.188 0 0 1 9.489 0V37.6l1.484.794 1.484-.794v-2.329a1.483 1.483 0 0 0-.584-1.18Z"
                    transform="translate(-153.671 -32.088)"
                    fill="url(#a)"
                  />
                  <G
                    data-name="Group 1095"
                    transform="translate(14.861 5.511)"
                    fill="url(#b)">
                    <Path
                      data-name="Path 3426"
                      d="M0 0v1.638l1.484.794 1.484-.794V0Z"
                    />
                    <Path
                      data-name="Path 3427"
                      d="M15.425 0h-2.973v1.638l1.484.794 1.484-.794Z"
                    />
                  </G>
                  <Path
                    data-name="Path 3428"
                    d="M13.247 298.286v13.182a2.834 2.834 0 0 0 2.834 2.834h37.144a2.834 2.834 0 0 0 2.834-2.834v-13.182Z"
                    transform="translate(-12.079 -274.813)"
                    fill="url(#c)"
                  />
                  <Path
                    data-name="Path 3429"
                    d="M1.168 21.834v1.638c2.354 1.869 8.576 5.5 21.406 5.5s19.052-3.628 21.406-5.5v-1.643H1.168Z"
                    fill={yellow}
                  />
                  <Path
                    data-name="Path 3430"
                    d="M1.045 126.91c2.256 1.837 8.469 5.6 21.529 5.6s19.273-3.759 21.529-5.6a4.733 4.733 0 0 0 1.045-2.992V116a2.834 2.834 0 0 0-2.834-2.833H2.834A2.834 2.834 0 0 0 0 116v7.916a4.733 4.733 0 0 0 1.045 2.992Z"
                    transform="translate(0 -106.019)"
                    fill="url(#c)"
                  />
                  <Path
                    data-name="Path 3431"
                    d="M44.1 237.276c-2.256 1.837-8.469 5.6-21.529 5.6s-19.273-3.759-21.529-5.6A2.834 2.834 0 0 1 0 235.078v1.638a2.834 2.834 0 0 0 1.045 2.2c2.256 1.837 8.469 5.6 21.529 5.6s19.273-3.759 21.529-5.6a2.834 2.834 0 0 0 1.045-2.2v-1.638a2.834 2.834 0 0 1-1.048 2.198Z"
                    transform="translate(0 -217.179)"
                    fill="url(#a)"
                  />
                  <Path
                    data-name="Path 3432"
                    d="M22.573 30.058c1.466 0 2.654-1.089 2.654-3.448v-1.609a.944.944 0 0 0-.944-.944h-3.419a.944.944 0 0 0-.945.944v1.61c.003 2.359 1.188 3.447 2.654 3.447Z"
                    fill="#fff"
                  />
                  <Path
                    data-name="Path 3433"
                    d="M228.551 336.528a2.654 2.654 0 0 1-2.654-2.654v1.639a2.654 2.654 0 0 0 5.308 0v-1.639a2.654 2.654 0 0 1-2.654 2.654Z"
                    transform="translate(-205.978 -307.263)"
                    fill="url(#c)"
                  />
                </G>
              </G>
            </Svg>
          }>
          <SocialsCard
            imgSouce={require('../../assets/facebook.png')}
            text="Facebook link"
          />
          <SocialsCard
            imgSouce={require('../../assets/google.png')}
            text="Google link"
          />
          <SocialsCard
            imgSouce={require('../../assets/linkedin.png')}
            text="LinkedIn link"
          />
        </ProfessionalDetailsCard>
        <View style={{marginBottom: 90}} />
      </ScrollView>
      <CustomBottomTabBar
        animatedBar={animatedBar}
        navigation={navigation}
        isSelected={isSelected}
        setIsSelected={setIsSelected}
      />
      <DegreePopup
        modalVisible={addDegreesModalVisible}
        setModalVisible={setAddDegreesModalVisible}
        title="Add Degree"
      />
      <DegreePopup
        modalVisible={editDegreesModalVisible}
        setModalVisible={setEditDegreesModalVisible}
        title="Edit Degree"
      />
      <SkillPopup
        modalVisible={addSkillsModalVisible}
        setModalVisible={setAddSkillsModalVisible}
        title="Add Skill"
      />
      <SkillPopup
        modalVisible={editSkillsModalVisible}
        setModalVisible={setEditSkillsModalVisible}
        title="Edit Skill"
      />
      <CertificatePopup
        modalVisible={addCertificationsModalVisible}
        setModalVisible={setAddCertificationsModalVisible}
        title="Add Certificate"
      />
      <CertificatePopup
        modalVisible={editCertificationsModalVisible}
        setModalVisible={setEditCertificationsModalVisible}
        title="Edit Certificate"
      />
      <ExperiencePopup
        modalVisible={addExperienceModalVisible}
        setModalVisible={setAddExperienceModalVisible}
        title="Add Work Experiance"
      />
      <SocialsPopup
        modalVisible={addSocialsModalVisible}
        setModalVisible={setAddSocialsModalVisible}
        title="Upload Socials"
      />
      <SocialsPopup
        modalVisible={editSocialsModalVisible}
        setModalVisible={setEditSocialsModalVisible}
        title="Edit Socials"
      />
    </Animated.View>
  );
}
