import React from 'react';
import {Input, Popup} from '../../components';
import Textarea from '../../components/Textarea';

const CustomRequirementsPopup = ({modalVisible, setModalVisible, title}) => {
  return (
    <Popup
      title={title}
      buttonLabel="Save"
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      onButtonPress={() => {
        setModalVisible(!modalVisible);
      }}>
      <Input placeholder="Contact Name" />
      <Input placeholder="Phone" />
      <Input placeholder="Email Address" />
      <Textarea placeholder="Description" />
    </Popup>
  );
};

export default CustomRequirementsPopup;
