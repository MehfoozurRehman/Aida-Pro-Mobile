import React from 'react';
import {Popup} from '../../components';
import Select from '../../components/Select';

const SkillPopup = ({modalVisible, setModalVisible, title}) => {
  return (
    <Popup
      small={true}
      title={title}
      buttonLabel="Save"
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      onButtonPress={() => {
        setModalVisible(!modalVisible);
      }}>
      <Select placeholder="Select Skill" />
    </Popup>
  );
};

export default SkillPopup;
