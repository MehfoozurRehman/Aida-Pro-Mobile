import React from 'react';
import {ScrollView, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import {detailsActive} from '../../assets/icons';
import {Input, Select, Textarea} from '../../components';
import {companyDetailsInputs} from '../../constants/constants';
import ButtonPrimary from '../../components/ButtonPrimary';
import HeaderPrimary from '../../components/HeaderPrimary';
import GooglePlacesInput from '../../components/GooglePlacesInput';
import PhoneInput from '../../components/PhoneInput';
import {white} from '../../constants/colors';

export default function CompanyDetailsEditScreen({
  setProgress,
  drawerAnimationStyle,
  navigation,
}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary
        title="Company Details"
        backPath="Company Details"
        icon={detailsActive}
      />
      <ScrollView
        style={{
          flex: 1,
          width: '100%',
          paddingHorizontal: 15,
          marginVertical: 10,
        }}>
        <Input
          label="Company Name"
          placeholder="Enter Company Name"
          error={false}
          errorMessage="Please enter job title"
        />
        <Input
          label="Representative Name"
          placeholder="Enter Representative Name"
          error={false}
          errorMessage="Please enter job title"
        />
        <Input
          label="Email Address"
          placeholder="Enter Email Address"
          error={false}
          errorMessage="Please enter job title"
        />
        <PhoneInput
          label="Phone"
          placeholder="Enter Phone"
          error={false}
          errorMessage="Please enter job title"
        />
        <Input
          label="Industry / Branch"
          placeholder="Enter Industry / Branch"
          error={false}
          errorMessage="Please enter job title"
        />
        <Input
          label="No. of Employee"
          placeholder="Enter No. of Employee"
          error={false}
          errorMessage="Please enter job title"
        />
        <Input
          label="Address"
          placeholder="Enter Address"
          error={false}
          errorMessage="Please enter job title"
        />
        <Input
          label="Zip Code"
          placeholder="Select Zip Code"
          error={false}
          errorMessage="Please enter job title"
        />
        <Select
          label="Country"
          placeholder="Select Country"
          height={500}
          error={false}
          errorMessage="Please enter time"
        />
        <Select
          label="City"
          placeholder="Select City"
          height={500}
          error={false}
          errorMessage="Please enter time"
        />
        <Textarea
          placeholder="Company Description"
          error={false}
          errorMessage="Please enter time"
        />
      </ScrollView>
      <View style={{marginBottom: 20, width: '100%', paddingHorizontal: 15}}>
        <ButtonPrimary
          onPress={() => {
            navigation.navigate('Company Details');
          }}
          label="Save"
          style={{marginVertical: 0}}
        />
      </View>
    </Animated.View>
  );
}
