import React from 'react';
import {Input, Popup} from '../../components';

const SocialsPopup = ({modalVisible, setModalVisible, title}) => {
  return (
    <Popup
      title={title}
      buttonLabel="Save"
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      onButtonPress={() => {
        setModalVisible(!modalVisible);
      }}>
      <Input placeholder="Facebook link" />
      <Input placeholder="Google link" />
      <Input placeholder="Linkedin link" />
    </Popup>
  );
};

export default SocialsPopup;
