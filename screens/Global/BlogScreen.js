import React from 'react';
import {FlatList, ScrollView, Text, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import {blogActive} from '../../assets/icons';
import Svg, {G, Path} from 'react-native-svg';
import BlogCard from '../../components/BlogCard';
import SearchInput from '../../components/SearchInput';
import HeaderPrimary from '../../components/HeaderPrimary';
import {dark, white} from '../../constants/colors';

export default function BlogScreen({
  setProgress,
  drawerAnimationStyle,
  navigation,
}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  const DATA = [
    {
      id: '1',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '2',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '3',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '4',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '5',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '6',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '7',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '8',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '9',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
  ];
  const renderItem = ({item}) => (
    <BlogCard
      authorName="Author name"
      blogName="Blog name"
      image={require('../../assets/userPic.png')}
      description="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took"
      onPress={() => {
        navigation.navigate('Blog Artical');
      }}
    />
  );
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderPrimary title="Blog" backPath="Home" icon={blogActive} />
      <FlatList
        ListHeaderComponent={
          <>
            <Text style={{fontSize: 20, color: dark}}>
              Blogs are dedicated to our
            </Text>
            <Text
              style={{
                fontSize: 22,
                color: dark,
                fontWeight: '600',
                marginTop: -5,
              }}>
              AI and data community.
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: '#7A7A7A',
                marginVertical: 10,
                marginBottom: 15,
              }}>
              Would you like to contribute or help us? We highly appreciate
              comments or corrections, please just contact us. Enjoy the
              readings :)
            </Text>
            <SearchInput
              placeholder="Search"
              svg={
                <Svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={20.681}
                  height={20.681}>
                  <G
                    data-name="Icon feather-search"
                    fill="none"
                    stroke="#545454"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={1}>
                    <Path
                      data-name="Path 2299"
                      d="M17.553 9.526A8.026 8.026 0 119.526 1.5a8.026 8.026 0 018.027 8.026z"
                    />
                    <Path
                      data-name="Path 2300"
                      d="M19.559 19.559l-4.364-4.364"
                    />
                  </G>
                </Svg>
              }
            />
            <Text
              style={{
                fontSize: 18,
                color: dark,
                fontWeight: '600',
                marginTop: 10,
              }}>
              Trending Blogs
            </Text>
            <View
              style={{
                width: 60,
                height: 2,
                backgroundColor: '#0DC5A1',
                marginBottom: 20,
                marginTop: 5,
              }}
            />
          </>
        }
        style={{
          flex: 1,
          width: '100%',
          paddingVertical: 20,
          paddingHorizontal: 15,
        }}
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </Animated.View>
  );
}
