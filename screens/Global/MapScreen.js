import React from 'react';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import Svg, {G, Path, Circle, ClipPath, Defs, Stop} from 'react-native-svg';
import LinearGradientComp from 'react-native-linear-gradient';
import {postingListItem} from '../../constants/constants';
import {
  SafeAreaView,
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity,
  Text,
  ScrollView,
  Image,
  FlatList,
} from 'react-native';
import {ProjectCard} from '../../components';
import {dark, white} from '../../constants/colors';

const MapStyle = [
  {
    elementType: 'geometry',
    stylers: [
      {
        color: '#f5f5f5',
      },
    ],
  },
  {
    elementType: 'labels.icon',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#616161',
      },
    ],
  },
  {
    elementType: 'labels.text.stroke',
    stylers: [
      {
        color: '#f5f5f5',
      },
    ],
  },
  {
    featureType: 'administrative.land_parcel',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#bdbdbd',
      },
    ],
  },
  {
    featureType: 'poi',
    elementType: 'geometry',
    stylers: [
      {
        color: '#eeeeee',
      },
    ],
  },
  {
    featureType: 'poi',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#757575',
      },
    ],
  },
  {
    featureType: 'poi.park',
    elementType: 'geometry',
    stylers: [
      {
        color: '#e5e5e5',
      },
    ],
  },
  {
    featureType: 'poi.park',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#9e9e9e',
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'geometry',
    stylers: [
      {
        color: white,
      },
    ],
  },
  {
    featureType: 'road.arterial',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#757575',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry',
    stylers: [
      {
        color: '#dadada',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#616161',
      },
    ],
  },
  {
    featureType: 'road.local',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#9e9e9e',
      },
    ],
  },
  {
    featureType: 'transit.line',
    elementType: 'geometry',
    stylers: [
      {
        color: '#e5e5e5',
      },
    ],
  },
  {
    featureType: 'transit.station',
    elementType: 'geometry',
    stylers: [
      {
        color: '#eeeeee',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'geometry',
    stylers: [
      {
        color: '#c9c9c9',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#9e9e9e',
      },
    ],
  },
];
export default function MapViewScreen({navigation}) {
  const DATA = [
    {
      id: '1',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '2',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '3',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '4',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '5',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '6',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '7',
      title: 'First Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '8',
      title: 'Second Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
    {
      id: '9',
      title: 'Third Item',
      content:
        'It is a long established fact that a reader will be distracted by the readable content.',
    },
  ];
  const renderItem = ({item}) => (
    <ProjectCard
      onPress={() => {
        navigation.navigate('Job Preview');
      }}
    />
  );
  return (
    <SafeAreaView style={{flex: 1, position: 'relative'}}>
      <MapView
        style={styles.map}
        initialRegion={{
          latitude: 37.78825,
          longitude: -122.4324,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
        provider={PROVIDER_GOOGLE}
      />
      <FlatList
        style={{
          position: 'absolute',
          bottom: 0,
          width: '100%',
          padding: 20,
          backgroundColor: white,
          height: 250,
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        }}
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        ListFooterComponent={<View style={{height: 30}} />}
      />

      <View style={styles.header}>
        <TouchableOpacity
          style={styles.headerBackBtn}
          onPress={() => navigation.navigate('Home')}>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={20.012}
            height={17.163}>
            <G data-name="Group 505">
              <Path
                data-name="Path 730"
                d="M18.809 7.176a1.43 1.43 0 00-.247-.018H4.443l.308-.143a2.864 2.864 0 00.809-.573l3.959-3.959a1.482 1.482 0 00.208-1.9A1.432 1.432 0 007.579.397L.42 7.558a1.432 1.432 0 000 2.025l7.159 7.159a1.432 1.432 0 002.148-.143 1.482 1.482 0 00-.208-1.9l-3.952-3.961a2.864 2.864 0 00-.716-.523l-.43-.193h14.062a1.482 1.482 0 001.511-1.2 1.432 1.432 0 00-1.185-1.646z"
                fill="#333"
              />
            </G>
          </Svg>
        </TouchableOpacity>
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: white,
            paddingVertical: 10,
            paddingHorizontal: 30,
            borderTopLeftRadius: 70,
            borderBottomLeftRadius: 70,
          }}>
          <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={24}
            height={24}
            fill="none"
            style={{marginRight: 10}}
            stroke={dark}
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
            className="feather feather-map-pin">
            <Path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z" />
            <Circle cx={12} cy={10} r={3} />
          </Svg>
          <Text style={styles.headerScreenName}>Location</Text>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  header: {
    position: 'absolute',
    top: 0,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 90,
    paddingTop: 30,
    paddingLeft: 20,
    width: '100%',
  },
  headerBackBtn: {
    borderRadius: 100 / 2,
  },
  headerBackBtnIcon: {color: white},
  headerScreenName: {
    color: '#0B2239',
    fontSize: 15,
  },
  headerScreenIcon: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
});
