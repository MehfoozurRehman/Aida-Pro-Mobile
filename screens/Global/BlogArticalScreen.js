import React from 'react';
import {Image, ScrollView, Text, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {useDrawerProgress} from '@react-navigation/drawer';
import HeaderSimple from '../../components/HeaderSimple';
import {dark, white} from '../../constants/colors';

export default function BlogArticalScreen({setProgress, drawerAnimationStyle}) {
  const progress = useDrawerProgress();
  setProgress(progress);
  return (
    <Animated.View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: white,
        ...drawerAnimationStyle,
        overflow: 'hidden',
      }}>
      <HeaderSimple backPath="Blog" />
      <ScrollView
        style={{flex: 1, width: '100%', paddingHorizontal: 15, paddingTop: 0}}>
        <Image
          source={require('../../assets/blogImage.png')}
          style={{width: '100%', height: 200, borderRadius: 20}}
        />
        <Text
          style={{
            fontSize: 22,
            fontWeight: '600',
            color: dark,
            marginTop: 15,
          }}>
          Topic of Blog
        </Text>
        <View
          style={{
            backgroundColor: '#F2F2F2',
            padding: 15,
            flexDirection: 'row',
            marginVertical: 15,
            borderRadius: 10,
            marginTop: 10,
          }}>
          <Image
            source={require('../../assets/userPic.png')}
            style={{width: 50, height: 50, borderRadius: 50, marginRight: 15}}
          />
          <View>
            <Text
              style={{
                fontSize: 14,
                color: dark,
                fontWeight: '600',
                marginBottom: 5,
              }}>
              Writer Name Here
            </Text>
            <Text style={{color: dark}}>Date Posted 12/4/2021</Text>
          </View>
        </View>
        <Text
          style={{
            fontSize: 12,
            width: '100%',
            color: dark,
            marginBottom: 20,
          }}>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries, but also the leap into electronic typesetting,
          remaining essentially unchanged. It was popularised in the 1960s with
          the release of Letraset sheets containing Lorem Ipsum passages, and
          more recently with desktop publishing software like Aldus PageMaker
          including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of
          the printing and typesetting industry. Lorem Ipsum has been the
          industry's standard dummy text ever since the 1500s, when an unknown
          printer took a galley of type and scrambled it to make a type specimen
          book. It has survived not only five centuries, but also the leap into
          electronic typesetting, remaining essentially unchanged. It was
          popularised in the 1960s with the release of Letraset sheets
          containing Lorem Ipsum passages, and more recently with desktop
          publishing software like Aldus PageMaker including versions of Lorem
          Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries, but also the leap into electronic typesetting,
          remaining essentially unchanged. It was popularised in the 1960s with
          the release of Letraset sheets containing Lorem Ipsum passages, and
          more recently with desktop publishing software like Aldus PageMaker
          including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of
          the printing and typesetting industry. Lorem Ipsum has been the
          industry's standard dummy text ever since the 1500s, when an unknown
          printer took a galley of type and scrambled it to make a type specimen
          book. It has survived not only five centuries, but also the leap into
          electronic typesetting, remaining essentially unchanged. It was
          popularised in the 1960s with the release of Letraset sheets
          containing Lorem Ipsum passages, and more recently with desktop
          publishing software like Aldus PageMaker including versions of Lorem
          Ipsum.
        </Text>
      </ScrollView>
    </Animated.View>
  );
}
