import React from 'react';
import {Modal, View, Text} from 'react-native';
import {Shadow} from 'react-native-shadow-2';
import {windowHeight, windowWidth} from '../constants/constants';
import ButtonPrimary from '../components/ButtonPrimary';
import LinearGradient from 'react-native-linear-gradient';
import ButtonSecondary from '../components/ButtonSecondary';
import {dark, white} from '../constants/colors';

const ActionConfirmation = ({
  modalVisible,
  setModalVisible,
  title,
  onAccept,
  onDecline,
  content,
}) => {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      hardwareAccelerated={true}
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible(!modalVisible);
      }}>
      <LinearGradient
        colors={['#0EE1A390', '#0CA69D90']}
        style={{
          width: windowWidth,
          height: windowHeight,
          justifyContent: 'center',
          padding: 20,
        }}>
        <Shadow
          distance={40}
          startColor={'rgba(0,0,0,.05)'}
          finalColor={'rgba(0,0,0,.01)'}
          offset={[2, 3]}
          containerViewStyle={{backgroundColor: 'rgba(0,0,0,0)'}}>
          <View
            style={{
              width: windowWidth - 40,
              maxHeight: windowHeight - 100,
              borderRadius: 10,
              borderBottomRadius: 0,
              paddingHorizontal: 20,
              paddingTop: 15,
              paddingBottom: 10,
              alignItems: 'center',
              backgroundColor: white,
              position: 'relative',
            }}>
            <Text
              style={{
                fontSize: 20,
                color: dark,
                width: '100%',
                fontWeight: 'bold',
                marginBottom: 5,
              }}>
              {title}
            </Text>
            <Text
              style={{
                fontSize: 14,
                color: dark,
                width: '100%',
                textAlign: 'center',
                marginBottom: 5,
              }}>
              {content}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                width: '100%',
              }}>
              <ButtonPrimary
                label="Yes"
                onPress={onAccept}
                style={{maxWidth: '49%', height: 40}}
              />
              <ButtonSecondary
                label="No"
                onPress={onDecline}
                style={{maxWidth: '49%', height: 40}}
              />
            </View>
          </View>
        </Shadow>
      </LinearGradient>
    </Modal>
  );
};

export default ActionConfirmation;
