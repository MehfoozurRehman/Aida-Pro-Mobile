import React from 'react';
import {ScrollView} from 'react-native';
import CustomDrawerItem from '../components/CustomDrawerItem';
import {
  companyDrawerItemList,
  freelancerDrawerItemList,
  professionalDrawerItemList,
  role,
} from '../constants/constants';

export default function CustomDrawerItems({
  isSelected,
  setIsSelected,
  navigation,
}) {
  return (
    <ScrollView style={{flex: 1, width: '100%', overflow: 'scroll'}}>
      {role === 'Freelancer'
        ? freelancerDrawerItemList.map((item, i) => (
            <CustomDrawerItem
              key={i}
              label={item.label}
              isSelected={isSelected}
              setIsSelected={setIsSelected}
              icon={item.icon}
              iconActive={item.iconActive}
              navigation={navigation}
            />
          ))
        : role === 'Professional'
        ? professionalDrawerItemList.map((item, i) => (
            <CustomDrawerItem
              key={i}
              label={item.label}
              isSelected={isSelected}
              setIsSelected={setIsSelected}
              icon={item.icon}
              iconActive={item.iconActive}
              navigation={navigation}
            />
          ))
        : companyDrawerItemList.map((item, i) => (
            <CustomDrawerItem
              key={i}
              label={item.label}
              isSelected={isSelected}
              setIsSelected={setIsSelected}
              icon={item.icon}
              iconActive={item.iconActive}
              navigation={navigation}
            />
          ))}
    </ScrollView>
  );
}
