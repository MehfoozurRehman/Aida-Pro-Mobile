import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {DrawerContentScrollView} from '@react-navigation/drawer';
import Svg, {G, Path} from 'react-native-svg';
import CustomDrawerItem from '../components/CustomDrawerItem';
import CustomDrawerItems from './CustomDrawerItems';
import {logout, logoutActive} from '../assets/icons';
import {white} from '../constants/colors';

export default function CustomDrawerContent({
  navigation,
  setIsSelected,
  isSelected,
}) {
  return (
    <DrawerContentScrollView
      screenOptions={{
        scrollEnabled: true,
      }}
      contentContainerStyle={{
        flex: 1,
      }}>
      <View
        style={{
          flex: 1,
          padding: 20,
          paddingRight: 0,
          justifyContent: 'space-between',
        }}>
        <View
          style={{
            flex: 1,
          }}>
          {/* close */}
          <View style={{alignItems: 'flex-start', justifyContent: 'center'}}>
            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              onPress={() => {
                navigation.closeDrawer();
              }}>
              <Svg
                xmlns="http://www.w3.org/2000/svg"
                width={30}
                height={30}
                fill="none"
                stroke={white}
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
                className="prefix__feather prefix__feather-x">
                <Path d="M18 6L6 18M6 6l12 12" />
              </Svg>
            </TouchableOpacity>
          </View>
          {/* Profile */}
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 10,
            }}
            onPress={() => {
              navigation.navigate('Profile');
              // role === 'Company' ? 'Company Details' :
            }}>
            <Image
              source={require('../assets/userPic.png')}
              style={{width: 50, height: 50, borderRadius: 50}}
            />
            <View style={{marginLeft: 10}}>
              <Text
                style={{
                  fontSize: 12,
                  color: white,
                  fontWeight: '600',
                  flexWrap: 'wrap',
                }}>
                Mehfooz-ur-Rehman
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  color: white,
                  flexWrap: 'wrap',
                }}>
                View your profile
              </Text>
            </View>
          </TouchableOpacity>
          {/* drawerItems */}
          <CustomDrawerItems
            isSelected={isSelected}
            setIsSelected={setIsSelected}
            navigation={navigation}
          />
        </View>
      </View>
    </DrawerContentScrollView>
  );
}
