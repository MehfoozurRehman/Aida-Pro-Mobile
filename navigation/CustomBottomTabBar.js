import React, {useEffect, useState} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import Animated from 'react-native-reanimated';
import CustomTabButton from '../components/CustomTabButton';
import {white} from '../constants/colors';
import {
  companyTabItemList,
  freelancerTabItemList,
  professionalTabItemList,
  role,
} from '../constants/constants';

export default function CustomBottomTabBar({
  navigation,
  animatedBar,
  setIsSelected,
  isSelected,
}) {
  const [tabItemList, setTabItemList] = useState(companyTabItemList);
  useEffect(() => {
    role === 'Freelancer'
      ? setTabItemList(freelancerTabItemList)
      : role === 'Professional'
      ? setTabItemList(professionalTabItemList)
      : setTabItemList(companyTabItemList);
  }, [role]);
  return (
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      colors={['#F6B038', '#F5833C']}
      style={{
        width: '100%',
        borderRadius: 26,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 8,
        position: 'absolute',
        bottom: 0,
      }}>
      <Animated.View
        style={{
          width: '100%',
          height: 50,
          flexDirection: 'row',
          backgroundColor: white,
          borderTopLeftRadius: 25,
          borderTopRightRadius: 25,
          paddingVertical: 8,
          paddingHorizontal: 18,
          justifyContent: 'space-between',
          ...animatedBar,
        }}>
        {tabItemList.map((item, i) => (
          <CustomTabButton
            key={item.label + i}
            navigation={navigation}
            label={item.label}
            icon={item.icon}
            activeIcon={item.iconActive}
            selected={isSelected}
            setSelected={setIsSelected}
          />
        ))}
      </Animated.View>
    </LinearGradient>
  );
}
