import React, {useState} from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Animated from 'react-native-reanimated';
import LinearGradient from 'react-native-linear-gradient';
import {globalDrawerScreenList} from '../constants/constants';
import CustomDrawerContent from './CustomDrawerContent';
import {green100, green200} from '../constants/colors';

const Drawer = createDrawerNavigator();

export default function CustomDrawer() {
  const [isSelected, setIsSelected] = useState('Home');

  const [progress, setProgress] = useState(0);
  const scale = Animated.interpolateNode(progress, {
    inputRange: [0, 1],
    outputRange: [1, 0.8],
  });

  const borderRadius = Animated.interpolateNode(progress, {
    inputRange: [0, 1],
    outputRange: [0, 26],
  });

  const animatedStyle = {borderRadius, transform: [{scale}]};
  const animatedBar = {borderRadius};

  return (
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      colors={[green100, green200]}
      style={{
        flex: 1,
      }}>
      <Drawer.Navigator
        screenOptions={{
          drawerType: 'slide',
          overlayColor: 'transparent',
          drawerStyle: {
            flex: 1,
            width: '65%',
            backgroundColor: 'transparent',
          },
          sceneContainerStyle: {
            backgroundColor: 'transparent',
          },
          header: () => null,
        }}
        drawerContent={props => {
          return (
            <CustomDrawerContent
              isSelected={isSelected}
              setIsSelected={setIsSelected}
              navigation={props.navigation}
            />
          );
        }}>
        {globalDrawerScreenList.map(item => (
          <Drawer.Screen name={item.name} key={item.name}>
            {props => (
              <item.component
                {...props}
                drawerAnimationStyle={animatedStyle}
                animatedBar={animatedBar}
                setProgress={setProgress}
                isSelected={isSelected}
                setIsSelected={setIsSelected}
              />
            )}
          </Drawer.Screen>
        ))}
      </Drawer.Navigator>
    </LinearGradient>
  );
}
