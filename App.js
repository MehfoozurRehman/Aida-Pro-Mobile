import 'react-native-gesture-handler';
import * as React from 'react';
import SplashScreen from 'react-native-splash-screen';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {ForgotPassword, Login, Signup, VerificationCode} from './screens/Auth';
import Onboarding from './screens/Onboarding';
import Welcome from './screens/Welcome';
import TermsAndConditionsScreen from './screens/TermsAndConditionsScreen';
import CustomDrawer from './navigation/CustomDrawer';
import PrivacyPolicyScreen from './screens/PrivacyPolicyScreen';
import {StatusBar} from 'react-native';

const Stack = createNativeStackNavigator();

const config = {
  animation: 'spring',
  config: {
    stiffness: 1000,
    damping: 500,
    mass: 3,
    overshootClamping: true,
    restDisplacementThreshold: 0.01,
    restSpeedThreshold: 0.01,
  },
};

function App() {
  React.useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <NavigationContainer>
      <StatusBar animated={true} />
      <Stack.Navigator
        screenOptions={{
          header: () => null,
          transitionSpec: {
            open: config,
            close: config,
          },
        }}
        initialRouteName="Onboarding">
        <Stack.Screen name="Onboarding" component={Onboarding} />
        <Stack.Screen name="Welcome" component={Welcome} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Signup" component={Signup} />
        <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
        <Stack.Screen name="VerificationCode" component={VerificationCode} />
        <Stack.Screen name="CustomDrawer" component={CustomDrawer} />
        <Stack.Screen
          name="TermsConditions"
          component={TermsAndConditionsScreen}
        />
        <Stack.Screen name="PrivacyPolicy" component={PrivacyPolicyScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
